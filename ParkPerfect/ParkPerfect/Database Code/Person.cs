﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect
{
    public class Person
    {

        public Person(string aFirstName, string aLastName, string aPersonID, string aSpotID, Occupations aOccupation, Grades aGrade, Image licensePicture = null)
        {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            FirstName = aFirstName;
            LastName = aLastName;
            PersonID = aPersonID;
            SpotID = aSpotID;
            Occupation = aOccupation;
            Grade = aGrade;
            LicenseImage = licensePicture;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonID { get; set; }
        public string SpotID { get; set; }

        public enum Occupations { Student, Teacher };
        public Occupations Occupation { get; set; }

        public enum Grades { NoGrade, Freshman, Sophomore, Junior, Senior };
        public Grades Grade { get; set; }
        public Image LicenseImage { get; set; }

        public Vehicle GetVehicle()
        {
            VehicleToPerson vtp = DBParkingLot.instance.SearchForVehicleToPerson(DBParkingLot.VehicleToPersonSearchCriteria.PersonID, PersonID);
            return DBParkingLot.instance.SearchForVehicle(DBParkingLot.VehiclesSearchCriteria.Plate, vtp.Plate);
        }

        public Vehicle[] GetVehicles()
        {
            VehicleToPerson[] vtps = DBParkingLot.instance.SearchForVehicleToPersons(DBParkingLot.VehicleToPersonSearchCriteria.PersonID, PersonID);

            string[] plates = new string[vtps.Length];
            for (int i = 0; i < plates.Length; i++)
            {
                plates[i] = vtps[i].Plate;
            }

            if (plates.Length <= 0) return new Vehicle[0];

            return DBParkingLot.instance.SearchForVehicles(DBParkingLot.VehiclesSearchCriteria.Plate, plates);
        }

        public override string ToString() {
            return FirstName + " " + LastName; 
        }
    }
}
