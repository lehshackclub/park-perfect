﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using Excel = Microsoft.Office.Interop.Excel;


namespace ParkingLotProject {
    class DBParkingLot : DBManager {
        public static DBParkingLot instance;

        public enum AllTables { People, Vehicles }
        public enum AllColumns { FirstName, LastName, PersonID, Occupation, Grade, SpotNumber, Plate, Make, Model, Color, YearMade, VIN }

        public DBParkingLot(string path) : base(path) {
            #region Enforces Singleton Pattern.
            //Check if instance already exists
            //if (instance == null) {
            //if not, set instance to this
            instance = this;
            //}
            #endregion
        }

        #region Insert

        /// <summary>
        /// Inserts general information into the GeneralInfo table.
        /// </summary>
        /// <param name="schoolName">The school's name.</param>
        /// <param name="aImage">The background image of the school.</param>
        /// <returns></returns>
        public bool InsertGeneral(string schoolName, Image aImage) {
            string[] columns = { "SchoolName", "SchoolImage" };
            object[] data = { schoolName, ConvertImageToString(aImage) };

            return Insert("GeneralInfo", columns, data);
        }

        /// <summary>
        /// Inserts a person into the people table.
        /// </summary>
        /// <param name="aPerson">The person being inserted</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertPerson(Person aPerson) {
            return InsertPerson(aPerson.FirstName, aPerson.LastName, aPerson.PersonID, aPerson.SpotNumber, aPerson.Occupation.ToString(), aPerson.Grade.ToString(), ConvertImageToString(aPerson.LicenseImage));
        }

        /// <summary>
        /// Inserts a person into the people table.
        /// </summary>
        /// <param name="firstName">The person's first name.</param>
        /// <param name="lastName">The person's second name.</param>
        /// <param name="personID">The person's id.</param>
        /// <param name="spotNumber">The person's spot number.</param>
        /// <param name="occupation">The person's occupation (0 = student, 1 = teacher).</param>
        /// <param name="grade">The person's grade level. -1 if they're not a student.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertPerson(string firstName, string lastName, string personID, int spotNumber, string occupation, string grade, string licenseImage) {
            string[] columns = { "FirstName", "LastName", "PersonID", "SpotNumber", "Occupation", "Grade", "LicenseImage" };
            object[] data = { firstName, lastName, personID, spotNumber, occupation, grade, licenseImage };

            return Insert("People", columns, data);
        }

        /// <summary>
        /// Inserts a vehicle in the vehicles table.
        /// </summary>
        /// <param name="aVehicle">The vehicle being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertVehicle(Vehicle aVehicle) {
            return InsertVehicle(aVehicle.Plate, aVehicle.Year, aVehicle.Make, aVehicle.Model, aVehicle.Color, aVehicle.Vin, aVehicle.PersonID, aVehicle.SpotNumber, ConvertImageToString(aVehicle.RegistrationImage));
        }

        /// <summary>
        /// Inserts a vehicle into the vehicles table.
        /// </summary>
        /// <param name="plate">The vehicle's plate number.</param>
        /// <param name="yearMade">The vehicle's manufacture year.</param>
        /// <param name="make">The vehicle's make.</param>
        /// <param name="model">The vehcile's model.</param>
        /// <param name="color">The vehicle's color.</param>
        /// <param name="vin">The vehicle's vin.</param>
        /// <param name="personID">The personID of the vehicle's owner.</param>
        /// <param name="spotNumber">The spotNumber of the vehicle's owner.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertVehicle(string plate, int yearMade, string make, string model, string color, string vin, string personID, int spotNumber, string registrationImage) {
            string[] columns = { "Plate", "YearMade", "Make", "Model", "Color", "VIN", "PersonID", "SpotNumber", "RegistrationImage" };
            object[] data = { plate, yearMade, make, model, color, vin, personID, spotNumber, registrationImage };

            return Insert("Vehicles", columns, data);
        }

        /// <summary>
        /// Inserts a ParkingSpot into the ParkingSpots table.
        /// </summary>
        /// <param name="aParkingSpot">The parking spot being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertSpot(Spot aParkingSpot) {
            return InsertSpot(aParkingSpot.AreaName, aParkingSpot.ZoneName, aParkingSpot.SpotNumber, (int)aParkingSpot.SpotType, aParkingSpot.Assigned);
        }

        /// <summary>
        /// Inserts a ParkingSpot into the ParkingSpots table.
        /// </summary>
        /// <param name="areaID">The areaID of parking spot.</param>
        /// <param name="zoneID">The zoneID of the parking spot.</param>
        /// <param name="spotNumber">The parking spots number.</param>
        /// <param name="type">The type of parking spot. (normal = 0, guest = 1, handicap = 2)</param>
        /// <param name="assigned">Whether the parking spot is assigned.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertSpot(string areaName, string zoneName, int spotNumber, int type, bool assigned) {
            string[] columns = { "AreaName", "ZoneName", "SpotNumber", "Type", "Assigned" };
            object[] data = { areaName, zoneName, spotNumber, type, assigned };

            return Insert("ParkingSpots", columns, data);
        }

        /// <summary>
        /// Inserts an SpotPoint into the ParkingSpotPoints table.
        /// </summary>
        /// <param name="aSpotPoint">The SpotPoint being added.</param>
        /// <returns></returns>
        public bool InsertSpotPoint(SpotPoint aSpotPoint) {
            return InsertSpotPoint(aSpotPoint.SpotNumber, aSpotPoint.X, aSpotPoint.Y);
        }

        /// <summary>
        /// Inserts an SpotPoint into the ParkingSpotPoints table.
        /// </summary>
        /// <param name="spotNumber">The spot number of the point.</param>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns></returns>
        public bool InsertSpotPoint(string spotNumber, double x, double y) {
            string[] columns = { "SpotNumber", "X", "Y" };
            object[] data = { spotNumber, x, y };

            return Insert("ParkingSpotPoints", columns, data);
        }

        /// <summary>
        /// Inserts a zone into the Zones table.
        /// </summary>
        /// <param name="aZone">The zone being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertZone(Zone aZone) {
            return InsertZone(/*aZone.ZoneID,*/ aZone.Name, aZone.Color);
        }

        /// <summary>
        /// Inserts a zone into the Zones table.
        /// </summary>
        /// <param name="zoneName">The zone's name.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertZone(string zoneName, string zoneColor) {
            string[] columns = {"ZoneName", "ZoneColor" };
            object[] data = { zoneName, zoneColor };

            return Insert("Zones", columns, data);

        }

        /// <summary>
        /// Inserts a ZonePoint into the ZonePoints table.
        /// </summary>
        /// <param name="aZonePoint">The ZonePoint being added.</param>
        /// <returns></returns>
        public bool InsertZonePoint(ZonePoint aZonePoint) {
            return InsertZonePoint(aZonePoint.ZoneName, aZonePoint.X, aZonePoint.Y);
        }

        /// <summary>
        /// Inserts a ZonePoint into the ZonePoints table.
        /// </summary>
        /// <param name="zoneName">The zonename.</param>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns></returns>
        public bool InsertZonePoint(string zoneName, double x, double y) {
            string[] columns = { "ZoneName", "X", "Y" };
            object[] data = { zoneName, x, y };

            return Insert("ZonePoints", columns, data);
        }

        /// <summary>
        /// Inserts an area into the areas table.
        /// </summary>
        /// <param name="aArea">The area being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertArea(Area aArea) {
            return InsertArea(aArea.AreaName, aArea.ZoneName, aArea.Spacing, aArea.Rows);
        }

        /// <summary>
        /// Inserts an area into the areas table.
        /// </summary>
        /// <param name="areaName">The area's name.</param>
        /// <param name="zoneID">The zoneID the area belongs to.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertArea(string areaName, string zoneName, int spacing, int rows) {
            string[] columns = { "AreaName", "ZoneName", "Spacing", "NumOfRows" };
            object[] data = { areaName, zoneName, spacing, rows };

            return Insert("Areas", columns, data);
        }

        /// <summary>
        /// Inserts an AreaPoint into the AreaPoints table.
        /// </summary>
        /// <param name="aAreaPoint">The AreaPoint being added.</param>
        /// <returns></returns>
        public bool InsertAreaPoint(AreaPoint aAreaPoint) {
            return InsertAreaPoint(aAreaPoint.AreaName, aAreaPoint.X, aAreaPoint.Y);
        }

        /// <summary>
        /// Inserts an AreaPoint into the AreaPoints table.
        /// </summary>
        /// <param name="areaName">The area name.</param>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns></returns>
        public bool InsertAreaPoint(string areaName, double x, double y) {
            string[] columns = { "AreaName", "X", "Y" };
            object[] data = { areaName, x, y };

            return Insert("AreaPoints", columns, data);
        }

        /// <summary>
        /// Inserts a Citation into the Citations table.
        /// </summary>
        /// <param name="aCitation"></param>
        /// <returns></returns>
        public bool InsertCitation(Citation aCitation) {
            return InsertCitation(aCitation.PersonID, aCitation.CitationName, aCitation.CitationInfo);
        }

        /// <summary>
        /// Inserts a Citation into the Citations table.
        /// </summary>
        /// <param name="personID">The ID of the person the citation is for.</param>
        /// <param name="citationName">The name of the citations.</param>
        /// <param name="citationInfo">The information of the citation.</param>
        /// <returns></returns>
        public bool InsertCitation(string personID, string citationName, string citationInfo) {
            string[] columns = { "PersonID", "CitationName", "CitationInfo" };
            object[] data = { personID, citationName, citationInfo };

            return Insert("Citations", columns, data);
        }

        #endregion

        #region Update

        public bool UpdateGeneral(string schoolName, string imagePath) {
            string[] columns = { "SchoolName", "ImagePath" };
            object[] data = { schoolName, imagePath };

            return Update("GeneralInfo", columns, data);
        }

        /// <summary>
        /// Updates peoples' info in the people table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the people being updated.</param>
        /// <param name="key">The search key for finding the people being updated.</param>
        /// <param name="aPerson">The updated the person.</param>
        /// <returns></returns>
        public bool UpdatePeople(PeopleSearchCriteria searchCriteria, object key, Person aPerson) {
            string[] columns = { "FirstName", "LastName", "PersonID", "SpotNumber", "Occupation", "Grade", "LicenseImage" };
            object[] data = { aPerson.FirstName, aPerson.LastName, aPerson.PersonID, aPerson.SpotNumber, aPerson.Occupation.ToString(), aPerson.Grade.ToString(), ConvertImageToString(aPerson.LicenseImage) };

            return Update("People", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates vehicles' info in the vehicles table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the vehicles being updated.</param>
        /// <param name="key">The search key for finding the vehicles being updated.</param>
        /// <param name="aPerson">The updated the vehicle.</param>
        public bool UpdateVehicle(VehiclesSearchCriteria searchCriteria, object key, Vehicle aVehicle) {
            string[] columns = { "Plate", "YearMade", "Make", "Model", "Color", "VIN", "PersonID", "SpotNumber", "RegistrationImage" };
            object[] data = { aVehicle.Plate, aVehicle.Year, aVehicle.Make, aVehicle.Model, aVehicle.Color, aVehicle.Vin, aVehicle.PersonID, aVehicle.SpotNumber, ConvertImageToString(aVehicle.RegistrationImage) };

            return Update("Vehicles", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates parking spots' info in the parking spots table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the parking spots being updated.</param>
        /// <param name="key">The search key for finding the parking spots being updated.</param>
        /// <param name="aPerson">The updated the parking spot.</param>
        public bool UpdateParkingSpot(ParkingSpotSearchCriteria searchCriteria, object key, Spot aSpot) {
            string[] columns = { "AreaName", "ZoneName", "SpotNumber", "Type", "Assigned" };
            object[] data = { aSpot.AreaName, aSpot.ZoneName, aSpot.SpotNumber, (int)aSpot.SpotType, aSpot.Assigned };

            return Update("ParkingSpots", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates parking spot points' info in the parking spot points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the parking spot points being updated.</param>
        /// <param name="key">The search key for finding the parking spot points being updated.</param>
        /// <param name="aPerson">The updated the parking spot point.</param>
        public bool UpdateParkingSpotPoint(SpotPointSearchCriteria searchCriteria, object key, SpotPoint aSpotPoint) {
            string[] columns = { "SpotNumber", "X", "Y" };
            object[] data = { aSpotPoint.SpotNumber, aSpotPoint.X, aSpotPoint.Y };

            return Update("ParkingSpotPoints", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates areas' info in the areas table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the areas being updated.</param>
        /// <param name="key">The search key for finding the areas being updated.</param>
        /// <param name="aPerson">The updated the area.</param>
        public bool UpdateArea(AreaSearchCriteria searchCriteria, object key, Area aArea) {
            string[] columns = { "AreaName", "ZoneName", "AreaColor", "Spacing" };
            object[] data = { aArea.AreaName, aArea.ZoneName, aArea.Spacing };

            return Update("Areas", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates area points' info in the area points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the area points being updated.</param>
        /// <param name="key">The search key for finding the area points being updated.</param>
        /// <param name="aPerson">The updated the area point.</param>
        public bool UpdateAreaPoint(AreaPointSearchCriteria searchCriteria, object key, AreaPoint aAreaPoint) {
            string[] columns = { "AreaName", "X", "Y" };
            object[] data = { aAreaPoint.AreaName, aAreaPoint.X, aAreaPoint.Y };

            return Update("AreaPoints", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates zones' info in the zones table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the zones being updated.</param>
        /// <param name="key">The search key for finding the zones being updated.</param>
        /// <param name="aPerson">The updated the zone.</param>
        public bool UpdateZone(ZoneSearchCriteria searchCriteria, object key, Zone aZone) {
            string[] columns = {"ZoneName", "ZoneColor" };
            object[] data = {aZone.Name, aZone.Color };

            return Update("Zones", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates zone points' info in the zone points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the zone points being updated.</param>
        /// <param name="key">The search key for finding the zone points being updated.</param>
        /// <param name="aPerson">The updated the zone point.</param>
        public bool UpdateZonePoint(ZonePointSearchCriteria searchCriteria, object key, ZonePoint aZonePoint) {
            string[] columns = { "ZoneName", "X", "Y" };
            object[] data = { aZonePoint.ZoneName, aZonePoint.X, aZonePoint.Y };

            return Update("ZonePoints", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates Citation's info in the Citations table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the citation being updated.</param>
        /// <param name="key">The search key for finding the citation being updated.</param>
        /// <param name="aPerson">The updated the citation.</param>
        /// <returns></returns>
        public bool UpdateCitation(CitationSearchCriteria searchCriteria, object key, Citation aCitation) {
            string[] columns = { "PersonID", "CitationName", "CitationInfo" };
            object[] data = { aCitation.PersonID, aCitation.CitationName, aCitation.CitationInfo };

            return Update("Citations", columns, data, searchCriteria.ToString(), key);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a person from the people table.
        /// </summary>
        /// <param name="personID">The personID of the person being removed.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeletePerson(string personID) {
            return DeleteSingle("People", "PersonID", personID);
        }

        /// <summary>
        /// Deletes a vehicle from the vehicles table.
        /// </summary>
        /// <param name="plate">The plate number of the vehicle being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteVehicle(string plate) {
            return DeleteSingle("Vehicles", "Plate", plate);
        }

        /// <summary>
        /// Deletes a spot from the parkingspots table.
        /// </summary>
        /// <param name="spotNumber">The spot number of the spot being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteSpot(string spotNumber) {
            return DeleteSingle("ParkingSpots", "SpotNumber", spotNumber);
        }

        /// <summary>
        /// Deletes all spots points for a spot.
        /// </summary>
        /// <param name="spotNumber">The SpotNumber of the points being deleted.</param>
        /// <returns></returns>
        public bool DeleteSpotPoints(string spotNumber) {
            return DeleteAll("ParkingSpotPoints", "SpotNumber", spotNumber);
        }

        /// <summary>
        /// Deletes an area from the areas table.
        /// </summary>
        /// <param name="areaName">The name of the area being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteArea(string areaName) {
            return DeleteSingle("Areas", "AreaName", areaName);
        }

        /// <summary>
        /// Deletes all area points for a area.
        /// </summary>
        /// <param name="areaName">The AreaName of the points being deleted.</param>
        /// <returns></returns>
        public bool DeleteAreaPoints(string areaName) {
            return DeleteAll("AreaPoints", "AreaName", areaName);
        }

        /// <summary>
        /// Deletes a zone from the zones table.
        /// </summary>
        /// <param name="zoneName">The name of the zone being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteZone(string zoneName) {
            return DeleteSingle("Zones", "ZoneName", zoneName);
        }

        /// <summary>
        /// Deletes all zone points for a zone.
        /// </summary>
        /// <param name="areaName">The ZoneName of the points being deleted.</param>
        /// <returns></returns>
        public bool DeleteZonePoints(string zoneName) {
            return DeleteAll("ZonePoints", "ZoneName", zoneName);
        }

        #endregion

        #region Search

        public GeneralInfo SearchGeneral() {
            DataTable dt = Search("GeneralInfo");
            return ConvertToGeneral(dt);
        }

        public enum PeopleSearchCriteria { FirstName, LastName, PersonID, SpotNumber, Occupation, Grade }

        public Person[] SearchForPeople() {
            DataTable dt = Search("People");
            return ConvertToPerson(dt);
        }

        public Person[] SearchForPeople(PeopleSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("People", searchCriteria.ToString(), searchKey);
            return ConvertToPerson(dt);
        }

        public Person SearchForPerson() {
            Person[] results = SearchForPeople();
            return (results.Length > 0) ? results[0] : null;
        }

        public Person SearchForPerson(PeopleSearchCriteria searchCriteria, object searchKey) {
            Person[] results = SearchForPeople(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum VehiclesSearchCriteria { Plate, YearMade, Make, Model, Color, VIN, PersonID, SpotNumber }

        public Vehicle[] SearchForVehicles() {
            DataTable dt = Search("Vehicles");
            return ConvertToVehicle(dt);
        }

        public Vehicle[] SearchForVehicles(VehiclesSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Vehicles", searchCriteria.ToString(), searchKey);
            return ConvertToVehicle(dt);
        }

        public Vehicle SearchForVehicle() {
            Vehicle[] results = SearchForVehicles();
            return (results.Length > 0) ? results[0] : null;
        }

        public Vehicle SearchForVehicle(VehiclesSearchCriteria searchCriteria, object searchKey) {
            Vehicle[] results = SearchForVehicles(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum AreaSearchCriteria { AreaName, ZoneName, AreaColor, Spacing, NumOfRows }

        public Area[] SearchForAreas() {
            DataTable dt = Search("Areas");
            return ConvertToArea(dt);
        }

        public Area[] SearchForAreas(AreaSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Areas", searchCriteria.ToString(), searchKey);
            return ConvertToArea(dt);
        }

        public Area SearchForArea() {
            Area[] results = SearchForAreas();
            return (results.Length > 0) ? results[0] : null;
        }

        public Area SearchForArea(AreaSearchCriteria searchCriteria, object searchKey) {
            Area[] results = SearchForAreas(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum AreaPointSearchCriteria { AreaName, X, Y }

        public AreaPoint[] SearchForAreaPoints() {
            DataTable dt = Search("AreaPoints");
            return ConvertToAreaPoint(dt);
        }

        public AreaPoint[] SearchForAreaPoints(AreaPointSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("AreaPoints", searchCriteria.ToString(), searchKey);
            return ConvertToAreaPoint(dt);
        }

        public enum ZoneSearchCriteria { ZoneID, ZoneName, ZoneColor }

        public Zone[] SearchForZones() {
            DataTable dt = Search("Zones");
            return ConvertToZone(dt);
        }

        public Zone[] SearchForZones(ZoneSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Zones", searchCriteria.ToString(), searchKey);
            return ConvertToZone(dt);
        }

        public Zone SearchForZone() {
            Zone[] results = SearchForZones();
            return (results.Length > 0) ? results[0] : null;
        }

        public Zone SearchForZone(ZoneSearchCriteria searchCriteria, object searchKey) {
            Zone[] results = SearchForZones(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum ZonePointSearchCriteria { ZoneName, X, Y }

        public ZonePoint[] SearchForZonePoints() {
            DataTable dt = Search("ZonePoints");
            return ConvertToZonePoint(dt);
        }

        public ZonePoint[] SearchForZonePoints(ZonePointSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("ZonePoints", searchCriteria.ToString(), searchKey);
            return ConvertToZonePoint(dt);
        }

        public enum ParkingSpotSearchCriteria { AreaName, ZoneName, SpotNumber, Type, Assigned}

        public Spot[] SearchForSpots ()
        {
            DataTable dt = Search("ParkingSpots");
            return ConvertToSpot(dt);
        }

        public Spot[] SearchForSpots(ParkingSpotSearchCriteria searchCriteria, object searchKey)
        {
            DataTable dt = Search("ParkingSpots", searchCriteria.ToString(), searchKey);
            return ConvertToSpot(dt);
        }

        public enum SpotPointSearchCriteria { SpotNumber, X, Y }

        public SpotPoint[] SearchForSpotPoints() {
            DataTable dt = Search("ParkingSpotPoints");
            return ConvertToSpotPoint(dt);
        }

        public SpotPoint[] SearchForSpotPoints(SpotPointSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("ParkingSpotPoints", searchCriteria.ToString(), searchKey);
            return ConvertToSpotPoint(dt);
        }

        public enum CitationSearchCriteria { PersonID, CitationName, CitationInfo }

        public Citation[] SearchForCitations(CitationSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Citations", searchCriteria.ToString(), searchKey);
            return ConvertToCitation(dt);
        }

        #endregion

        #region DataTable Converters
        private GeneralInfo ConvertToGeneral(DataTable dataTable) {
            var imageConverter = new ImageConverter();

            SpotPoint[] areaPoints = new SpotPoint[dataTable.Rows.Count];
            if (dataTable.Rows.Count <= 0) return null;
            DataRow row = dataTable.Rows[0];

            GeneralInfo ap = new GeneralInfo(
                    (string)row["SchoolName"],
                    ConvertStringToImage((string)row["SchoolImage"]));
            return ap;
        }

        private Person[] ConvertToPerson(DataTable dataTable) {
            Person[] people = new Person[dataTable.Rows.Count];
            for (int i = 0; i < people.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Person p = new Person(
                    (string)row["FirstName"],
                    (string)row["LastName"],
                    (string)row["PersonID"],
                    (int)row["SpotNumber"],
                    //Converts to enum.
                    (Person.Occupations)Enum.Parse(typeof(Person.Occupations), (string)row["Occupation"]),
                    (Person.Grades)Enum.Parse(typeof(Person.Grades),(string)row["Grade"]),

                    //Converts to image if the long text in the database is not null (please note, a check is needed for long text).
                    ConvertStringToImage(Convert.IsDBNull(row["LicenseImage"]) ? null : (string)row["LicenseImage"]));
                people[i] = p;
            }
            return people;
        }

        private Vehicle[] ConvertToVehicle(DataTable dataTable) {
            Vehicle[] vehicles = new Vehicle[dataTable.Rows.Count];
            for (int i = 0; i < vehicles.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Vehicle v = new Vehicle(
                    (string)row["Plate"],
                    (int)row["YearMade"],
                    (string)row["Make"],
                    (string)row["Model"],
                    (string)row["Color"],
                    (string)row["VIN"],
                    (string)row["PersonID"],
                    (int)row["SpotNumber"],

                    //Converts to image if the long text in the database is not null (please note, a check is needed for long text).
                    ConvertStringToImage(Convert.IsDBNull(row["RegistrationImage"]) ? null : (string)row["RegistrationImage"]));
                vehicles[i] = v;
            }
            return vehicles;
        }

        private Area[] ConvertToArea(DataTable dataTable) {
            Area[] areas = new Area[dataTable.Rows.Count];
            for (int i = 0; i < areas.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Area a = new Area(
                    (string)row["AreaName"],
                    (string)row["ZoneName"],
                    (int)row["Spacing"],
                    (int)row["NumOfRows"]);
                areas[i] = a;
            }
            return areas;
        }

        private AreaPoint[] ConvertToAreaPoint(DataTable dataTable) {
            AreaPoint[] areaPoints = new AreaPoint[dataTable.Rows.Count];
            for (int i = 0; i < areaPoints.Length; i++) {
                DataRow row = dataTable.Rows[i];
                AreaPoint ap = new AreaPoint(
                    (string)row["AreaName"],
                    (double)row["X"],
                    (double)row["Y"]);
                areaPoints[i] = ap;
            }
            return areaPoints;
        }

        private Zone[] ConvertToZone(DataTable dataTable) {
            Zone[] zone = new Zone[dataTable.Rows.Count];
            for (int i = 0; i < zone.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Zone a = new Zone(
                    (string)row["ZoneName"],
                    (string)row["ZoneColor"]);
                zone[i] = a;
            }
            return zone;
        }

        private ZonePoint[] ConvertToZonePoint(DataTable datatable) {
            ZonePoint[] zonePoints = new ZonePoint[datatable.Rows.Count];
            for (int i = 0; i < zonePoints.Length; i++) {
                DataRow row = datatable.Rows[i];
                ZonePoint zp = new ZonePoint(
                    (string)row["ZoneName"],
                    (double)row["X"],
                    (double)row["Y"]);
                zonePoints[i] = zp; 
            }
            return zonePoints;
        }

        private Spot[] ConvertToSpot(DataTable datatable)
        {
            Spot[] spots = new Spot[datatable.Rows.Count];
            for (int i = 0; i < spots.Length; i++)
            {
                DataRow row = datatable.Rows[i];
                Spot zp = new Spot(
                    (int)row["SpotNumber"],
                    (Spot.SpotTypes)Convert.ToInt32(row["Type"]),
                    (string)row["AreaName"],
                    (string)row["ZoneName"],
                    (bool)row["Assigned"]);
                spots[i] = zp;
            }
            return spots;
        }

        private SpotPoint[] ConvertToSpotPoint(DataTable dataTable) {
            SpotPoint[] spotPoints = new SpotPoint[dataTable.Rows.Count];
            for (int i = 0; i < spotPoints.Length; i++) {
                DataRow row = dataTable.Rows[i];
                SpotPoint sp = new SpotPoint(
                    row["SpotNumber"].ToString(),
                    (double)row["X"],
                    (double)row["Y"]);
                spotPoints[i] = sp;
            }
            return spotPoints;
        }

        private Citation[] ConvertToCitation (DataTable dataTable) {
            Citation[] citations = new Citation[dataTable.Rows.Count];
            for (int i = 0; i < citations.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Citation c = new Citation(
                    (string)row["PersonID"],
                    (string)row["CitationName"],
                    (string)row["CitationInfo"]);
                citations[i] = c;
            }
            return citations;
        }

        private string ConvertImageToString (Image image) {
            if (image == null) return string.Empty;
            ImageConverter converter = new ImageConverter();
            byte[] b = (byte[])converter.ConvertTo(image, typeof(byte[]));
            string imageString = Convert.ToBase64String(b);
            return imageString;
        }

        private Image ConvertStringToImage(string str) {
            if (str == null || str == string.Empty) return null;
            byte[] b = Convert.FromBase64String(str);
            MemoryStream ms = new MemoryStream(b);
            return Image.FromStream(ms);
        }
        #endregion

        #region Save / Report
        private const int VEHICLES_CUTOFF = 6;

        /// <summary>
        /// Returns a report of the database.
        /// </summary>
        /// <param name="path">The path of the .csv file.</param>
        /// <param name="columns">The columns that the .csv file will report.</param>
        /// <param name="conditionTable">The table that will be used to check the condition.</param>
        /// <param name="conditionColumn">The column that will be used to check the condition.</param>
        /// <param name="conditionKey">The key that column in the table will be compared to. If the condition is true it will be added to the report.</param>
        /// <param name="conditionOperator">How the conditionKey will be compared to the column.</param>
        public DataTable ReportDataTable(AllColumns[] columns, AllColumns conditionColumn, object conditionKey, SearchKeyOperator conditionOperator = SearchKeyOperator.EqualTo) {
            List<string> tablesL = new List<string>();
            //string[] tables = { "People", "Vehicles" };

            for (int i = 0; i < columns.Length; i++) {
                if ((int)columns[i] >= VEHICLES_CUTOFF && !tablesL.Contains("Vehicles")) tablesL.Add("Vehicles");
                if ((int)columns[i] < VEHICLES_CUTOFF && !tablesL.Contains("People")) tablesL.Add("People");
            }

            string[] tables = tablesL.ToArray();

            AllTables conditionTable;
            if ((int)conditionColumn >= VEHICLES_CUTOFF) {
                conditionTable = AllTables.Vehicles;
            } else {
                conditionTable = AllTables.People;
            }

            DataTable dt = Search(tables, "PersonID", conditionTable.ToString(), conditionColumn.ToString(), conditionKey, conditionOperator);

            ReportDataTable(dt, columns);

            return dt;
        }

        /// <summary>
        /// Returns a report of the database.
        /// </summary>
        /// <param name="path">The path of the .csv file.</param>
        /// <param name="columns">The columns that the .csv file will report.</param>
        /// <param name="conditionTable">The tables that will be used to check the condition.</param>
        /// <param name="conditionColumns">The columns that will be used to check the condition.</param>
        /// <param name="conditionKeys">The keys that column in the table will be compared to. If the condition is true it will be added to the report.</param>
        /// <param name="conditionOperator">How the conditionKeys will be compared to the column.</param>
        public DataTable ReportDataTable(AllColumns[] columns, AllColumns[] conditionColumns, object[] conditionKeys, SearchKeyOperator[] conditionOperator) {
            List<string> tablesL = new List<string>();
            //string[] tables = { "People", "Vehicles" };

            for (int i = 0; i < columns.Length; i++) {
                if ((int)columns[i] >= VEHICLES_CUTOFF && !tablesL.Contains("Vehicles")) tablesL.Add("Vehicles");
                if ((int)columns[i] < VEHICLES_CUTOFF && !tablesL.Contains("People")) tablesL.Add("People");
            }

            string[] tables = tablesL.ToArray();

            string[] conditionTablesStr = new string[conditionColumns.Length];
            string[] conditionColumnsStr = new string[conditionColumns.Length];
            for (int i = 0; i < conditionColumns.Length; i++) {
                conditionColumnsStr[i] = conditionColumns[i].ToString();
                if ((int)conditionColumns[i] >= VEHICLES_CUTOFF) {
                    conditionTablesStr[i] = AllTables.Vehicles.ToString();
                } else {
                    conditionTablesStr[i] = AllTables.People.ToString();
                }
            }

            DataTable dt = Search(tables, "PersonID", conditionTablesStr, conditionColumnsStr, conditionKeys, conditionOperator);

            ReportDataTable(dt, columns);

            return dt;
        }

        /// <summary>
        /// Returns a report of the database.
        /// </summary>
        /// <param name="path">The path of the .csv file.</param>
        /// <param name="columns">The columns that the .csv file will report.</param>
        public DataTable ReportDataTable(AllColumns[] columns) {
            List<string> tablesL = new List<string>();
            //string[] tables = { "People", "Vehicles" };

            for (int i = 0; i < columns.Length; i++) {
                if ((int)columns[i] >= VEHICLES_CUTOFF && !tablesL.Contains("Vehicles")) tablesL.Add("Vehicles");
                if ((int)columns[i] < VEHICLES_CUTOFF && !tablesL.Contains("People")) tablesL.Add("People");
            }

            string[] tables = tablesL.ToArray();

            DataTable dt = Search(tables, "PersonID");

            ReportDataTable(dt, columns);

            return dt;
        }

        /// <summary>
        /// Returns a fuzzy (Not 100% accurate search) report of the database.
        /// </summary>
        /// <param name="path">The path of the .csv file.</param>
        /// <param name="columns">The columns that the .csv file will report.</param>
        /// <param name="conditionTable">The table that will be used to check the condition.</param>
        /// <param name="conditionColumn">The column that will be used to check the condition.</param>
        /// <param name="conditionKey">The key that column in the table will be compared to. If the condition is true it will be added to the report.</param>
        /// <param name="conditionOperator">How the conditionKey will be compared to the column.</param>
        public DataTable ReportDataTableFuzzy(AllColumns[] columns, AllColumns conditionColumn, object conditionKey) {
            List<string> tablesL = new List<string>();
            //string[] tables = { "People", "Vehicles" };

            for (int i = 0; i < columns.Length; i++) {
                if ((int)columns[i] >= VEHICLES_CUTOFF && !tablesL.Contains("Vehicles")) tablesL.Add("Vehicles");
                if ((int)columns[i] < VEHICLES_CUTOFF && !tablesL.Contains("People")) tablesL.Add("People");
            }

            string[] tables = tablesL.ToArray();

            AllTables conditionTable;
            if ((int)conditionColumn >= VEHICLES_CUTOFF) {
                conditionTable = AllTables.Vehicles;
            } else {
                conditionTable = AllTables.People;
            }

            DataTable dt = SearchFuzzy(tables, "PersonID", conditionTable.ToString(), conditionColumn.ToString(), conditionKey);

            ReportDataTable(dt, columns);

            return dt;
        }

        /// <summary>
        /// Returns a report of the database.
        /// </summary>
        /// <param name="dt">The datatable the report is being created from.</param>
        /// <param name="path">The path of the .csv file.</param>
        /// <param name="columns">The columns that the .csv file will report.</param>
        private DataTable ReportDataTable(DataTable dt, AllColumns[] columns) {

            #region Need to find a better way. THIS IS GARBAGE!!!
            List<string> cols = new List<string>();
            List<string> colsH = new List<string>();

            for (int i = 0; i < columns.Length; i++) {
                switch (columns[i]) {
                    case AllColumns.PersonID:
                        cols.Add(columns[i].ToString());
                        cols.Add("People." + columns[i].ToString());
                        cols.Add("Vehicle." + columns[i].ToString());
                        break;
                    case AllColumns.SpotNumber:
                        cols.Add(columns[i].ToString());
                        cols.Add("People." + columns[i].ToString());
                        cols.Add("Vehicle." + columns[i].ToString());
                        break;
                    default:
                        cols.Add(columns[i].ToString());
                        break;
                }
            }

            //Remove columns.
            for (int i = 0; i < dt.Columns.Count; i++) {
                if (!cols.Contains(dt.Columns[i].ColumnName)) {
                    dt.Columns.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < dt.Columns.Count; i++) {
                dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Substring(dt.Columns[i].ColumnName.IndexOf('.') + 1);
            }

            #endregion  

            return dt;
            #region MyRegion
            //string str = string.Join(",", columns);


            //StreamWriter sw = new StreamWriter(path/* + (!path.Contains(".csv") ? ".csv" : "")*/);

            //sw.WriteLine(str);

            //for (int i = 0; i < dt.Rows.Count; i++) {
            //    DataRow row = dt.Rows[i];
            //    string[] columnData = new string[columns.Length];

            //    for (int j = 0; j < columns.Length; j++) {
            //        switch (columns[j]) {
            //            case SaveColumns.Occupation:
            //                columnData[j] = ((Person.Occupations)Convert.ToInt32(row[columns[j].ToString()])).ToString();
            //                break;
            //            case SaveColumns.Grade:
            //                columnData[j] = ((Person.Grades)Convert.ToInt32(row[columns[j].ToString()])).ToString();
            //                break;
            //            case SaveColumns.PersonID:
            //                try {
            //                    columnData[j] = row["PersonID"].ToString();
            //                } catch (Exception) {
            //                    columnData[j] = row["People.PersonID"].ToString();
            //                }
            //                break;
            //            case SaveColumns.SpotNumber:
            //                try {
            //                    columnData[j] = row["SpotNumber"].ToString();
            //                } catch (Exception) {
            //                    columnData[j] = row["People.SpotNumber"].ToString();
            //                }
            //                break;
            //            default:
            //                columnData[j] = row[columns[j].ToString()].ToString();
            //                break;
            //        }
            //    }
            //    //    sw.WriteLine(string.Join(",", columnData));
            //}

            //string fullPath = ((FileStream)(sw.BaseStream)).Name;
            //sw.Close();
            //return fullPath; 
            #endregion
        }


        #endregion

    }
}
