﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect
{
    static class DeletionHelpers
    {
        public static void deleteZone(ParkingZone currZone)
        {
            DBParkingLot.instance.DeleteZone(currZone.Name);
            DBParkingLot.instance.DeleteZonePoints(currZone.Name);
            foreach (var area in currZone.parkingAreas)
            {
                deleteArea(area);
            }
        }

        public static void deleteArea(ParkingArea currArea)
        {
            DBParkingLot.instance.DeleteArea(currArea.Name);
            DBParkingLot.instance.DeleteAreaPoints(currArea.Name);

            Spot[] s = DBParkingLot.instance.SearchForSpots(DBParkingLot.ParkingSpotSearchCriteria.AreaName, currArea.Name);
            foreach (var spot in s)
            {
                deleteSpot(spot);
            }

        }

        public static void deleteSpot(Spot currSpot)
        {
            DBParkingLot.instance.DeleteSpot(currSpot.SpotID);
            DBParkingLot.instance.DeleteSpotPoints(currSpot.SpotID);
        }

        public static void deletePerson(Person currPerson)
        {
            DBParkingLot.instance.DeletePerson(currPerson.PersonID);
            DBParkingLot.instance.DeleteCitation(DBParkingLot.CitationSearchCriteria.PersonID, currPerson.PersonID);

            Vehicle[] myVehicles = currPerson.GetVehicles();
            //Check if vehicle is specific to the person and if so delete it, if not then dont
            foreach (var vehicle in myVehicles)
            {
                if (DBParkingLot.instance.SearchForVehicleToPersons(DBParkingLot.VehicleToPersonSearchCriteria.Plate, vehicle.Plate).Count() <= 1) //if more then one person isnt connected
                {
                    deleteVehicle(vehicle);
                }
            }
        }

        public static void deleteVehicle(Vehicle currVehicle)
        {
            DBParkingLot.instance.DeleteVehicle(currVehicle.Plate);
            DBParkingLot.instance.DeleteVehiclesToPeople(currVehicle.GetOwner().PersonID, currVehicle.Plate);
        }
    }
}
