﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class TagsToSpot {

        public TagsToSpot(string aTagID, string aSpotNumber) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            TagID = aTagID;
            SpotID = aSpotNumber;
        }

        public string TagID { get; set; }

        public string SpotID { get; set; }

    }
}
