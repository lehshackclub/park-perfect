﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;


namespace ParkPerfect {

    /// <summary>
    /// Handles the connection of the parking lot database and the program.
    /// </summary>
    partial class DBParkingLot : DBManager {
        public static DBParkingLot instance;

        public enum AllTables { People, Vehicles }
        public enum AllColumns { FirstName, LastName, PersonID, Occupation, Grade, SpotID, LicenseImage, Plate, Make, Model, Color, YearMade, VIN, RegistrationImage }

        public DBParkingLot(string path) : base(path) {
            #region Enforces Singleton Pattern.
            //Check if instance already exists
            //if (instance == null) {
            //if not, set instance to this
            instance = this;
            //}
            #endregion
        }

        #region Insert

        /// <summary>
        /// Inserts general information into the GeneralInfo table.
        /// </summary>
        /// <param name="schoolName">The school's name.</param>
        /// <param name="aImage">The background image of the school.</param>
        /// <returns></returns>
        public bool InsertGeneral(string schoolName, Image aImage, Image aLogo) {
            string[] columns = { "SchoolName", "SchoolImage", "SchoolLogo" };
            object[] data = { schoolName, ConvertImageToString(aImage), ConvertImageToString(aLogo) };

            return Insert("GeneralInfo", columns, data);
        }

        /// <summary>
        /// Inserts a person into the people table.
        /// </summary>
        /// <param name="aPerson">The person being inserted</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertPerson(Person aPerson) {
            return InsertPerson(aPerson.FirstName, aPerson.LastName, aPerson.PersonID, aPerson.SpotID, aPerson.Occupation.ToString(), aPerson.Grade.ToString(), aPerson.LicenseImage);
        }

        /// <summary>
        /// Inserts a person into the people table.
        /// </summary>
        /// <param name="firstName">The person's first name.</param>
        /// <param name="lastName">The person's second name.</param>
        /// <param name="personID">The person's id.</param>
        /// <param name="spotID">The person's spot number.</param>
        /// <param name="occupation">The person's occupation (0 = student, 1 = teacher).</param>
        /// <param name="grade">The person's grade level. -1 if they're not a student.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertPerson(string firstName, string lastName, string personID, string spotID, string occupation, string grade, Image licenseImage) {
            string[] columns = { "FirstName", "LastName", "PersonID", "SpotID", "Occupation", "Grade", "LicenseImage" };
            object[] data = { firstName, lastName, personID, spotID, occupation, grade, ConvertImageToString(licenseImage) };

            return Insert("People", columns, data);
        }

        /// <summary>
        /// Inserts a vehicle in the vehicles table.
        /// </summary>
        /// <param name="aVehicle">The vehicle being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertVehicle(Vehicle aVehicle, string personID) {
            return InsertVehicle(aVehicle.Plate, aVehicle.Year, aVehicle.Make, aVehicle.Model, aVehicle.Color, aVehicle.Vin, personID, aVehicle.RegistrationImage);
        }

        /// <summary>
        /// Inserts a vehicle into the vehicles table.
        /// </summary>
        /// <param name="plate">The vehicle's plate number.</param>
        /// <param name="yearMade">The vehicle's manufacture year.</param>
        /// <param name="make">The vehicle's make.</param>
        /// <param name="model">The vehicle's model.</param>
        /// <param name="color">The vehicle's color.</param>
        /// <param name="vin">The vehicle's vin.</param>
        /// <param name="personID">The personID of the vehicle's owner.</param>
        /// <param name="spotID">The spotID of the vehicle's owner.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertVehicle(string plate, string yearMade, string make, string model, string color, string vin, string personID, Image registrationImage) {
            string[] columns = { "Plate", "YearMade", "Make", "Model", "Color", "VIN", "RegistrationImage" };
            object[] data = { plate, yearMade, make, model, color, vin, ConvertImageToString(registrationImage) };

            return  InsertVehiclesToPeople(personID, plate) && Insert("Vehicles", columns, data);
        }

        public bool InsertVehiclesToPeople (string personID, string plate) {
            string[] columns = { "PersonID", "Plate" };
            object[] data = { personID, plate };

            return Insert("VehiclesToPeople", columns, data);
        }

        /// <summary>
        /// Inserts a ParkingSpot into the ParkingSpots table.
        /// </summary>
        /// <param name="aParkingSpot">The parking spot being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertSpot(Spot aParkingSpot) {
            return InsertSpot(aParkingSpot.AreaName, aParkingSpot.ZoneName, aParkingSpot.SpotID, (int)aParkingSpot.SpotType);
        }

        /// <summary>
        /// Inserts a ParkingSpot into the ParkingSpots table.
        /// </summary>
        /// <param name="areaID">The areaID of parking spot.</param>
        /// <param name="zoneID">The zoneID of the parking spot.</param>
        /// <param name="spotID">The parking spots id.</param>
        /// <param name="type">The type of parking spot. (normal = 0, guest = 1, handicap = 2)</param>
        /// <param name="assigned">Whether the parking spot is assigned.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertSpot(string areaName, string zoneName, string spotID, int type) {
            string[] columns = { "AreaName", "ZoneName", "SpotID", "Type"};
            object[] data = { areaName, zoneName, spotID, type };

            return Insert("ParkingSpots", columns, data);
        }

        /// <summary>
        /// Inserts a parking spot tag into the ParkingSpotTags table.
        /// </summary>
        /// <param name="aParkingSpotTag">The tag being inserted.</param>
        /// <returns></returns>
        public bool InsertSpotTag(SpotTag aParkingSpotTag) {
            return InsertSpotTag(aParkingSpotTag.TagID, aParkingSpotTag.TagName, aParkingSpotTag.TagColor);
        }

        /// <summary>
        /// Inserts a parking spot tag into the ParkingSpotTags table.
        /// </summary>
        /// <param name="tagName">The name of the tag being inserted.</param>
        /// <param name="tagColor">The color of the tag being inserted.</param>
        /// <returns></returns>
        public bool InsertSpotTag(int tagID, string tagName, Color tagColor) {
            string[] columns = { "TagID", "TagName", "TagColor" };
            object[] data = { tagID, tagName, tagColor.ToArgb() };

            return Insert("ParkingSpotTags", columns, data);
        }

        public bool InsertTagsToSpot (TagsToSpot aTagsToSpot) {
            return InsertTagsToSpot(aTagsToSpot.TagID, aTagsToSpot.SpotID);
        }

        public bool InsertTagsToSpot (string tagID, string spotID) {
            string[] columns = { "TagID", "SpotID" };
            object[] data = { tagID, spotID };

            return Insert("TagsToSpot", columns, data);
        }

        /// <summary>
        /// Inserts an SpotPoint into the ParkingSpotPoints table.
        /// </summary>
        /// <param name="aSpotPoint">The SpotPoint being added.</param>
        /// <returns></returns>
        public bool InsertSpotPoint(SpotPoint aSpotPoint) {
            return InsertSpotPoint(aSpotPoint.SpotID, aSpotPoint.PointID, aSpotPoint.X, aSpotPoint.Y);
        }

        /// <summary>
        /// Inserts an SpotPoint into the ParkingSpotPoints table.
        /// </summary>
        /// <param name="spotID">The spot id of the point.</param>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns></returns>
        public bool InsertSpotPoint(string spotID, string pointID, double x, double y) {
            string[] columns = { "SpotID", "PointID", "X", "Y" };
            object[] data = { spotID, pointID, x, y };

            return Insert("ParkingSpotPoints", columns, data);
        }

        /// <summary>
        /// Inserts a zone into the Zones table.
        /// </summary>
        /// <param name="aZone">The zone being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertZone(Zone aZone) {
            return InsertZone(/*aZone.ZoneID,*/ aZone.Name, aZone.Color);
        }

        /// <summary>
        /// Inserts a zone into the Zones table.
        /// </summary>
        /// <param name="zoneName">The zone's name.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertZone(string zoneName, Color zoneColor) {
            string[] columns = {"ZoneName", "ZoneColor" };
            object[] data = { zoneName, zoneColor.ToArgb() };

            return Insert("Zones", columns, data);

        }

        /// <summary>
        /// Inserts a ZonePoint into the ZonePoints table.
        /// </summary>
        /// <param name="aZonePoint">The ZonePoint being added.</param>
        /// <returns></returns>
        public bool InsertZonePoint(ZonePoint aZonePoint) {
            return InsertZonePoint(aZonePoint.ZoneName, aZonePoint.X, aZonePoint.Y);
        }

        /// <summary>
        /// Inserts a ZonePoint into the ZonePoints table.
        /// </summary>
        /// <param name="zoneName">The zonename.</param>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns></returns>
        public bool InsertZonePoint(string zoneName, double x, double y) {
            string[] columns = { "ZoneName", "X", "Y" };
            object[] data = { zoneName, x, y };

            return Insert("ZonePoints", columns, data);
        }

        /// <summary>
        /// Inserts an area into the areas table.
        /// </summary>
        /// <param name="aArea">The area being inserted.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertArea(Area aArea) {
            return InsertArea(aArea.AreaName, aArea.ZoneName, aArea.Spacing, aArea.Rows);
        }

        /// <summary>
        /// Inserts an area into the areas table.
        /// </summary>
        /// <param name="areaName">The area's name.</param>
        /// <param name="zoneID">The zoneID the area belongs to.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        public bool InsertArea(string areaName, string zoneName, int spacing, int rows) {
            string[] columns = { "AreaName", "ZoneName", "Spacing", "NumOfRows" };
            object[] data = { areaName, zoneName, spacing, rows };

            return Insert("Areas", columns, data);
        }

        /// <summary>
        /// Inserts an AreaPoint into the AreaPoints table.
        /// </summary>
        /// <param name="aAreaPoint">The AreaPoint being added.</param>
        /// <returns></returns>
        public bool InsertAreaPoint(AreaPoint aAreaPoint) {
            return InsertAreaPoint(aAreaPoint.AreaName, aAreaPoint.PointID, aAreaPoint.X, aAreaPoint.Y);
        }

        /// <summary>
        /// Inserts an AreaPoint into the AreaPoints table.
        /// </summary>
        /// <param name="areaName">The area name.</param>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns></returns>
        public bool InsertAreaPoint(string areaName, string pointID, double x, double y) {
            string[] columns = { "AreaName", "PointID", "X", "Y" };
            object[] data = { areaName, pointID, x, y };

            return Insert("AreaPoints", columns, data);
        }

        /// <summary>
        /// Inserts a Citation into the Citations table.
        /// </summary>
        /// <param name="aCitation"></param>
        /// <returns></returns>
        public bool InsertCitation(Citation aCitation) {
            return InsertCitation(aCitation.PersonID, aCitation.CitationName, aCitation.CitationInfo);
        }

        /// <summary>
        /// Inserts a Citation into the Citations table.
        /// </summary>
        /// <param name="personID">The ID of the person the citation is for.</param>
        /// <param name="citationName">The name of the citations.</param>
        /// <param name="citationInfo">The information of the citation.</param>
        /// <returns></returns>
        public bool InsertCitation(string personID, string citationName, string citationInfo) {
            string[] columns = { "PersonID", "CitationName", "CitationInfo" };
            object[] data = { personID, citationName, citationInfo };

            return Insert("Citations", columns, data);
        }

        #endregion

        #region Update

        public bool UpdateGeneral(string schoolName, Image aImage, Image aLogo) {
            string[] columns = { "SchoolName", "SchoolImage", "SchoolLogo" };
            object[] data = { schoolName, ConvertImageToString(aImage), ConvertImageToString(aLogo) };

            return Update("GeneralInfo", columns, data);
        }

        /// <summary>
        /// Updates peoples' info in the people table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the people being updated.</param>
        /// <param name="key">The search key for finding the people being updated.</param>
        /// <param name="aPerson">The updated the person.</param>
        /// <returns></returns>
        public bool UpdatePeople(PeopleSearchCriteria searchCriteria, object key, Person aPerson) {
            string[] columns = { "FirstName", "LastName", "PersonID", "SpotID", "Occupation", "Grade", "LicenseImage" };
            object[] data = { aPerson.FirstName, aPerson.LastName, aPerson.PersonID, aPerson.SpotID, aPerson.Occupation.ToString(), aPerson.Grade.ToString(), ConvertImageToString(aPerson.LicenseImage) };

            return Update("People", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates vehicles' info in the vehicles table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the vehicles being updated.</param>
        /// <param name="key">The search key for finding the vehicles being updated.</param>
        /// <param name="aPerson">The updated the vehicle.</param>
        public bool UpdateVehicle(VehiclesSearchCriteria searchCriteria, object key, Vehicle aVehicle) {
            string[] columns = { "Plate", "YearMade", "Make", "Model", "Color", "VIN", "RegistrationImage" };
            object[] data = { aVehicle.Plate, aVehicle.Year, aVehicle.Make, aVehicle.Model, aVehicle.Color, aVehicle.Vin, ConvertImageToString(aVehicle.RegistrationImage) };

            return Update("Vehicles", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates parking spots' info in the parking spots table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the parking spots being updated.</param>
        /// <param name="key">The search key for finding the parking spots being updated.</param>
        /// <param name="aPerson">The updated the parking spot.</param>
        public bool UpdateParkingSpot(ParkingSpotSearchCriteria searchCriteria, object key, Spot aSpot) {
            string[] columns = { "AreaName", "ZoneName", "SpotID", "Type"};
            object[] data = { aSpot.AreaName, aSpot.ZoneName, aSpot.SpotID, (int)aSpot.SpotType };

            return Update("ParkingSpots", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates the parking spot tag's info in the parking spot tags table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the parking spot tags being updated.</param>
        /// <param name="key">The search key for finding the parking spot tags being updated.</param>
        /// <param name="aSpotTag">The updated parking spot tag.</param>
        /// <returns></returns>
        public bool UpdateParkingSpotTag(ParkingSpotTagSearchCriteria searchCriteria, object key, SpotTag aSpotTag) {
            string[] columns = { "TagID", "TagName", "TagColor" };
            object[] data = { aSpotTag.TagID, aSpotTag.TagName, aSpotTag.TagColor.ToArgb() };

            return Update("ParkingSpotTags", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates parking spot points' info in the parking spot points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the parking spot points being updated.</param>
        /// <param name="key">The search key for finding the parking spot points being updated.</param>
        /// <param name="aPerson">The updated the parking spot point.</param>
        public bool UpdateParkingSpotPoint(SpotPointSearchCriteria searchCriteria, object key, SpotPoint aSpotPoint) {
            string[] columns = { "SpotID", "PointID", "X", "Y" };
            object[] data = { aSpotPoint.SpotID, aSpotPoint.X, aSpotPoint.Y };

            return Update("ParkingSpotPoints", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates parking spot points' info in the parking spot points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the parking spot points being updated.</param>
        /// <param name="keys">The search keys for finding the parking spot points being updated.</param>
        /// <param name="aPerson">The updated the parking spot point.</param>
        public bool UpdateParkingSpotPoint(SpotPointSearchCriteria[] searchCriterias, object[] keys, SpotPoint aSpotPoint) {
            string[] searchCriteriasString = new string[searchCriterias.Length];
            for (int i = 0; i < searchCriterias.Length; i++) {
                searchCriteriasString[i] = searchCriterias[i].ToString();
            }

            string[] columns = { "SpotID", "PointID", "X", "Y" };
            object[] data = { aSpotPoint.SpotID, aSpotPoint.PointID, aSpotPoint.X, aSpotPoint.Y };

            return Update("ParkingSpotPoints", columns, data, searchCriteriasString, keys);
        }

        /// <summary>
        /// Updates areas' info in the areas table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the areas being updated.</param>
        /// <param name="key">The search key for finding the areas being updated.</param>
        /// <param name="aPerson">The updated the area.</param>
        public bool UpdateArea(AreaSearchCriteria searchCriteria, object key, Area aArea) {
            string[] columns = { "AreaName", "ZoneName", "AreaColor", "Spacing" };
            object[] data = { aArea.AreaName, aArea.ZoneName, aArea.Spacing };

            return Update("Areas", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates area points' info in the area points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the area points being updated.</param>
        /// <param name="key">The search key for finding the area points being updated.</param>
        /// <param name="aAreaPoint">The updated the area point.</param>
        public bool UpdateAreaPoint(AreaPointSearchCriteria searchCriteria, object key, AreaPoint aAreaPoint) {
            string[] columns = { "AreaName", "X", "Y" };
            object[] data = { aAreaPoint.AreaName, aAreaPoint.X, aAreaPoint.Y };

            return Update("AreaPoints", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates area points' info in the area points table.
        /// </summary>
        /// <param name="searchCriterias">The search criterias for finding the area points being updated.</param>
        /// <param name="keys">The search keys for finding the area points being updated.</param>
        /// <param name="aAreaPoint">The updated the area point.</param>
        public bool UpdateAreaPoint(AreaPointSearchCriteria[] searchCriterias, object[] keys, AreaPoint aAreaPoint) {
            string[] searchCriteriasString = new string[searchCriterias.Length];
            for (int i = 0; i < searchCriterias.Length; i++) {
                searchCriteriasString[i] = searchCriterias[i].ToString();
            }

            string[] columns = { "AreaName", "PointID", "X", "Y" };
            object[] data = { aAreaPoint.AreaName, aAreaPoint.PointID, aAreaPoint.X, aAreaPoint.Y };

            return Update("AreaPoints", columns, data, searchCriteriasString, keys);
        }

        /// <summary>
        /// Updates zones' info in the zones table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the zones being updated.</param>
        /// <param name="key">The search key for finding the zones being updated.</param>
        /// <param name="aPerson">The updated the zone.</param>
        public bool UpdateZone(ZoneSearchCriteria searchCriteria, object key, Zone aZone) {
            string[] columns = {"ZoneName", "ZoneColor" };
            object[] data = {aZone.Name, aZone.Color.ToArgb() };

            return Update("Zones", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates zone points' info in the zone points table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the zone points being updated.</param>
        /// <param name="key">The search key for finding the zone points being updated.</param>
        /// <param name="aPerson">The updated the zone point.</param>
        public bool UpdateZonePoint(ZonePointSearchCriteria searchCriteria, object key, ZonePoint aZonePoint) {
            string[] columns = { "ZoneName", "X", "Y" };
            object[] data = { aZonePoint.ZoneName, aZonePoint.X, aZonePoint.Y };

            return Update("ZonePoints", columns, data, searchCriteria.ToString(), key);
        }

        /// <summary>
        /// Updates Citation's info in the Citations table.
        /// </summary>
        /// <param name="searchCriteria">The search criteria for finding the citation being updated.</param>
        /// <param name="key">The search key for finding the citation being updated.</param>
        /// <param name="aPerson">The updated the citation.</param>
        /// <returns></returns>
        public bool UpdateCitation(CitationSearchCriteria searchCriteria, object key, Citation aCitation) {
            string[] columns = { "PersonID", "CitationName", "CitationInfo" };
            object[] data = { aCitation.PersonID, aCitation.CitationName, aCitation.CitationInfo };

            return Update("Citations", columns, data, searchCriteria.ToString(), key);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a person from the People table, their citations from the Citation table, and their vehicles from the Vehicle table if they aren't owned by anyone else.
        /// </summary>
        /// <param name="personID">The personID of the person being removed.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeletePersonAll(string personID) {
            return DeleteAll("People", "PersonID", personID) && DeleteVehiclesToPeople(personID) && DeleteCitation(CitationSearchCriteria.PersonID, personID) && DeleteOrphans("Vehicles", "VehiclesToPeople", "Plate", "Plate");
        }

        /// <summary>
        /// Deletes a person from the People table.
        /// </summary>
        /// <param name="personID">The personID of the person being removed.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeletePerson(string personID) {
            return DeleteAll("People", "PersonID", personID);
        }

        ///// <summary>
        ///// Deletes a person from the people table.
        ///// </summary>
        ///// <param name="searchCriteria">The criteria that a person is searched on.</param>
        ///// <param name="searchKey">The key that matches with the criteria to determine what person is deleted.</param>
        ///// <returns></returns>
        //public bool DeletePeople (PeopleSearchCriteria searchCriteria, object searchKey) {
        //    return DeleteAll("People", searchCriteria.ToString(), searchKey);
        //}

        /// <summary>
        /// LEGACY. Deletes a vehicle from the vehicles table.
        /// </summary>
        /// <param name="plate">The plate number of the vehicle being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteVehicle(string plate) {
            return DeleteSingle("Vehicles", "Plate", plate);
        }

        /// <summary>
        /// Deletes a vehicle from the Vehicles table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that a person is searchedon .</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what person is deleted.</param>
        /// <returns></returns>
        public bool DeleteVehicle(VehiclesSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("Vehicles", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// Deletes a vehicle to people relationship in the VehiclesToPeople table. 
        /// </summary>
        /// <param name="personID">The PersonID of the person in the relationship.</param>
        /// <param name="plate">The plate of the vehicle in the relationship.</param>
        /// <returns></returns>
        public bool DeleteVehiclesToPeople(string personID, string plate) {
            string[] columns = { "PersonID", "Plate" };
            string[] keys = { personID, plate };

            return DeleteAll("VehiclesToPeople", columns, keys);
        }

        /// <summary>
        /// Deletes a vehicle to people relationship in the VehiclesToPeople table. 
        /// </summary>
        /// <param name="personID">The PersonID of the person in the relationship.</param>
        /// <returns></returns>
        public bool DeleteVehiclesToPeople(string personID) {
            return DeleteAll("VehiclesToPeople", "PersonID", personID);
        }

        /// <summary>
        /// LEGACY. Deletes a spot from the parkingspots table.
        /// </summary>
        /// <param name="spotID">The spot number of the spot being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteSpot(string spotID) {
            return DeleteAll("ParkingSpots", "SpotID", spotID);
        }

        /// <summary>
        /// Deletes a spot from the ParkingSpots table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that a spot is searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what person is deleted.</param>
        /// <returns></returns>
        public bool DeleteSpot(ParkingSpotSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("ParkingSpots", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// Deletes a spot tag from the ParkingSpotTags table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that a spot tag is searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what spot tag is deleted.</param>
        /// <returns></returns>
        public bool DeleteSpotTag(ParkingSpotTagSearchCriteria searchCriteria, object searchKey) {
            return DeleteSingle("ParkingSpotTags", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// LEGACY. Deletes all spots points for a spot.
        /// </summary>
        /// <param name="spotID">The SpotID of the points being deleted.</param>
        /// <returns></returns>
        public bool DeleteSpotPoints(string spotID) {
            return DeleteAll("ParkingSpotPoints", "SpotID", spotID);
        }

        /// <summary>
        /// Deletes spot points from the ParkingSpotPoints table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that the spot points are searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what spot points deleted.</param>
        /// <returns></returns>
        public bool DeleteSpotPoints(SpotPointSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("ParkingSpotPoints", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// Deletes a tag to spot relationship from the ParkingTagsToPeople table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that a tag to spot relationship is searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what tag to spot relationship is deleted.</param>
        /// <returns></returns>
        public bool DeleteTagsToSpot (TagsToSpotSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("TagsToSpot", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// Deletes an area from the areas table.
        /// </summary>
        /// <param name="areaName">The name of the area being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteArea(string areaName) {
            return DeleteSingle("Areas", "AreaName", areaName);
        }

        /// <summary>
        /// LEGACY. Deletes all area points for a area.
        /// </summary>
        /// <param name="areaName">The AreaName of the points being deleted.</param>
        /// <returns></returns>
        public bool DeleteAreaPoints(string areaName) {
            return DeleteAll("AreaPoints", "AreaName", areaName);
        }

        /// <summary>
        /// Deletes area points from the AreaPoints table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that area points are searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what area points are deleted.</param>
        /// <returns></returns>
        public bool DeleteAreaPoints(AreaPointSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("AreaPoints", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// LEGACY. Deletes a zone from the zones table.
        /// </summary>
        /// <param name="zoneName">The name of the zone being deleted.</param>
        /// <returns>Returns whether the delete was successful or not.</returns>
        public bool DeleteZone(string zoneName) {
            return DeleteSingle("Zones", "ZoneName", zoneName);
        }

        /// <summary>
        /// Deletes a zone from the Zones table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that a zone is searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what zone is deleted.</param>
        /// <returns></returns>
        public bool DeleteZone(ZoneSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("Zones", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// LEGACY. Deletes all zone points for a zone.
        /// </summary>
        /// <param name="areaName">The ZoneName of the points being deleted.</param>
        /// <returns></returns>
        public bool DeleteZonePoints(string zoneName) {
            return DeleteAll("ZonePoints", "ZoneName", zoneName);
        }

        /// <summary>
        /// Deletes zone points from the ZonePoints table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that zone points are searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what zone points are deleted.</param>
        /// <returns></returns>
        public bool DeleteZonePoints(ZonePointSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("ZonePoints", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// LEGACY. Deletes a citation.
        /// </summary>
        /// <param name="citationName">The name of the citation being deleted.</param>
        /// <returns></returns>
        public bool DeleteCitation(string citationName) {
            return DeleteSingle("Citations", "CitationName", citationName);
        }

        /// <summary>
        /// Deletes a citation from the Citations table.
        /// </summary>
        /// <param name="searchCriteria">The criteria that a citation is searched on.</param>
        /// <param name="searchKey">The key that matches with the criteria to determine what citation is deleted.</param>
        /// <returns></returns>
        public bool DeleteCitation(CitationSearchCriteria searchCriteria, object searchKey) {
            return DeleteAll("Citations", searchCriteria.ToString(), searchKey);
        }

        /// <summary>
        /// Deletes a citation from the Citations table.
        /// </summary>
        /// <param name="searchCriterias">The criterias that a citation is searched on.</param>
        /// <param name="searchKeys">The keys that match with the criteria to determine what citation is deleted.</param>
        /// <returns></returns>
        public bool DeleteCitation(CitationSearchCriteria[] searchCriterias, object[] searchKeys) {
            string[] searchCriteriaString = new string[searchCriterias.Length];
            for (int i = 0; i < searchCriteriaString.Length; i++) {
                searchCriteriaString[i] = searchCriterias[i].ToString();
            }

            return DeleteAll("Citations", searchCriteriaString, searchKeys);
        }

        #endregion

        #region Search

        public GeneralInfo SearchGeneral() {
            DataTable dt = Search("GeneralInfo");
            return ConvertToGeneral(dt);
        }

        public enum PeopleSearchCriteria { FirstName, LastName, PersonID, SpotID, Occupation, Grade }

        public Person[] SearchForPeople() {
            DataTable dt = Search("People");
            return ConvertToPerson(dt);
        }

        public Person[] SearchForPeople(PeopleSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("People", searchCriteria.ToString(), searchKey);
            return ConvertToPerson(dt);
        }

        public Person[] SearchForPeople(PeopleSearchCriteria searchCriteria, object[] searchKey) {
            DataTable dt = Search("People", searchCriteria.ToString(), searchKey);
            return ConvertToPerson(dt);
        }

        public Person[] SearchForPeopleFuzzy(PeopleSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = SearchFuzzy("People", searchCriteria.ToString(), searchKey);
            return ConvertToPerson(dt);
        }

        public Person SearchForPerson() {
            Person[] results = SearchForPeople();
            return (results.Length > 0) ? results[0] : null;
        }

        public Person SearchForPerson(PeopleSearchCriteria searchCriteria, object searchKey) {
            Person[] results = SearchForPeople(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public Person SearchForPersonFuzzy(PeopleSearchCriteria searchCriteria, object searchKey) {
            Person[] results = SearchForPeopleFuzzy(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum VehiclesSearchCriteria { Plate, YearMade, Make, Model, Color, VIN }

        public Vehicle[] SearchForVehicles() {
            DataTable dt = Search("Vehicles");
            return ConvertToVehicle(dt);
        }

        public Vehicle[] SearchForVehicles(VehiclesSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Vehicles", searchCriteria.ToString(), searchKey);
            return ConvertToVehicle(dt);
        }

        public Vehicle[] SearchForVehicles(VehiclesSearchCriteria searchCriteria, object[] searchKey) {
            DataTable dt = Search("Vehicles", searchCriteria.ToString(), searchKey);
            return ConvertToVehicle(dt);
        }

        public Vehicle[] SearchForVehiclesFuzzy(VehiclesSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = SearchFuzzy("Vehicles", searchCriteria.ToString(), searchKey);
            return ConvertToVehicle(dt);
        }

        public Vehicle SearchForVehicle() {
            Vehicle[] results = SearchForVehicles();
            return (results.Length > 0) ? results[0] : null;
        }

        public Vehicle SearchForVehicle(VehiclesSearchCriteria searchCriteria, object searchKey) {
            Vehicle[] results = SearchForVehicles(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum VehicleToPersonSearchCriteria { PersonID, Plate }

        public VehicleToPerson[] SearchForVehicleToPersons (VehicleToPersonSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("VehiclesToPeople", searchCriteria.ToString(), searchKey);
            return ConvertVehiclesToPeople(dt);
        }

        public VehicleToPerson SearchForVehicleToPerson(VehicleToPersonSearchCriteria searchCriteria, object searchKey) {
            VehicleToPerson[] results = SearchForVehicleToPersons(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum AreaSearchCriteria { AreaName, ZoneName, AreaColor, Spacing, NumOfRows }

        public Area[] SearchForAreas() {
            DataTable dt = Search("Areas");
            return ConvertToArea(dt);
        }

        public Area[] SearchForAreas(AreaSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Areas", searchCriteria.ToString(), searchKey);
            return ConvertToArea(dt);
        }

        public Area SearchForArea() {
            Area[] results = SearchForAreas();
            return (results.Length > 0) ? results[0] : null;
        }

        public Area SearchForArea(AreaSearchCriteria searchCriteria, object searchKey) {
            Area[] results = SearchForAreas(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum AreaPointSearchCriteria { AreaName, PointID, X, Y }
        
        public AreaPoint[] SearchForAreaPoints() {
            DataTable dt = Search("AreaPoints");
            return ConvertToAreaPoint(dt);
            
        }

        public AreaPoint[] SearchForAreaPoints(AreaPointSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("AreaPoints", searchCriteria.ToString(), searchKey);
            return ConvertToAreaPoint(dt);
        }

        public AreaPoint[] SearchForAreaPoints(AreaPointSearchCriteria[] searchCriterias, object[] searchKeys) {
            string[] searchCriteriasString = new string[searchCriterias.Length];
            for (int i = 0; i < searchCriterias.Length; i++) {
                searchCriteriasString[i] = searchCriterias[i].ToString();
            }

            DataTable dt = Search("AreaPoints", searchCriteriasString, searchKeys);
            return ConvertToAreaPoint(dt);
        }

        public enum ZoneSearchCriteria { ZoneID, ZoneName, ZoneColor }

        public Zone[] SearchForZones() {
            DataTable dt = Search("Zones");
            return ConvertToZone(dt);
        }

        public Zone[] SearchForZones(ZoneSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Zones", searchCriteria.ToString(), searchKey);
            return ConvertToZone(dt);
        }

        public Zone SearchForZone() {
            Zone[] results = SearchForZones();
            return (results.Length > 0) ? results[0] : null;
        }

        public Zone SearchForZone(ZoneSearchCriteria searchCriteria, object searchKey) {
            Zone[] results = SearchForZones(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum ZonePointSearchCriteria { ZoneName, X, Y }

        public ZonePoint[] SearchForZonePoints() {
            DataTable dt = Search("ZonePoints");
            return ConvertToZonePoint(dt);
        }

        public ZonePoint[] SearchForZonePoints(ZonePointSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("ZonePoints", searchCriteria.ToString(), searchKey);
            return ConvertToZonePoint(dt);
        }

        public enum ParkingSpotSearchCriteria { AreaName, ZoneName, SpotID, Type, Assigned}

        public Spot[] SearchForSpots ()
        {
            DataTable dt = Search("ParkingSpots");
            return ConvertToSpot(dt);
        }

        public Spot[] SearchForSpots(ParkingSpotSearchCriteria searchCriteria, object searchKey)
        {
            DataTable dt = Search("ParkingSpots", searchCriteria.ToString(), searchKey);
            return ConvertToSpot(dt);
        }

        public Spot SearchForSpot(ParkingSpotSearchCriteria searchCriteria, object searchKey) {
            Spot[] results = SearchForSpots(searchCriteria, searchKey);
            return (results.Length > 0) ? results[0] : null;
        }

        public enum ParkingSpotTagSearchCriteria { TagID, TagName, TagColor }

        public SpotTag[] SearchForSpotTags() {
            DataTable dt = Search("ParkingSpotTags");
            return ConvertToSpotTag(dt);
        }

        public SpotTag[] SearchForSpotTags(ParkingSpotTagSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("ParkingSpotTags", searchCriteria.ToString(), searchKey);
            return ConvertToSpotTag(dt);
        }

        public enum TagsToSpotSearchCriteria { TagID, SpotID }

        public TagsToSpot[] SearchForTagsToSpot () {
            DataTable dt = Search("TagsToSpot");
            return ConvertToTagsToSpot(dt);
        }

        public TagsToSpot[] SearchForTagsToSpot(TagsToSpotSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("TagsToSpot", searchCriteria.ToString(), searchKey);
            return ConvertToTagsToSpot(dt);
        }

        public enum SpotPointSearchCriteria { SpotID, PointID, X, Y }

        public SpotPoint[] SearchForSpotPoints() {
            DataTable dt = Search("ParkingSpotPoints");
            return ConvertToSpotPoint(dt);
        }

        public SpotPoint[] SearchForSpotPoints(SpotPointSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("ParkingSpotPoints", searchCriteria.ToString(), searchKey);
            return ConvertToSpotPoint(dt);
        }

        public SpotPoint[] SearchForSpotPoints(SpotPointSearchCriteria[] searchCriterias, object[] searchKeys) {
            string[] searchCriteriasString = new string[searchCriterias.Length];
            for (int i = 0; i < searchCriterias.Length; i++) {
                searchCriteriasString[i] = searchCriterias[i].ToString();
            }

            DataTable dt = Search("ParkingSpotPoints", searchCriteriasString, searchKeys);
            return ConvertToSpotPoint(dt);
        }

        public enum CitationSearchCriteria { PersonID, CitationName, CitationInfo }

        public Citation[] SearchForCitations(CitationSearchCriteria searchCriteria, object searchKey) {
            DataTable dt = Search("Citations", searchCriteria.ToString(), searchKey);
            return ConvertToCitation(dt);
        }

        #endregion

        #region DataTable Converters
        private GeneralInfo ConvertToGeneral(DataTable dataTable) {
            var imageConverter = new ImageConverter();

            SpotPoint[] areaPoints = new SpotPoint[dataTable.Rows.Count];
            if (dataTable.Rows.Count <= 0) return null;
            DataRow row = dataTable.Rows[0];

            GeneralInfo ap = new GeneralInfo(
                    (string)row["SchoolName"],
                    ConvertStringToImage((string)row["SchoolImage"]),
                    ConvertStringToImage((string)row["SchoolLogo"]));
            return ap;
        }

        private Person[] ConvertToPerson(DataTable dataTable) {
            Person[] people = new Person[dataTable.Rows.Count];
            for (int i = 0; i < people.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Person p = new Person(
                    (string)row["FirstName"],
                    (string)row["LastName"],
                    (string)row["PersonID"],
                    (string)row["SpotID"],
                    //Converts to enum.
                    (Person.Occupations)Enum.Parse(typeof(Person.Occupations), (string)row["Occupation"]),
                    (Person.Grades)Enum.Parse(typeof(Person.Grades),(string)row["Grade"]),

                    //Converts to image if the long text in the database is not null (please note, a check is needed for long text).
                    ConvertStringToImage(Convert.IsDBNull(row["LicenseImage"]) ? null : (string)row["LicenseImage"]));
                people[i] = p;
            }
            return people;
        }

        private Vehicle[] ConvertToVehicle(DataTable dataTable) {
            Vehicle[] vehicles = new Vehicle[dataTable.Rows.Count];
            for (int i = 0; i < vehicles.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Vehicle v = new Vehicle(
                    (string)row["Plate"],
                    (string)row["YearMade"],
                    (string)row["Make"],
                    (string)row["Model"],
                    (string)row["Color"],
                    (string)row["VIN"],

                    //Converts to image if the long text in the database is not null (please note, a check is needed for long text).
                    ConvertStringToImage(Convert.IsDBNull(row["RegistrationImage"]) ? null : (string)row["RegistrationImage"]));
                vehicles[i] = v;
            }
            return vehicles;
        }

        private VehicleToPerson[] ConvertVehiclesToPeople(DataTable dataTable) {
            VehicleToPerson[] relationships = new VehicleToPerson[dataTable.Rows.Count];
            for (int i = 0; i < relationships.Length; i++) {
                DataRow row = dataTable.Rows[i];
                VehicleToPerson vtp = new VehicleToPerson(
                    (string)row["PersonID"],
                    (string)row["Plate"]);
                relationships[i] = vtp;
            }
            return relationships;
        }

        private Area[] ConvertToArea(DataTable dataTable) {
            Area[] areas = new Area[dataTable.Rows.Count];
            for (int i = 0; i < areas.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Area a = new Area(
                    (string)row["AreaName"],
                    (string)row["ZoneName"],
                    (int)row["Spacing"],
                    (int)row["NumOfRows"]);
                areas[i] = a;
            }
            return areas;
        }

        private AreaPoint[] ConvertToAreaPoint(DataTable dataTable) {
            AreaPoint[] areaPoints = new AreaPoint[dataTable.Rows.Count];
            for (int i = 0; i < areaPoints.Length; i++) {
                DataRow row = dataTable.Rows[i];
                AreaPoint ap = new AreaPoint(
                    (string)row["AreaName"],
                    (i + 1).ToString(),
                    (double)row["X"],
                    (double)row["Y"]);
                areaPoints[i] = ap;
            }
            return areaPoints;
        }

        private Zone[] ConvertToZone(DataTable dataTable) {
            Zone[] zone = new Zone[dataTable.Rows.Count];
            for (int i = 0; i < zone.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Zone a = new Zone(
                    (string)row["ZoneName"],
                    Color.FromArgb((int)row["ZoneColor"]));
                zone[i] = a;
            }
            return zone;
        }

        private ZonePoint[] ConvertToZonePoint(DataTable datatable) {
            ZonePoint[] zonePoints = new ZonePoint[datatable.Rows.Count];
            for (int i = 0; i < zonePoints.Length; i++) {
                DataRow row = datatable.Rows[i];
                ZonePoint zp = new ZonePoint(
                    (string)row["ZoneName"],
                    (double)row["X"],
                    (double)row["Y"]);
                zonePoints[i] = zp;
            }
            return zonePoints;
        }

        private Spot[] ConvertToSpot(DataTable datatable)
        {
            Spot[] spots = new Spot[datatable.Rows.Count];
            for (int i = 0; i < spots.Length; i++)
            {
                DataRow row = datatable.Rows[i];
                Spot s = new Spot(
                    (string)row["SpotID"],
                    (Spot.SpotTypes)Convert.ToInt32(row["Type"]),
                    (string)row["AreaName"],
                    (string)row["ZoneName"]);
                spots[i] = s;
            }
            return spots;
        }

        private SpotTag[] ConvertToSpotTag(DataTable datatable) {
            SpotTag[] spotTags = new SpotTag[datatable.Rows.Count];
            for (int i = 0; i < spotTags.Length; i++) {
                DataRow row = datatable.Rows[i];
                SpotTag st = new SpotTag(
                    (int)row["TagID"],
                    (string)row["TagName"],
                    Color.FromArgb(((int)row["TagColor"])));
                spotTags[i] = st;
            }
            return spotTags;
        }

        private TagsToSpot[] ConvertToTagsToSpot(DataTable datatable) {
            TagsToSpot[] spotTags = new TagsToSpot[datatable.Rows.Count];
            for (int i = 0; i < spotTags.Length; i++) {
                DataRow row = datatable.Rows[i];
                TagsToSpot ts = new TagsToSpot(
                    (string)row["TagID"],
                    (string)row["SpotID"]);
                spotTags[i] = ts;
            }
            return spotTags;
        }

        private SpotPoint[] ConvertToSpotPoint(DataTable dataTable) {
            SpotPoint[] spotPoints = new SpotPoint[dataTable.Rows.Count];
            for (int i = 0; i < spotPoints.Length; i++) {
                DataRow row = dataTable.Rows[i];
                SpotPoint sp = new SpotPoint(
                    row["SpotID"].ToString(),
                    row["PointID"].ToString(),
                    (double)row["X"],
                    (double)row["Y"]);
                spotPoints[i] = sp;
            }
            return spotPoints;
        }

        private Citation[] ConvertToCitation (DataTable dataTable) {
            Citation[] citations = new Citation[dataTable.Rows.Count];
            for (int i = 0; i < citations.Length; i++) {
                DataRow row = dataTable.Rows[i];
                Citation c = new Citation(
                    (string)row["PersonID"],
                    (string)row["CitationName"],
                    (string)row["CitationInfo"]);
                citations[i] = c;
            }
            return citations;
        }

        private string ConvertImageToString (Image image) {
            if (image == null) return string.Empty;
            ImageConverter converter = new ImageConverter();
            byte[] b = (byte[])converter.ConvertTo(image, typeof(byte[]));
            string imageString = Convert.ToBase64String(b);
            return imageString;
        }

        private Image ConvertStringToImage(string str) {
            if (str == null || str == string.Empty) return null;
            byte[] b = Convert.FromBase64String(str);
            MemoryStream ms = new MemoryStream(b);
            return Image.FromStream(ms);
        }
        #endregion

    }
}
