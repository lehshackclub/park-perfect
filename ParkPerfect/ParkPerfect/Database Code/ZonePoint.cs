﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class ZonePoint {

        public ZonePoint(string myZoneName, double aX, double aY) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiate yet. Please Instantiate first");

            X = aX;
            Y = aY;
            ZoneName = myZoneName;
        }

        string myZoneName;
        public string ZoneName {
            get { return myZoneName; }
            set { myZoneName = value; }
        }

        double myX;
        public double X {
            get { return myX; }
            set { myX = value; }
        }

        double myY;
        public double Y {
            get { return myY; }
            set { myY = value; }
        }

    }
}
