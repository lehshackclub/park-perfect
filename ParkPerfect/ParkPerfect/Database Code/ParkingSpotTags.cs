﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect {
    //Please note this class is public to account for it being used by SpotTagForm.cs.

    public class SpotTag {

        public SpotTag(int tagID, string tagName, Color tagColor) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            TagID = tagID;
            TagName = tagName;
            TagColor = tagColor;
        }

        /// <summary>
        /// The unique id of the tag. Primary key.
        /// </summary>
        public int TagID { get; set; }

        /// <summary>
        /// The name of the tag.
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// The color of the tag.
        /// </summary>
        public Color TagColor { get; set; }

    }
}
