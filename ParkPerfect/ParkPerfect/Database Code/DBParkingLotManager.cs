﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ParkPerfect {

    /// <summary>
    /// Handles the management of parking lot database(s). 
    /// Creating a new database, opening an existing database, etc.
    /// </summary>
    class DBParkingLotManager {

        /// <summary>
        /// Opens the forms to allow the user to open an existing database.
        /// </summary>
        public static void OpenDatabase() {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            /*Sets the openFileDialog with some default info so the user
            can easily find the file and can't open a wrong file type.*/
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Microsoft Access Database (*.accdb)|*.accdb";

            //Only opens if the user hits "Ok"
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                //If the file isn't in the recentFile list, add it at the top.
                if (!Properties.Settings.Default.RecentFiles.Contains(openFileDialog.FileName)) {
                    Properties.Settings.Default.RecentFiles.Insert(0, openFileDialog.FileName);
                } else {
                    //If the file is in the recentFile list, move it to the top.
                    Properties.Settings.Default.RecentFiles.Remove(openFileDialog.FileName);
                    Properties.Settings.Default.RecentFiles.Insert(0, openFileDialog.FileName);
                }

                //Save user settings
                Properties.Settings.Default.Save();

                if (!File.Exists(openFileDialog.FileName)) {
                    //If the file doesn't exist, display an error message.
                    MessageBox.Show("Please move the database somewhere readable and writable.", "Move database", MessageBoxButtons.OK);
                    //Quit the application after attempting to open the file.
                    Application.Exit();
                }

                //Opens the file.
                Form1.instance.DefaultLoad(openFileDialog.FileName);
            }
        }

        /// <summary>
        /// Opens the forms to allow the user to create a new database.
        /// </summary>
        public static void NewDatabase() {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            /*Sets the saveFileDialog with some default info so the user
            can easily save the file and as the right file type.*/
            saveFileDialog.InitialDirectory = "c:\\";
            saveFileDialog.FileName = "ParkingLotDatabase.accdb";
            saveFileDialog.Filter = "Microsoft Access Database (*.accdb)|*.accdb";

            //Only save if the user hits "Ok".
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                //Copies the empty database to the file location the user requested.
                File.WriteAllBytes(saveFileDialog.FileName, Properties.Resources.ParkingLotDatabase);

                //Add the new file to top of the recentFile list.
                Properties.Settings.Default.RecentFiles.Insert(0, saveFileDialog.FileName);

                //Save user settings
                Properties.Settings.Default.Save();

                //Opens the file.
                Form1.instance.DefaultLoad(saveFileDialog.FileName);

                //sender.Close();
            }
        }

        /// <summary>
        /// Opens the forms to allow the user to assign a person to a spot.
        /// </summary>
        public static void AssignPersonToSpot() {
            if (DBParkingLot.instance == null) return;

            //Asks the user what person they'd like to assign a spot.
            string personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to assign a spot.", "Enter an ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            if (personID == string.Empty) return;

            Person person = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, personID);

            //If the person is not found, indicate to user and stop assigning process.
            if (person == null) {
                MessageBox.Show("Sorry, that person could not be found.", "Person not found.", MessageBoxButtons.OK);
                return;
            }

            //Confirms to ensure the person is correct.
            if (MessageBox.Show(person.FirstName + " " + person.LastName + "? Is this the person you would like to assign a spot to?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            //Confirms to ensure the user wants to assign a person to a spot if they have already been assigned a spot.
            if (person.SpotID != string.Empty && MessageBox.Show("Are you sure you'd like assign " + person.FirstName + " " + person.LastName + " to a spot. They have previously" +
                " been assigned to a parking spot.", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            bool assigning = true;
            string spotID = "";
            Spot spot = null;
            Person personSpot = null;

            while (assigning) {
                spotID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the spot ID of the spot you'd like to assign to " + person.FirstName + " " + person.LastName + ".", "Enter an spot ID.", "").Trim();

                if (spotID == string.Empty) return;

                spot = DBParkingLot.instance.SearchForSpot(DBParkingLot.ParkingSpotSearchCriteria.SpotID, spotID);
                personSpot = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, spotID);

                if (spot == null) {
                    if (MessageBox.Show("A spot with that spot ID does not exist. Would you like to re-enter the spot ID?", "A spot with that ID does not exist.", MessageBoxButtons.YesNo) == DialogResult.No) return;
                } else {
                    assigning = false;
                }
            }

            DBParkingLot.instance.ForceOpenConnection();

            if (personSpot != null) {
                if (MessageBox.Show("The spot with that spot ID is already assigned. Would you like to unassign ther person in that spot?", "The spot with that spot ID is already assigned.", MessageBoxButtons.YesNo) == DialogResult.No) return;

                personSpot.SpotID = "";
                DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, personSpot.PersonID, personSpot);
            }

            person.SpotID = spotID;

            //Assigns the person a spot in the database.
            if (DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, person.PersonID, person)) {
                MessageBox.Show(person.FirstName + " " + person.LastName + " was successfully assigned to spot " + spot.SpotID + ".");
            } else {
                MessageBox.Show("Something went wrong. The person was not able to be assigned to the spot.", "Something went wrong.");
            }

            DBParkingLot.instance.ForceCloseConnection();
        }

        /// <summary>
        /// Opens the forms to allow the user to assign a person to a spot.
        /// </summary>
        /// <param name="aSpot">The spot a person is being assigned.</param>
        public static void AssignPersonToSpot(Spot aSpot) {
            if (DBParkingLot.instance == null) return;

            //Asks the user what person they'd like to assign a spot.
            string personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to assign a spot.", "Enter an ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            if (personID == string.Empty) return;

            Person person = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, personID);

            //If the person is not found, indicate to user and stop assigning process.
            if (person == null) {
                MessageBox.Show("Sorry, that person could not be found.", "Person not found.", MessageBoxButtons.OK);
                return;
            }

            //Confirms to ensure the person is correct.
            if (MessageBox.Show(person.FirstName + " " + person.LastName + "? Is this the person you would like to assign a spot to?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            //Confirms to ensure the user wants to assign a person to a spot if they have already been assigned a spot.
            if (person.SpotID != string.Empty && MessageBox.Show("Are you sure you'd like assign " + person.FirstName + " " + person.LastName + " to a spot. They have previously" +
                " been assigned to a parking spot.", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            bool assigning = true;
            string spotID = "";
            Spot spot = aSpot;
            Person personSpot = null;

            while (assigning) {
                //spotID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the spot ID of the spot you'd like to assign to " + person.FirstName + " " + person.LastName + ".", "Enter an spot ID.", "").Trim();

                //if (spotID == string.Empty) return;

                spot = DBParkingLot.instance.SearchForSpot(DBParkingLot.ParkingSpotSearchCriteria.SpotID, spotID);
                personSpot = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, spotID);

                if (spot == null) {
                    if (MessageBox.Show("A spot with that spot ID does not exist. Would you like to re-enter the spot ID?", "A spot with that ID does not exist.", MessageBoxButtons.YesNo) == DialogResult.No) return;
                } else {
                    assigning = false;
                }
            }

            DBParkingLot.instance.ForceOpenConnection();

            if (personSpot != null) {
                if (MessageBox.Show("The spot with that spot ID is already assigned. Would you like to unassign ther person in that spot?", "The spot with that spot ID is already assigned.", MessageBoxButtons.YesNo) == DialogResult.No) return;

                personSpot.SpotID = "";
                DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, personSpot.PersonID, personSpot);
            }

            person.SpotID = spotID;

            //Assigns the person a spot in the database.
            if (DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, person.PersonID, person)) {
                MessageBox.Show(person.FirstName + " " + person.LastName + " was successfully assigned to spot " + spot.SpotID + ".", "Sucessfully assigned");
            } else {
                MessageBox.Show("Something went wrong. The person was not able to be assigned to the spot.", "Something went wrong.");
            }

            DBParkingLot.instance.ForceCloseConnection();
        }

        /// <summary>
        /// Opens the forms to allow the user to assign a person to a spot.
        /// </summary>
        /// <param name="aPerson">The person being asssigned to a spot.</param>
        public static void AssignPersonToSpot(Person aPerson) {
            if (DBParkingLot.instance == null) return;

            //Asks the user what person they'd like to assign a spot.
            //string personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to assign a spot.", "Enter an ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            //if (personID == string.Empty) return;

            Person person = /*DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, personID);*/aPerson;

            //If the person is not found, indicate to user and stop assigning process.
            if (person == null) {
                MessageBox.Show("Sorry, that person could not be found.", "Person not found.", MessageBoxButtons.OK);
                return;
            }

            //Confirms to ensure the person is correct.
            if (MessageBox.Show(person.FirstName + " " + person.LastName + "? Is this the person you would like to assign a spot to?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            //Confirms to ensure the user wants to assign a person to a spot if they have already been assigned a spot.
            if (person.SpotID != string.Empty && MessageBox.Show("Are you sure you'd like assign " + person.FirstName + " " + person.LastName + " to a spot. They have previously" +
                " been assigned to a parking spot.", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            bool assigning = true;
            string spotID = "";
            Spot spot = null;
            Person personSpot = null;

            while (assigning) {
                spotID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the spot ID of the spot you'd like to assign to " + person.FirstName + " " + person.LastName + ".", "Enter an spot ID.", "").Trim();

                if (spotID == string.Empty) return;

                spot = DBParkingLot.instance.SearchForSpot(DBParkingLot.ParkingSpotSearchCriteria.SpotID, spotID);
                personSpot = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, spotID);

                if (spot == null) {
                    if (MessageBox.Show("A spot with that spot ID does not exist. Would you like to re-enter the spot ID?", "A spot with that ID does not exist.", MessageBoxButtons.YesNo) == DialogResult.No) return;
                } else {
                    assigning = false;
                }
            }

            DBParkingLot.instance.ForceOpenConnection();

            if (personSpot != null) {
                if (MessageBox.Show("The spot with that spot ID is already assigned. Would you like to unassign ther person in that spot?", "The spot with that spot ID is already assigned.", MessageBoxButtons.YesNo) == DialogResult.No) return;

                personSpot.SpotID = "";
                DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, personSpot.PersonID, personSpot);
            }

            person.SpotID = spotID;

            //Assigns the person a spot in the database.
            if (DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, person.PersonID, person)) {
                MessageBox.Show(person.FirstName + " " + person.LastName + " was successfully assigned to spot " + spot.SpotID + ".");
            } else {
                MessageBox.Show("Something went wrong. The person was not able to be assigned to the spot.", "Something went wrong.");
            }


            DBParkingLot.instance.ForceCloseConnection();
        }

        /// <summary>
        /// Opens the forms to allow the user to unassign a person from their spot.
        /// </summary>
        public static void UnassignPersonFromSpot () {
            if (DBParkingLot.instance == null) return;

            //Asks the user what person they'd like to unassign from a spot.
            string personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to unassign from a spot.", "Enter an ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            if (personID == string.Empty) return;

            //Searches the database for the person.
            Person person = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, personID);

            //If the person does not exist, prompt the user again for the person ID.
            while (person == null) {
                if (MessageBox.Show("The person with that ID does not exist. Would you like to try again?", "The person with that ID does not exist.", MessageBoxButtons.OKCancel) != DialogResult.OK) return;
                personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to unassign from a spot.", "Enter an ID.", "").Trim();
                person = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, personID);
            }

            //Checks if the person has a spot assigned to them already. If they don't, there's no reason to unassign a spot that doesn't exist.
            if (person.SpotID == string.Empty) {
                MessageBox.Show(person.FirstName + " " + person.LastName + " does not have a spot asssigned to them to.", "No spot to unassign.", MessageBoxButtons.OK);
                return;
            }

            //Confirms the unassignment again with the user.
            if (MessageBox.Show(person.FirstName + " " + person.LastName + "? Are you sure you'd like to unassign this person from spot " + person.SpotID + "?", "Confirm unassignment", MessageBoxButtons.YesNo) != DialogResult.Yes) return;

            //Unassocitates the person from the spot in the database and gives a success/failure popup.
            person.SpotID = "";
            if (DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, person.PersonID, person))
                MessageBox.Show(person.FirstName + " " + person.LastName + " was successfully unassigned from their spot.", "Sucessfully unassigned");
            else
                MessageBox.Show("Something went wrong. The person was not able to be unassigned from their spot.", "Unsucessfully unassigned");
        }

        /// <summary>
        /// Opens the forms to allow the user to unassign a spot from a person.
        /// </summary>
        public static void UnassignSpotFromPerson () {
            if (DBParkingLot.instance == null) return;

            //Asks the user the spot ID of the spot they'd like to unassign from a spot.
            string spotID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the spot ID of the spot you would like to unassign a person from", "Enter a spot ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            if (spotID == string.Empty) return;

            //Searches the database for the spot.
            Spot spot = DBParkingLot.instance.SearchForSpot(DBParkingLot.ParkingSpotSearchCriteria.SpotID, spotID);

            //If the spot does not exist, prompt the user again for the spot ID.
            while (spot == null) {
                if (MessageBox.Show("The spot with that ID does not exist. Would you like to try again?", "The spot with that ID does not exist.", MessageBoxButtons.OKCancel) != DialogResult.OK) return;
                spotID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the spot ID of the spot you would like to unassign a person from", "Enter a spot ID.", "").Trim();
                spot = DBParkingLot.instance.SearchForSpot(DBParkingLot.ParkingSpotSearchCriteria.SpotID, spotID);
            }

            //Searches for the person associated the spot.
            Person person = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, spot.SpotID);

            //Checks if the person exists. If they don't exist the spot hasn't been assigned.
            if (person == null) {
                MessageBox.Show("Spot " + spot.SpotID + " is already unassigned", "Already unassigned.", MessageBoxButtons.OK);
                return;
            }

            //Confirms the unassignment again with the user.
            if (MessageBox.Show(spot.SpotID + "? Are you sure you'd like to unassign spot " + spot.SpotID + " from " + person.FirstName + " " + person.LastName + "?", "Confirm unassignment", MessageBoxButtons.YesNo) != DialogResult.Yes) return;

            //Unassocitates the person from the spot in the database and gives a success/failure popup.
            person.SpotID = "";
            if (DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, person.PersonID, person))
                MessageBox.Show(person.FirstName + " " + person.LastName + " was successfully unassigned from the spot.", "Sucessfully unassigned");
            else
                MessageBox.Show("Something went wrong. The spot was not able to be unassigned.", "Unsucessfully unassigned");
        }

        public static void DeletePerson () {
            if (DBParkingLot.instance == null) return;

            //Asks the user what person they'd like to assign a spot.
            string personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to delete.", "Enter an ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            if (personID == string.Empty) return;

            Person person = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, personID);

            //If the person is not found, indicate to user and stop assigning process.
            if (person == null) {
                MessageBox.Show("Sorry, that person could not be found.", "Person not found.", MessageBoxButtons.OK);
                return;
            }

            //Confirms to ensure the person is correct.
            if (MessageBox.Show(person.FirstName + " " + person.LastName + "? Is this the person you would like to delete?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;
            if (MessageBox.Show("Are you sure you'd like to delete " + person.FirstName + " " + person.LastName + "?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            if (DBParkingLot.instance.DeletePersonAll(person.PersonID))
                MessageBox.Show(person.FirstName + " " + person.LastName + " has been deleted", "Person deleted.", MessageBoxButtons.OK);

        }

        public static void DeletePerson(Person aPerson) {
            if (DBParkingLot.instance == null) return;

            //Asks the user what person they'd like to assign a spot.
            //string personID = Microsoft.VisualBasic.Interaction.InputBox("Please enter the ID of the person you would like to delete.", "Enter an ID.", "").Trim();

            /*Because using a textbox from the VisualBasic reference, 
             * need to check if the user inputted anything.*/
            //if (personID == string.Empty) return;

            Person person = aPerson;

            //If the person is not found, indicate to user and stop assigning process.
            //if (person == null) {
            //    MessageBox.Show("Sorry, that person could not be found.", "Person not found.", MessageBoxButtons.OK);
            //    return;
            //}

            //Confirms to ensure the person is correct.
            if (MessageBox.Show(person.FirstName + " " + person.LastName + "? Is this the person you would like to delete?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;
            if (MessageBox.Show("Are you sure you'd like to delete " + person.FirstName + " " + person.LastName + "?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.No) return;

            if (DBParkingLot.instance.DeletePersonAll(person.PersonID))
                MessageBox.Show(person.FirstName + " " + person.LastName + " has been deleted", "Person deleted.", MessageBoxButtons.OK);

        }

    }
}
