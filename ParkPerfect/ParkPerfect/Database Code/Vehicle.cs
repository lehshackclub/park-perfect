﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect {
    public class Vehicle {

        /// <summary>
        /// Creates a Vehicle data container
        /// </summary>
        /// <param name="aPlate">The plate of the vehicle. Primary key.</param>
        /// <param name="aYear">The year of the vehicle.</param>
        /// <param name="aMake">The make of the vehicle.</param>
        /// <param name="aModel">The model of the vehicle.</param>
        /// <param name="aColor">The color of the vehicle.</param>
        /// <param name="aVin">The VIN number of the vehicle.</param>
        /// <param name="aPersonID">The PersonID of the person who owns the vehicle.</param>
        /// <param name="aSpotNumber">The spot number of where the vehicle is.</param>
        /// <param name="registationPicture">The image of the registration.</param>
        public Vehicle(string aPlate, string aYear, string aMake, string aModel, string aColor, string aVin, Image registationPicture = null) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            Plate = aPlate;
            Year = aYear;
            Make = aMake;
            Model = aModel;
            Color = aColor;
            Vin = aVin;
            RegistrationImage = registationPicture;
        }
        /// <summary>
        /// The plate of the vehicle. Primary key.
        /// </summary>
        public string Plate { get; set; }

        /// <summary>
        /// The year of the vehicle.
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// The make of the vehicle.
        /// </summary>
        public string Make { get; set; }

        /// <summary>
        /// The model of the vehicle.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// The color of the vehicle.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// The VIN number of the vehicle.
        /// </summary>
        public string Vin { get; set; }

        /// <summary>
        /// The image of the registration.
        /// </summary>
        public Image RegistrationImage { get; set; }

        public Person GetOwner () {
            VehicleToPerson vtp = DBParkingLot.instance.SearchForVehicleToPerson(DBParkingLot.VehicleToPersonSearchCriteria.Plate, Plate);

            return DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, vtp.PersonID);
        }

        public Person[] GetOwners() {
            VehicleToPerson[] vtps = DBParkingLot.instance.SearchForVehicleToPersons(DBParkingLot.VehicleToPersonSearchCriteria.Plate, Plate);

            string[] personIDs = new string[vtps.Length];
            for (int i = 0; i < personIDs.Length; i++) {
                personIDs[i] = vtps[i].PersonID;
            }

            if (personIDs.Length <= 0) return new Person[0];

            return DBParkingLot.instance.SearchForPeople(DBParkingLot.PeopleSearchCriteria.PersonID, personIDs) ?? new Person[0];
        }



        public override string ToString() {
            return "(" + Plate + ")" + " " + Color + " " + Year + " " + Make + " " + Model;
        }

    }
}
