﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
//using Adox;

namespace ParkPerfect {

    /// <summary>
    /// Handles the connection between the program and a database.
    /// </summary>
    class DBManager {

        protected OleDbConnection conn;

        private bool forceOpen;

        public DBManager(string path) {
            Connect(path);
        }

        /// <summary>
        /// Establishes a connection between the object and the database.
        /// </summary>
        /// <param name="path">The file path of the database.</param>
        /// <returns>Wheter the connection was successfully established or not.</returns>
        public bool Connect(string path) {
            try {
                conn = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + path);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        /// <summary>
        /// Forces the connection state to the database to open.
        /// Helpful to do this when mass inserting/modifying to ensure
        /// to ensure that the connection is not opening multiple times.
        /// </summary>
        public void ForceOpenConnection () {
            if (conn.State != ConnectionState.Open) conn.Open();
            forceOpen = true;
        }

        /// <summary>
        /// Forces the connection state to the database to closed.
        /// Helpful to do this when mass inserting/modifying to ensure
        /// to ensure that the connection is closing multiple times.
        /// </summary>
        public void ForceCloseConnection () {
            if (conn.State != ConnectionState.Closed) conn.Close();
            forceOpen = false;
        }

        #region Insert

        /// <summary>
        /// Inserts data into a table.
        /// </summary>
        /// <param name="table">The table the data is being inserted into.</param>
        /// <param name="columns">The column of which the data corresponds to.</param>
        /// <param name="data">The corresponding data to the column.</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        protected bool Insert(string table, string column, object data) {
            OleDbCommand cmd = new OleDbCommand("INSERT INTO " + table + "(" + column + ")VALUES(@" + column + ")", conn);

            cmd.Parameters.AddWithValue("@" + column, data);

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                //Execute command.
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
                cmd.Dispose();
            }

            return true;
        }

        /// <summary>
        /// Inserts data into a table.
        /// </summary>
        /// <param name="table">The table the data is being inserted into.</param>
        /// <param name="columns">The columns of which the data corresponds to. Must be the same length as data.</param>
        /// <param name="data">The corresponding data to the columns. Must be the same length as columns</param>
        /// <returns>Returns whether the insert was successful or not.</returns>
        protected bool Insert(string table, string[] columns, object[] data) {
            /*If the columns and data aren't the same length
            return false;*/
            if (columns.Length != data.Length) return false;

            //Create the strings used for the insert command.
            string columnsString = string.Join(",", columns);
            string parameterString = "@" + string.Join(", @", columns);

            OleDbCommand cmd = new OleDbCommand("INSERT INTO " + table + "(" + columnsString + ")VALUES(" + parameterString + ")", conn);

            //Send the parameters the data.
            for (int i = 0; i < columns.Length; i++) {
                cmd.Parameters.AddWithValue("@" + columns[i], data[i]);
            }


            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                //Execute command.
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
                cmd.Dispose();
            }

            return true;
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates a single column of all rows with new information.
        /// </summary>
        /// <param name="table">The table where the update will happen.</param>
        /// <param name="column">The column being updated.</param>
        /// <param name="data">The data that will be replacing the old data.</param>
        /// <returns></returns>
        protected bool Update(string table, string column, object data) {

            OleDbCommand cmd = new OleDbCommand("UPDATE " + table + " SET " + column + " = @" + column, conn);

            for (int i = 0; i < column.Length; i++) {
                cmd.Parameters.AddWithValue("@" + column, data);
            }

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
            }

            return true;
        }

        /// <summary>
        /// Updates all rows with new information.
        /// </summary>
        /// <param name="table">The table where the update will happen.</param>
        /// <param name="columns">The columns being updated.</param>
        /// <param name="data">The new data going in the columns.</param>
        /// <returns></returns>
        protected bool Update(string table, string[] columns, object[] data) {
            if (columns.Length != data.Length) return false;

            string[] stringColumns = (string[])columns.Clone();

            for (int i = 0; i < stringColumns.Length; i++) {
                stringColumns[i] = stringColumns[i] + " = @" + columns[i];
            }

            string columnsString = string.Join(",", stringColumns);
            OleDbCommand cmd = new OleDbCommand("UPDATE " + table + " SET " + columnsString, conn);

            for (int i = 0; i < columns.Length; i++) {
                cmd.Parameters.AddWithValue("@" + columns[i], data[i]);
            }

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
            }

            return true;
        }

        /// <summary>
        /// Updates a single column of a row with new information.
        /// </summary>
        /// <param name="table">The table where the update will happen.</param>
        /// <param name="column">The column being updated.</param>
        /// <param name="data">The data that will be replacing the old data.</param>
        /// <param name="keyColumn">The column that the key will match in.</param>
        /// <param name="key">All rows with this key will be updated.</param>
        /// <returns>Whether the update was successful or not.</returns>
        protected bool Update(string table, string column, object data, string keyColumn, object key) {

            OleDbCommand cmd = new OleDbCommand("UPDATE " + table + " SET " + column + " = @" + column + " " + " WHERE " + keyColumn + " = @Key", conn);

            cmd.Parameters.AddWithValue("@" + column, data);
            cmd.Parameters.AddWithValue("@Key", key);

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
            }

            return true;
        }

        /// <summary>
        /// Updates a row with new information.
        /// </summary>
        /// <param name="table">The table where the update will happen.</param>
        /// <param name="columns">The columns being updated.</param>
        /// <param name="data">The new data going in the columns.</param>
        /// <param name="keyColumn">The column that the key will match in.</param>
        /// <param name="key">All rows with this key will be updated.</param>
        /// <returns>Whether the update was successful or not.</returns>
        protected bool Update(string table, string[] columns, object[] data, string keyColumn, object key) {
            if (columns.Length != data.Length) return false;

            string[] stringColumns = (string[])columns.Clone();

            for (int i = 0; i < stringColumns.Length; i++) {
                stringColumns[i] = stringColumns[i] + " = @" + columns[i];
            }

            string columnsString = string.Join(",", stringColumns);
            OleDbCommand cmd = new OleDbCommand("UPDATE " + table + " SET " + columnsString + " WHERE " + keyColumn + " = @Key", conn);

            for (int i = 0; i < columns.Length; i++) {
                cmd.Parameters.AddWithValue("@" + columns[i], data[i]);
            }
            cmd.Parameters.AddWithValue("@Key", key);

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
            }

            return true;
        }

        /// <summary>
        /// Updates a row with new information.
        /// </summary>
        /// <param name="table">The table where the update will happen.</param>
        /// <param name="columns">The columns being updated.</param>
        /// <param name="data">The new data going in the columns.</param>
        /// <param name="keyColumns">The columns that the key will match in.</param>
        /// <param name="keys">All rows with these keys will be updated.</param>
        /// <returns>Whether the update was successful or not.</returns>
        protected bool Update(string table, string[] columns, object[] data, string[] keyColumns, object[] keys) {
            if (columns.Length != data.Length) return false;

            //Creates the column string.
            string[] stringColumns = (string[])columns.Clone();

            for (int i = 0; i < stringColumns.Length; i++) {
                stringColumns[i] = stringColumns[i] + " = @" + columns[i];
            }

            string columnsString = string.Join(",", stringColumns);

            //Creates the key string.
            string keyString = "";
            for (int i = 0; i < keyColumns.Length - 1; i++) {
                keyString += keyColumns[i] + " = @Key" + i + " AND ";
            }
            keyString += keyColumns[keyColumns.Length - 1] + " = @Key" + (keyColumns.Length - 1);

            //Sends the command.
            OleDbCommand cmd = new OleDbCommand("UPDATE " + table + " SET " + columnsString + " WHERE " + keyString, conn);

            //Inserts the parameters.
            for (int i = 0; i < columns.Length; i++) {
                cmd.Parameters.AddWithValue("@" + columns[i], data[i]);
            }
            for (int i = 0; i < keyColumns.Length; i++) {
                cmd.Parameters.AddWithValue("@Key", keys[i]);
            }

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();
            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
            }

            return true;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes orphaned rows from the table.
        /// </summary>
        /// <param name="table1">The table where the rows are being deleted from.</param>
        /// <param name="table2">The table where the column is checked to be null.</param>
        /// <param name="column1">The column of table 1 that is being checked with column of table 2.</param>
        /// <param name="column2">The column of table 2 that is being checked with column of table 1.</param>
        /// <returns></returns>
        protected bool DeleteOrphans (string table1, string table2, string column1, string column2) {
            OleDbCommand cmd = new OleDbCommand("DELETE FROM " + table1 + " WHERE NOT EXISTS (SELECT NULL FROM " + table2 + " WHERE " + table1 + "."+ column1 + " = "+ table2 + "."+ column2 + ")", conn);

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();

            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
                cmd.Dispose();
            }

            return true;
        }

        /// <summary>
        /// Deletes a single row from a table that meets the criteria.
        /// </summary>
        /// <param name="table">The table the row is being deleted from.</param>
        /// <param name="column">The column that the key is being checked.</param>
        /// <param name="key">The key of the row being deleted.</param>
        /// <returns></returns>
        protected bool DeleteSingle(string table, string column, object key) {
            OleDbCommand cmd = new OleDbCommand("DELETE FROM " + table + " WHERE " + column + " = @" + column, conn);

            cmd.Parameters.AddWithValue("@" + column, key);

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();

            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
                cmd.Dispose();
            }

            return true;
        }

        /// <summary>
        /// Deletes all rows from a table that meet the criteria.
        /// </summary>
        /// <param name="table">The table the row is being deleted from.</param>
        /// <param name="column">The column that the key is being checked.</param>
        /// <param name="key">The key of the row being deleted.</param>
        /// <returns></returns>
        protected bool DeleteAll(string table, string column, object key) {
            OleDbCommand cmd = new OleDbCommand("DELETE * FROM " + table + " WHERE " + column + " = @" + column, conn);

            cmd.Parameters.AddWithValue("@" + column, key);

            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();

            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
                cmd.Dispose();
            }

            return true;
        }

        protected bool DeleteAll(string table, string[] columns, object[] keys) {

            string stringColumn = "";
            for (int i = 0; i < columns.Length - 1; i++) {
                stringColumn += columns[i] + " = @" + columns[i] + " AND "; 
            }
            stringColumn += columns[columns.Length - 1] + " = @" + columns[columns.Length - 1];

            OleDbCommand cmd = new OleDbCommand("DELETE * FROM " + table + " WHERE " + stringColumn, conn);

            for (int i = 0; i < columns.Length; i++) {
                cmd.Parameters.AddWithValue("@" + columns[i], keys[i]);
            }


            if (conn.State == ConnectionState.Closed && !forceOpen) conn.Open();

            try {
                cmd.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            } finally {
                if (conn.State == ConnectionState.Open && !forceOpen) conn.Close();
                cmd.Dispose();
            }

            return true;
        }

        #endregion

        #region Search / Get

        protected DataTable GetMax (string table, string column) {
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table + " Where " + column + " =(SELECT MAX(" + column + ") FROM " + table + ")", conn);

            //cmd.Parameters.AddWithValue("@Key", key);

            return GetData(cmd);
        }

        protected DataTable GetMin(string table, string column) {
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table + " Where " + column + " =(SELECT MIN(" + column + ") FROM " + table + ")", conn);

            //cmd.Parameters.AddWithValue("@Key", key);

            return GetData(cmd);
        }

        /// <summary>
        /// Returns a DataTable.
        /// </summary>
        /// <param name="commandText">The command text that determines what is in the datatable.</param>
        /// <returns>The created DataTable</returns>
        protected DataTable GetData(OleDbCommand commandText) {
            OleDbDataAdapter da = new OleDbDataAdapter(commandText);
            DataTable dt = new DataTable();

            da.Fill(dt);
            return dt;
        }

        public enum SearchKeyOperator { EqualTo, GreaterThanOrEqualTo, LessThanOrEqualTo, GreaterThan, LessThan }

        /// <summary>
        /// Searches a table.
        /// </summary>
        /// <param name="table">The table being searched.</param>
        /// <param name="keyColumn">The column the key is checked in.</param>
        /// <param name="key">The key.</param>
        /// <param name="op">How the key is compared.</param>
        /// <returns></returns>
        protected DataTable Search(string table, string keyColumn, object key, SearchKeyOperator op = SearchKeyOperator.EqualTo) {
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table + " WHERE " + keyColumn + " " + GetOperator(op) + " @Key", conn);

            cmd.Parameters.AddWithValue("@Key", key);

            return GetData(cmd);
        }

        /// <summary>
        /// Searches a table.
        /// </summary>
        /// <param name="table">The table being searched.</param>
        /// <param name="keyColumn">The column the key is checked in.</param>
        /// <param name="key">The key.</param>
        /// <param name="op">How the key is compared.</param>
        /// <returns></returns>
        protected DataTable Search(string table, string keyColumn, object[] keys, SearchKeyOperator op = SearchKeyOperator.EqualTo) {

            string stringKey = string.Empty;
            for (int i = 0; i < keys.Length - 1; i++) {
                stringKey += keyColumn + " " + GetOperator(op) + " @Key" + i + " OR ";
            }
            stringKey += keyColumn + " " + GetOperator(op) + " @Key" + (keys.Length - 1);

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table + " WHERE " + stringKey, conn);

            for (int i = 0; i < keys.Length; i++) {
                cmd.Parameters.AddWithValue("@Key", keys[i]);

            }

            return GetData(cmd);
        }

        /// <summary>
        /// Searches a table.
        /// </summary>
        /// <param name="table">The table being searched.</param>
        /// <param name="keyColumns">The columns the keys are being checked in.</param>
        /// <param name="keys">The keys.</param>
        /// <param name="op">How the key is compared.</param>
        /// <returns></returns>
        protected DataTable Search(string table, string[] keyColumns, object[] keys, SearchKeyOperator op = SearchKeyOperator.EqualTo) {
            string keyString = "";
            for (int i = 0; i < keyColumns.Length - 1; i++) {
                keyString += keyColumns[i] + " " + GetOperator(op) + " @Key" + i  + " AND ";
            }
            keyString += keyColumns[keyColumns.Length - 1] + " " + GetOperator(op) + " @Key" + (keyColumns.Length - 1);

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table + " WHERE " + keyString, conn);


            for (int i = 0; i < keyColumns.Length; i++) {
                cmd.Parameters.AddWithValue("@Key", keys[i]);
            }


            return GetData(cmd);
        }

        /// <summary>
        /// Searches a table.
        /// </summary>
        /// <param name="table">The table being searched.</param>
        /// <returns></returns>
        protected DataTable Search(string table) {
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table, conn);
            return GetData(cmd);
        }

        /// <summary>
        /// Searches multiple tables.
        /// </summary>
        /// <param name="tables">All the tables.</param>
        /// <param name="linkColumn">What column the tables are linked together with.</param>
        /// <returns></returns>
        protected DataTable Search(string[] tables, string linkColumn) {
            string tableString = string.Join(",", tables);
            string keyString = (tables.Length > 1) ? " WHERE " : "";
            for (int i = 1; i < tables.Length; i++) {
                keyString += tables[i - 1] + "." + linkColumn + " = " + tables[i] + "." + linkColumn + ((i >= 1 && i < tables.Length - 1) ? " AND " : "");
            }
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + tableString + keyString, conn);

            return GetData(cmd);
        }

        /// <summary>
        /// Searches multiple tables.
        /// </summary>
        /// <param name="tables">All the tables.</param>
        /// <param name="keyColumn">The column the key is checked in and linked what all the tables are linked with.</param>
        /// <param name="key">The key.</param>
        /// <param name="op">How the key is compared.</param>
        /// <returns></returns>
        protected DataTable Search(string[] tables, string keyColumn, object key, SearchKeyOperator op = SearchKeyOperator.EqualTo) {
            string tableString = string.Join(",", tables);
            string keyString = " WHERE ";
            for (int i = 1; i < tables.Length; i++) {
                keyString += tables[i - 1] + "." + keyColumn + " = " + tables[i] + "." + keyColumn + " AND ";
            }
            keyString += tables[0] + "." + keyColumn + " " + GetOperator(op) + " @Key";

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + tableString + keyString, conn);

            cmd.Parameters.AddWithValue("@Key", key);

            return GetData(cmd);
        }

        /// <summary>
        /// Searches multiple tables.
        /// </summary>
        /// <param name="tables">All the tables.</param>
        /// <param name="linkColumn">What column the tables are linked together with.</param>
        /// <param name="keyTable">The table the key is checked in.</param>
        /// <param name="keyColumn">The column the key is checked in.</param>
        /// <param name="key">The key.</param>
        /// <param name="op">How the key is compared.</param>
        /// <returns></returns>
        protected DataTable Search(string[] tables, string linkColumn, string keyTable, string keyColumn, object key, SearchKeyOperator op = SearchKeyOperator.EqualTo) {
            string tableString = string.Join(",", tables);
            string keyString = " WHERE ";
            for (int i = 1; i < tables.Length; i++) {
                keyString += tables[i - 1] + "." + linkColumn + " = " + tables[i] + "." + linkColumn + " AND ";
            }
            keyString += keyTable + "." + keyColumn + " " + GetOperator(op) + " @Key";

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + tableString + keyString, conn);

            cmd.Parameters.AddWithValue("@Key", key);

            return GetData(cmd);
        }

        /// <summary>
        /// Searches multiple tables with multiple keys.
        /// </summary>
        /// <param name="tables">All the tables.</param>
        /// <param name="linkColumn">What column the tables are linked together with.</param>
        /// <param name="keyTables">The tables with the keys.</param>
        /// <param name="keyColumns">The columns with the keys.</param>
        /// <param name="keys">The keys.</param>
        /// <param name="op">How the keys are compared.</param>
        /// <returns></returns>
        protected DataTable Search(string[] tables, string linkColumn, string[] keyTables, string[] keyColumns, object[] keys, SearchKeyOperator[] op) {
            string tableString = string.Join(",", tables);
            string keyString = " WHERE ";
            for (int i = 1; i < tables.Length; i++) {
                keyString += tables[i - 1] + "." + linkColumn + " = " + tables[i] + "." + linkColumn + " AND ";
            }
            for (int i = 0; i < keyTables.Length - 1; i++) {
                keyString += keyTables[i] + "." + keyColumns[i] + " " + GetOperator(op[i]) + " @Key" + i + " AND ";
            }
            keyString += keyTables[keyTables.Length - 1] + "." + keyColumns[keyTables.Length - 1] + " " + GetOperator(op[keyTables.Length - 1]) + " @Key" + (keyTables.Length - 1);

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + tableString + keyString, conn);

            for (int i = 0; i < keys.Length; i++) {
                cmd.Parameters.AddWithValue("@Key" + i, keys[i]);
            }

            return GetData(cmd);
        }

        /// <summary>
        /// Does a fuzzy search; doesn't return exacts, just somehwat close results.
        /// </summary>
        /// <param name="table">The table being searched.</param>
        /// <param name="keyColumn">The column the key is checked in.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        protected DataTable SearchFuzzy(string table, string keyColumn, object key) {
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + table + " WHERE " + keyColumn + " LIKE @Key", conn);

            cmd.Parameters.AddWithValue("@Key", key + "%");

            return GetData(cmd);
        }


        /// <summary>
        /// Does a fuzzy search; doesn't return exacts, just somehwat close results.
        /// </summary>
        /// <param name="table">The tables being searched.</param>
        /// <param name="linkColumn">What column the tables are linked together with.</param>
        /// <param name="keyTable">The table the key is checked in.</param>
        /// <param name="keyColumn">The column the key is checked in.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        protected DataTable SearchFuzzy(string[] tables, string linkColumn, string keyTable, string keyColumn, object key) {
            string tableString = string.Join(",", tables);
            string keyString = " WHERE ";
            for (int i = 1; i < tables.Length; i++) {
                keyString += tables[i - 1] + "." + linkColumn + " = " + tables[i] + "." + linkColumn + " AND ";
            }
            
            keyString += keyTable + "." + keyColumn + " LIKE @Key";

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + tableString + keyString, conn);

            cmd.Parameters.AddWithValue("@Key", key + "%");

            return GetData(cmd);
        }

        /// <summary>
        /// Converts SearchKeyOperator to a string.
        /// </summary>
        /// <param name="keyOperator"></param>
        /// <returns></returns>
        private string GetOperator (SearchKeyOperator keyOperator) {
            switch (keyOperator) {
                case SearchKeyOperator.EqualTo:
                    return "=";
                case SearchKeyOperator.GreaterThanOrEqualTo:
                    return ">=";
                case SearchKeyOperator.LessThanOrEqualTo:
                    return "<=";
                case SearchKeyOperator.GreaterThan:
                    return ">";
                case SearchKeyOperator.LessThan:
                    return "<";
                default:
                    return "";
            }
        }

        #endregion

        ///// <summary>
        ///// Creates an Access Database programmatically. Will we need to do this?
        ///// </summary>
        //private void CreateAccessDatabase()
        //{
        //    ADOX.Catalog cat = new ADOX.Catalog();

        //    cat.Create("Provider=Microsoft.Jet.OLEDB.4.0;" +
        //       @"Data Source=F:\ConsoleApp2\NewMDB.accdb;" +
        //       "Jet OLEDB:Engine Type=5");

        //    Console.WriteLine("Database Created Successfully");

        //    cat = null;
        //}

    }
}
