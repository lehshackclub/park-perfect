﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class VehicleToPerson {
        
        public VehicleToPerson(string aPersonID, string aPlate) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiate yet. Please Instantiate first");

            PersonID = aPersonID;
            Plate = aPlate;
        }

        private string myPersonID;
        public string PersonID {
            get { return myPersonID; }
            set { myPersonID = value; }
        }

        private string myPlate;
        public string Plate {
            get { return myPlate; }
            set { myPlate = value; }
        }
    }
}
