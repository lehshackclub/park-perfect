﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class Citation {

        /// <summary>
        /// Creates a citation data container
        /// </summary>
        /// <param name="aPersonID">The PersonID of the person affiliated with the citation.</param>
        /// <param name="aCitationName">The name of the citation. Primary key.</param>
        /// <param name="aInfo">Information about the citation.</param>
        public Citation(string aPersonID, string aCitationName, string aInfo) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            PersonID = aPersonID;
            CitationName = aCitationName;
            CitationInfo = aInfo;
        }
        /// <summary>
        /// The PersonID of the person affiliated with the citation
        /// </summary>
        public string PersonID { get; set; }

        /// <summary>
        /// The name of the citation.
        /// </summary>
        public string CitationName { get; set; }

        /// <summary>
        /// Information about the citation.
        /// </summary>
        public string CitationInfo { get; set; }

    }
}
