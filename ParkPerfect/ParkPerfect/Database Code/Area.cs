﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class Area {

        /// <summary>
        /// Creates an area data container.
        /// </summary>
        /// <param name="aName">The name of the area. The primary key.</param>
        /// <param name="aZoneName">The name of the zone the area is in.</param>
        /// <param name="aSpacing">The spacing of the area.</param>
        /// <param name="aRows">The number of rows in the area.</param>
        public Area(string aName, string aZoneName, int aSpacing, int aRows) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            AreaName = aName;
            ZoneName = aZoneName;
            Spacing = aSpacing;
            Rows = aRows;
        }
        /// <summary>
        /// The name of the area. The primary key.
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// The name of the zone the area is in.
        /// </summary>
        private string myZoneName;
        public string ZoneName {
            get { return myZoneName; }
            set { myZoneName = value; }
        }

        /// <summary>
        /// The spacing of the area.
        /// </summary>
        private int mySpacing;
        public int Spacing {
            get { return mySpacing; }
            set { mySpacing = value; }
        }

        /// <summary>
        /// The number of rows in the area.
        /// </summary>
        private int myRows;
        public int Rows {
            get { return myRows; }
            set { myRows = value; }
        }
    }
}
