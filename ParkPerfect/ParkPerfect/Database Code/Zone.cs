﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect {
    class Zone {

        public Zone(string aName, Color aColor) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            Name = aName;
            Color = aColor;
        }

        public string Name { get; set; }
        public Color Color { get; set; }

    }
}
