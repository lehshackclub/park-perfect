﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class AreaPoint {

        /// <summary>
        /// Creates a area point.
        /// </summary>
        /// <param name="aAreaName">The name of the area the area point is affiliated.</param>
        /// <param name="pointID">The id of the point relative to the spot.</param>
        /// <param name="aX">The x coordinate.</param>
        /// <param name="aY">The y coordinate.</param>
        public AreaPoint(string aAreaName, string pointID, double aX, double aY) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            X = aX;
            Y = aY;
            AreaName = aAreaName;
            myPointID = pointID;
        }

        /// <summary>
        /// The number of the spot the spot point is affiliated.
        /// </summary>
        string myPointID;
        public string PointID {
            get { return myPointID; }
            set { myPointID = value; }
        }

        /// <summary>
        /// The name of the area the area point is affiliated.
        /// </summary>
        string myAreaName;
        public string AreaName {
            get { return myAreaName; }
            set { myAreaName = value; }
        }

        /// <summary>
        /// The x coordinate.
        /// </summary>
        double myX;
        public double X {
            get { return myX; }
            set { myX = value; }
        }

        /// <summary>
        /// The y coordinate.
        /// </summary>
        double myY;
        public double Y {
            get { return myY; }
            set { myY = value; }
        }

    }
}
