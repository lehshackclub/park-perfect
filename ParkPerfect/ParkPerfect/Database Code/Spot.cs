﻿using System;

namespace ParkPerfect {
    public class Spot {

        /// <summary>
        /// Creates a Spot data container
        /// </summary>
        /// <param name="aSpotNumber">The spot number. Primary key.</param>
        /// <param name="aSpotType">The type of the spot.</param>
        /// <param name="aAreaName">The name of the area the spot is in.</param>
        /// <param name="aZoneName">The name of the zone the spot is in.</param>
        /// <param name="assigned">Whether the spot is assigned or not.</param>
        public Spot(string aSpotNumber, SpotTypes aSpotType, string aAreaName, string aZoneName) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            SpotID = aSpotNumber;
            SpotType = aSpotType;
            AreaName = aAreaName;
            ZoneName = aZoneName;
        }

        /// <summary>
        /// The spot number. Primary key.
        /// </summary>
        public string SpotID { get; set; }

        public enum SpotTypes { Normal, Guest, Handicap, NotAssigned, NonAssignable }
        /// <summary>
        /// The type of the spot.
        /// </summary>
        public SpotTypes SpotType { get; set; }

        /// <summary>
        /// The name of the area the spot is in.
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// The name of the zone the spot is in.
        /// </summary>
        public string ZoneName { get; set; }

    }
}
