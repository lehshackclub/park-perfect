﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkPerfect {
    class SpotPoint {

        /// <summary>
        /// Creates a spot point.
        /// </summary>
        /// <param name="aSpotNumber">The number of the spot the spot point is affiliated.</param>
        /// <param name="pointID">The id of the point relative to the spot.</param>
        /// <param name="aX">The x coordinate.</param>
        /// <param name="aY">The y coordinate.</param>
        public SpotPoint(string aSpotNumber, string pointID, double aX, double aY) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            X = aX;
            Y = aY;
            SpotID = aSpotNumber;
            myPointID = pointID;
        }

        /// <summary>
        /// The number of the spot the spot point is affiliated.
        /// </summary>
        string myPointID;
        public string PointID {
            get { return myPointID; }
            set { myPointID = value; }
        }

        /// <summary>
        /// The id of the point relative to the spot.
        /// </summary>
        string mySpotNumber;
        public string SpotID {
            get { return mySpotNumber; }
            set { mySpotNumber = value; }
        }

        /// <summary>
        /// The x coordinate.
        /// </summary>
        double myX;
        public double X {
            get { return myX; }
            set { myX = value; }
        }

        /// <summary>
        /// The y coordinate.
        /// </summary>
        double myY;
        public double Y {
            get { return myY; }
            set { myY = value; }
        }

    }
}
