﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.OleDb;


namespace ParkPerfect {

    /// <summary>
    /// Helper methods for DBParkingLot. 
    /// Methods that assist with program and user interface code.
    /// </summary>
    partial class DBParkingLot {

        public bool AssignAllSpots (bool assign) {
            Update("ParkingSpots", "Assigned", assign);
            return false;
        }

        public int NextTagID () {
            DataTable dt = GetMax("ParkingSpotTags", "TagID");
            SpotTag[] tag  =  ConvertToSpotTag(dt);

            return (tag.Length <= 0) ? 0 : tag[0].TagID;
        }

        public DataTable Report (AllColumns[] columns) {

            string str = "SELECT * FROM People, Vehicles, VehiclesToPeople WHERE People.PersonID = VehiclesToPeople.PersonID AND Vehicles.Plate = VehiclesToPeople.Plate";

            OleDbCommand cmd = new OleDbCommand(str, conn);

            DataTable dt = GetData(cmd);

            for (int i = 0; i < dt.Columns.Count; i++) {
                try {
                    dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Substring(dt.Columns[i].ColumnName.IndexOf('.') + 1);
                    AllColumns test = (AllColumns)Enum.Parse(typeof(AllColumns), dt.Columns[i].ColumnName);
                    if (!columns.Contains(test)) {
                        dt.Columns.RemoveAt(i);
                        i--;
                    }
                } catch (DuplicateNameException) {
                    dt.Columns.RemoveAt(i);
                    i--;
                } catch (ArgumentException) {
                    dt.Columns.RemoveAt(i);
                    i--;
                }
            }

            return dt;
        }

    }
}
