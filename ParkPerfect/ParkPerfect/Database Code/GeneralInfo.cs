﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect {
    class GeneralInfo {

        public GeneralInfo(string aSchoolName, Image aImage, Image aLogo) {
            if (DBParkingLot.instance == null) new FormatException("The database manager has not been instantiated yet. Please Instantiate first");

            SchoolName = aSchoolName;
            Image = aImage;
            Logo = aLogo;
        }
        public string SchoolName { get; set; }
        public Image Image { get; set; }
        public Image Logo { get; set; }
    }
}
