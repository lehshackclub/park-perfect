﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingLotProject
{
    public class Student
    {
        private String myName;
        private String mySpotNumber;
        private String myCar;
        private String myLicensePlate;

        public Student()
        {

        }

        public Student(String aName, String aSpotNumber, String aCar, String aLicensePlate)
        {
            myName = aName;
            mySpotNumber = aSpotNumber;
            myCar = aCar;
            myLicensePlate = aLicensePlate;
        }

        #region Getter Methods
        public String getName()
        {
            return myName;
        }

        public String getSpotNumber()
        {
            return mySpotNumber;
        }

        public String getCar()
        {
            return myCar;
        }

        public String getLicensePlates ()
        {
            return myLicensePlate;
        }
        #endregion

        #region Setter Methods
        public void setName(String aName)
        {
            myName = aName;
        }

        public void setSpotNumber(String aSpotNumber)
        {
            mySpotNumber = aSpotNumber;
        }

        public void setCar(String aCar)
        {
            myCar = aCar;
        }

        public void setLicensePlate(String aLicensePlate)
        {
            myLicensePlate = aLicensePlate;
        }
        #endregion
    }
}
