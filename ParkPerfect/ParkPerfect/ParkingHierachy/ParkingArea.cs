﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace ParkPerfect
{
    public class ParkingArea
    {
        public Point startingPoint;
        private ParkingZone myZone;
        private Form1 f1;

        private string myName;
        public string Name
        {
            get { return myName; }
            set { myName = value; }
        }

        private int mySpacing;
        public int Spacing
        {
            get { return mySpacing; }
            set { mySpacing = value; }
        }

        private int myRows;
        public int Rows
        {
            get { return myRows; }
            set { myRows = value; }
        }

        public ParkingArea(string aName, Point startPoint, ParkingZone aZone, bool isCreated, int newRows, int NewsingOrDub, Form1 f)
        {
            Name = aName;
            startingPoint = startPoint;
            myZone = aZone;
            myRows = newRows;
            Spacing = NewsingOrDub;
            bool areaExists = DBParkingLot.instance.SearchForArea(DBParkingLot.AreaSearchCriteria.AreaName, Name) == null;
            if (!isCreated) { userChoice(); }
            if (Spacing == 3)
            {
                return;
            }
            f1 = f;
            if (areaExists) { generateArea(); }
            loadArea();
        }

        private void userChoice()
        {
            List<string> strs = new List<string>();
            strs.Add("Single");
            strs.Add("Double");
            SelectionForm SOD = new SelectionForm("Single or Double Spaced?", strs);

            InputForm ROWS = new InputForm("How many rows will there be?"); // create prompt for row amount

            List<string> strs2 = new List<string>();
            strs2.Add("Vertical");
            strs2.Add("Horizontal");
            SelectionForm SOD2 = new SelectionForm("Vertical or Horizontal?", strs2);
            SOD.ShowDialog();
            Spacing = Convert.ToInt32(SOD.getData());
            if (Spacing == 3)
            {
                return;
            }
            int value;
            do
            {
                ROWS.ShowDialog();
                if (!(int.TryParse(ROWS.getData(), out value)))
                {
                    MessageBox.Show("Please enter a whole number!");
                }
            } while (!(int.TryParse(ROWS.getData(), out value)));
            myRows = Convert.ToInt32(ROWS.getData());

            SOD2.ShowDialog();
            int orientation = Convert.ToInt32(SOD2.getData());

            #region Adds Area/AreaPoints to the database.
            if (orientation == 1)//Vertical
            {
                if (Spacing == 1)//Single
                {
                    Area aArea = new Area(myZone.Name + myZone.parkingAreas.Count, myZone.Name, Spacing, Rows);
                    DBParkingLot.instance.InsertArea(aArea);

                    AreaPoint ap1 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 1.ToString(), startingPoint.X, startingPoint.Y);
                    AreaPoint ap2 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 2.ToString(), startingPoint.X + Style.spotWidth, startingPoint.Y + Style.spotHeight * Rows);

                    DBParkingLot.instance.InsertAreaPoint(ap1);
                    DBParkingLot.instance.InsertAreaPoint(ap2);
                }
                else if (Spacing == 2)//Double
                {
                    Area aArea = new Area(myZone.Name + myZone.parkingAreas.Count, myZone.Name, Spacing, Rows);
                    DBParkingLot.instance.InsertArea(aArea);

                    AreaPoint ap1 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 1.ToString(), startingPoint.X, startingPoint.Y);
                    AreaPoint ap2 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 2.ToString(), startingPoint.X + (Style.spotWidth * 2), startingPoint.Y + Style.spotHeight * Rows);

                    DBParkingLot.instance.InsertAreaPoint(ap1);
                    DBParkingLot.instance.InsertAreaPoint(ap2);
                }
                else
                {

                }
            }
            else //Horizontal
            {
                if(Spacing == 1)//Single
                {
                    Area aArea = new Area(myZone.Name + myZone.parkingAreas.Count, myZone.Name, Spacing, Rows);
                    DBParkingLot.instance.InsertArea(aArea);

                    AreaPoint ap1 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 1.ToString(), startingPoint.X, startingPoint.Y);
                    AreaPoint ap2 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 2.ToString(), startingPoint.X + (Style.spotHeight * Rows), Style.spotWidth);

                    DBParkingLot.instance.InsertAreaPoint(ap1);
                    DBParkingLot.instance.InsertAreaPoint(ap2);

                }
                else if (Spacing == 2) //Double
                {
                    Area aArea = new Area(myZone.Name + myZone.parkingAreas.Count, myZone.Name, Spacing, Rows);
                    DBParkingLot.instance.InsertArea(aArea);

                    AreaPoint ap1 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 1.ToString(), startingPoint.X, startingPoint.Y);
                    AreaPoint ap2 = new AreaPoint(myZone.Name + myZone.parkingAreas.Count, 2.ToString(), startingPoint.X + (Style.spotHeight * Rows), Style.spotWidth * 2);

                    DBParkingLot.instance.InsertAreaPoint(ap1);
                    DBParkingLot.instance.InsertAreaPoint(ap2);

                }
                else
                {

                }

            }

            #endregion

        }

        /// <summary>
        /// Pulls the spots out of the DB to get the info about them, makes a rectangle out of the 2 points in the DB and then puts it into the program
        /// </summary>
        public void loadArea()
        {
            spots.Clear();
            DBParkingLot.instance.ForceOpenConnection();
            Spot[] tempSpots = DBParkingLot.instance.SearchForSpots(DBParkingLot.ParkingSpotSearchCriteria.AreaName, Name); //gets all the parking spots in the area
            Rectangle tempRect;
            for (int i = 0; i < tempSpots.Length; i++) //foreach spot in the area, load them into the program
            {
                SpotPoint[] s = DBParkingLot.instance.SearchForSpotPoints(DBParkingLot.SpotPointSearchCriteria.SpotID, tempSpots[i].SpotID);
                try
                {
                    tempRect = new Rectangle( //This logic is just getting the points from the db and turning them into a rectangle
                                (int)s[0].X,
                                (int)s[0].Y,
                                (int)(s[1].X - s[0].X),
                                (int)(s[1].Y - s[0].Y));
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("Error Code: 001", "CRITICAL ERROR");
                    Environment.Exit(0);
                    throw;
                }
                ParkingSpot newSpot =
                    new ParkingSpot(
                        tempSpots[i].SpotType,
                        tempSpots[i].SpotID,
                        DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, tempSpots[i].SpotID),
                        tempRect);
                spots.Add(newSpot);
            }
            DBParkingLot.instance.ForceCloseConnection();
        }

        public void generateArea() {

            DBParkingLot.instance.ForceOpenConnection();

            int nextSpotNum = DBParkingLot.instance.SearchForSpots().Length + 1; //This should start counting after the total number of spots
            while (DBParkingLot.instance.SearchForSpots(DBParkingLot.ParkingSpotSearchCriteria.SpotID, nextSpotNum).Count() > 0)
            {
                nextSpotNum++; //This prevents it from using an existing spot number
            }
            if (nextSpotNum == 0) { nextSpotNum = 1; }

            //space generation
            if (getOrientation() == 1) //if vertical
            {
                int currenty = startingPoint.Y;
                if (Spacing == 1) //if single
                {
                    for (int i = 1; i <= myRows; i++) //Goes through each row
                    {
                        DBParkingLot.instance.InsertSpot(myName, myZone.Name, nextSpotNum.ToString(), 0); //insert the spot into the DB
                        Rectangle temp = new Rectangle(startingPoint.X, currenty, Style.spotWidth, Style.spotHeight); //Create a temporary rectangle that represents our spot
                        //Insert our 2 spot points that represent a rectangle into the DB
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 1.ToString(), temp.X, temp.Y);
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 2.ToString(), temp.X + temp.Width, temp.Y + temp.Height);

                        currenty += Style.spotHeight;
                        nextSpotNum++;
                    }
                }
                else //if double
                {
                    int nextX = 0;
                    for (int i = 1; i <= myRows * Spacing; i++)
                    {
                        if (i % 2 == 1) { nextX = startingPoint.X; } //if on left
                        else { nextX = startingPoint.X + Style.spotWidth; }//if on the right side
                        DBParkingLot.instance.InsertSpot(myName, myZone.Name, nextSpotNum.ToString(), 0); //Insert the spot into the database
                        Rectangle temp = new Rectangle(nextX, currenty, Style.spotWidth, Style.spotHeight); //temporary rectangle that represents our spot
                        //Insert our 2 spot points that represent a rectangle in the DB
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 1.ToString(), temp.X, temp.Y);
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 2.ToString(), temp.X + temp.Width, temp.Y + temp.Height);

                        if (i % 2 == 0) { currenty += Style.spotHeight; }
                        nextSpotNum++;
                    }
                }
            }
            else if (getOrientation() == 2) // if horizontal
            {
                int currentx = startingPoint.X;
                if (Spacing == 1)//if single
                {
                    for (int i = 1; i <= myRows; i++)
                    {
                        DBParkingLot.instance.InsertSpot(myName, myZone.Name, nextSpotNum.ToString(), 0); //Insert the spot into DB
                        Rectangle temp = new Rectangle(currentx, startingPoint.Y, Style.spotHeight, Style.spotWidth); //Temp rectangle that represents our spot
                        //insert our two spots into DB
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 1.ToString(), temp.X, temp.Y);
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 2.ToString(), temp.X + temp.Width, temp.Y + temp.Height);

                        currentx += Style.spotWidth;
                        nextSpotNum++;
                    }
                }
                else //if double
                {
                    int nextY = 0;
                    for (int i = 1; i <= myRows * Spacing; i++)
                    {
                        if (i % 2 == 1) { nextY = startingPoint.Y; } //if on top
                        else { nextY = startingPoint.Y + Style.spotHeight; }//if on the bottom
                        DBParkingLot.instance.InsertSpot(myName, myZone.Name, nextSpotNum.ToString(), 0); //insert spot into the db
                        Rectangle temp = new Rectangle(currentx, nextY, Style.spotHeight, Style.spotWidth); //temp rectangle for our spot
                        //insert our two spots into the DB
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 1.ToString(), temp.X, temp.Y);
                        DBParkingLot.instance.InsertSpotPoint(nextSpotNum.ToString(), 2.ToString(), temp.X + temp.Width, temp.Y + temp.Height);

                        if (i % 2 == 0) { currentx += Style.spotHeight; }
                        nextSpotNum++;
                    }
                }
            }
            f1.CurrentSpot = nextSpotNum;

            foreach (ParkingSpot sp in spots)
            {
                Person p = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, sp.SpotNumber);
                sp.Person = (p != null) ? p : null;
            }

            DBParkingLot.instance.ForceCloseConnection();
        }

        public void draw(Graphics g)
        {
            APBox rectangle = getRectangle();
            int rowIncrement = 0;
            int orientation = 0;
            Pen temppen = new Pen(Style.AreaLineColor, Style.AreaLineWidth);

            if (getOrientation() == 1) //if vertical
            {
                rowIncrement = rectangle.getHeight() / myRows; //We want the rows to be divided up based on the heihgt
                orientation = 1; //Denotates it as being vertical
            }
            else if (getOrientation() == 2) //if horizontal
            {
                rowIncrement = rectangle.getWidth() / myRows; //We want the rows to be divided up based on width
                orientation = 2; //Denotates it as being horizontal
            }

            if (orientation == 1)//if vertical
            {
                if (Spacing == 1) //if single
                {
                    int rowsPainted = 0; //Starts off with 0 rows painted obviously
                    int currenty = rectangle.getY1(); //currenty will keep track of how far down we have progressed in drawing the area
                    while (rowsPainted <= myRows) //While the rows that have been painted is less than or equal to the amount of rows we have to paint
                    {
                        g.DrawLine(temppen, rectangle.getX1(), currenty, rectangle.getX2(), currenty);
                        currenty += rowIncrement;
                        rowsPainted++;
                    }
                }
                else if (Spacing == 2)//if double
                {
                    int rowsPainted = 0;
                    int currenty = rectangle.getY1();
                    g.DrawLine(temppen, rectangle.getX1() + (rectangle.getWidth() / 2), rectangle.getY1(), rectangle.getX1() + (rectangle.getWidth() / 2), rectangle.getY1() + (myRows * rowIncrement));
                    while (rowsPainted <= myRows)
                    {
                        g.DrawLine(temppen, rectangle.getX1(), currenty, rectangle.getX2(), currenty);
                        currenty += rowIncrement;
                        rowsPainted++;
                    }
                }
            }
            else if (orientation == 2) //if horizontal
            {
                if (Spacing == 1) //if single
                {
                    int rowsPainted = 0;
                    int currentx = rectangle.getX1();
                    while (rowsPainted <= myRows)
                    {
                        g.DrawLine(temppen, currentx, rectangle.getY1(), currentx, rectangle.getY2());
                        currentx += rowIncrement;
                        rowsPainted++;
                    }
                }
                else if (Spacing == 2) //if double
                {
                    int rowsPainted = 0;
                    int currentx = rectangle.getX1();
                    g.DrawLine(temppen, rectangle.getX1(), rectangle.getY1() + (rectangle.getHeight() / 2), rectangle.getX1() + (myRows * rowIncrement), rectangle.getY1() + (rectangle.getHeight() / 2));
                    while (rowsPainted <= myRows)
                    {
                        g.DrawLine(temppen, currentx, rectangle.getY1(), currentx, rectangle.getY2());
                        currentx += rowIncrement;
                        rowsPainted++;
                    }
                }
            }
        }

        private int getOrientation()   //1 = Vertical, 2 = Horizontal, 3 = Square
        {
            APBox rect = getRectangle();
            
            int orient = 0;
            if (rect.getWidth() < rect.getHeight())
            {
                orient = 1;
            }
            else if (rect.getWidth() > rect.getHeight())
            {
                orient = 2;
            }
            else
            {
                orient = 3;
            }

            return orient;
        }

        public void updatePoint1(int x, int y)
        {
            DBParkingLot.AreaPointSearchCriteria[] crit = new DBParkingLot.AreaPointSearchCriteria[] { DBParkingLot.AreaPointSearchCriteria.AreaName, DBParkingLot.AreaPointSearchCriteria.PointID };
            object[] keys = new object[] {myName, 1 };
            DBParkingLot.instance.UpdateAreaPoint(crit, keys, new AreaPoint(myName, "1", x, y));
        }

        public void updatePoint2(int x, int y)
        {
            DBParkingLot.AreaPointSearchCriteria[] crit = new DBParkingLot.AreaPointSearchCriteria[] { DBParkingLot.AreaPointSearchCriteria.AreaName, DBParkingLot.AreaPointSearchCriteria.PointID };
            object[] keys = new object[] { myName, 2 };
            DBParkingLot.instance.UpdateAreaPoint(crit, keys, new AreaPoint(myName, "2", x, y));
        }

        public APBox getRectangle()
        {
            AreaPoint[] Points = DBParkingLot.instance.SearchForAreaPoints(DBParkingLot.AreaPointSearchCriteria.AreaName, myName);
            if (Points.Count() > 0)
            {
                APBox box = new APBox(Convert.ToInt32(Points[0].X), Convert.ToInt32(Points[0].Y), Convert.ToInt32(Points[1].X), Convert.ToInt32(Points[1].Y));
                return box;
            }
            else
            {
                return null;
            }
        }

        public List<ParkingSpot> spots { get; } = new List<ParkingSpot>();
    }
}
