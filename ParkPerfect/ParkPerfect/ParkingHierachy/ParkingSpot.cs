﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect
{
    public class ParkingSpot
    {
        public Color myHoverColor;
        private int mySpotSpacing;
        public Spot.SpotTypes typeOfSpot;
        Random r = new Random();

        public ParkingSpot(Spot.SpotTypes aSpotType, string aSpotNumber, Person aPerson, Rectangle r)
        {
            typeOfSpot = aSpotType;
            SpotNumber = aSpotNumber;
            Person = aPerson;
            rect = new APBox(r.X, r.Y, r.X + r.Width, r.Y + r.Height);
            mySpotSpacing = SpotNumber.ToString().Length * 10;
            myHoverColor = Style.SpotHoverColor;
        }

        public void HoverDraw(Graphics g)
        {
            g.FillRectangle(new SolidBrush(Color.FromArgb(110, myHoverColor)), new RectangleF(rect.getX1(), rect.getY1(), rect.getWidth(), rect.getHeight()));
        }

        public void Draw(Graphics g)
        {
            g.DrawString(SpotNumber.ToString(), Style.SpotFont, new SolidBrush(Style.SpotFontColor), new Point(rect.getX1(), rect.getY1()));
            bool wider = false;
            bool valid = false;
            if (Person != null && isVertical())
            {
                string tempLast = Person.LastName;
                while (!valid)
                {
                    if (g.MeasureString(tempLast, Style.SpotFont).Width + mySpotSpacing > rect.getWidth()) //if wider then the spot
                    {
                        tempLast = tempLast.Substring(0, tempLast.Length - 1);
                        wider = true;
                    }
                    else { valid = true; }
                }
                if (wider) { tempLast += "..."; }
                g.DrawString(tempLast, Style.SpotFont, new SolidBrush(Style.SpotFontColor), new Point(rect.getX1() + mySpotSpacing, rect.getY1()));
            }
        }

        private bool isVertical()
        {
            if(rect.getWidth() > rect.getHeight())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Person Person { get; set; }

        public APBox rect;

        public bool Showing { get; set; }

        public string SpotNumber { get; set; }
        
    }
}
