﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkingLotGUIPrototype {
    public partial class SearchForm : Form {
        public SearchForm() {
            InitializeComponent();
            DBParkingLot.SaveColumns[] cols = { DBParkingLot.SaveColumns.FirstName, DBParkingLot.SaveColumns.LastName, DBParkingLot.SaveColumns.PersonID, DBParkingLot.SaveColumns.Occupation }; ;
            DataTable dt = DBParkingLot.instance.ReportDataTable(cols);
            dataGridView1.DataSource = dt;

            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
            buttonColumn.Name = "";
            buttonColumn.Text = "Open Form";
            buttonColumn.UseColumnTextForButtonValue = true;
            int columnIndex = 0;
            if (dataGridView1.Columns["button_column"] == null) {
                dataGridView1.Columns.Insert(columnIndex, buttonColumn);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {
                var senderGrid = (DataGridView)sender;

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                    e.RowIndex >= 0) {
                    //TODO - Button Clicked - Execute Code Here
                    //label1.Text = senderGrid.Columns[e.ColumnIndex].row;
                    label1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();

                }
            }
    }
}
