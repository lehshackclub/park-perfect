﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect {
    public class ParkingZone {
        Form1 f1;

        public ParkingZone(List<Point> boundaries, string aName, Color aColor, Form1 f) {
            points = boundaries;
            color = aColor;
            parkingAreas = new List<ParkingArea>();
            poly = new APPolygon(aColor, points);
            Name = aName;
            f1 = f;

            if (aName != null) {
                #region Finds Area/AreaPoints in the database and turns them into ParkingAreas
                Area[] dbAreas = DBParkingLot.instance.SearchForAreas(DBParkingLot.AreaSearchCriteria.ZoneName, Name);

                for (int i = 0; i < dbAreas.Length; i++) {
                    AreaPoint[] points = DBParkingLot.instance.SearchForAreaPoints(DBParkingLot.AreaPointSearchCriteria.AreaName, dbAreas[i].AreaName);
                    parkingAreas.Add(new ParkingArea(dbAreas[i].AreaName, new Point(50, 50), this, true, dbAreas[i].Rows, dbAreas[i].Spacing, f1));
                }
                #endregion
            }

        }

        public void Reload () {
            //Start with a fresh array
            parkingAreas.Clear();

            #region Finds Area/AreaPoints in the database and turns them into ParkingAreas
            Area[] dbAreas = DBParkingLot.instance.SearchForAreas(DBParkingLot.AreaSearchCriteria.ZoneName, Name);

            for (int i = 0; i < dbAreas.Length; i++) {
                AreaPoint[] points = DBParkingLot.instance.SearchForAreaPoints(DBParkingLot.AreaPointSearchCriteria.AreaName, dbAreas[i].AreaName);
                parkingAreas.Add(new ParkingArea(dbAreas[i].AreaName, new Point(50, 50), this, true, dbAreas[i].Rows, dbAreas[i].Spacing, f1));
            }
            #endregion
        }

        public List<Point> points { get; }

        private string myName;
        public string Name {
            get { return myName; }
            set { myName = value; poly.setName(value); }
        }

        public Color color { get; set; }

        public List<ParkingArea> parkingAreas { get; }

        public void addArea(ParkingArea item) {
            parkingAreas.Add(item);
        }

        public APPolygon poly { get; }
    }
}
