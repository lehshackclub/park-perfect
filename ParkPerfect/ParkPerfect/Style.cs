﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ParkPerfect
{
    static class Style
    {
        static public float AreaLineWidth { get; set; } = 1;
        static public Color AreaLineColor { get; set; } = Color.Black;
        static public Color AreaBackgroundColor { get; set; } = Color.LightGray;
        static public bool AreaAutoAlign { get; set; } = true;
        static public Font ZoneFont { get; set; } = new Font("Senoe UI", 18, FontStyle.Bold);
        //static public Brush ZoneFontBrush { get; set; } = Brushes.Black;
        static public Color ZoneFontColor { get; set; } = Color.Black;
        static public float ZoneOutlineWidth { get; set; } = 5;
        static public Color SpotHoverColor { get; set; } = Color.Gray;
        static public Font SpotFont { get; set; } = SystemFonts.MenuFont;
        //static public Brush SpotFontBrush { get; set; } = Brushes.Black;
        static public Color SpotFontColor { get; set; } = Color.Black;
        static public int spotHeight = 0;
        static public int spotWidth = 0;
        
        public static void Load () {
            ZoneFont = Properties.Settings.Default.ZoneFont;
            ZoneFontColor = Properties.Settings.Default.ZoneFontColor;
            ZoneOutlineWidth = Properties.Settings.Default.ZoneFontOutlineWidth;
            AreaBackgroundColor = Properties.Settings.Default.AreaBackgroundColor;
            AreaLineColor = Properties.Settings.Default.AreaLineColor;
            AreaLineWidth = Properties.Settings.Default.AreaLineWidth;
            AreaAutoAlign = Properties.Settings.Default.AreaAutoAlign;
            SpotHoverColor = Properties.Settings.Default.SpotHoverColor;
            SpotFont = Properties.Settings.Default.SpotFont;
            SpotFontColor = Properties.Settings.Default.SpotFontColor;
            spotHeight = Properties.Settings.Default.spotHeight;
            spotWidth = Properties.Settings.Default.spotWidth;
        }

        public static void Save () {
            Properties.Settings.Default.ZoneFont = ZoneFont;
            Properties.Settings.Default.ZoneFontColor = ZoneFontColor;
            Properties.Settings.Default.ZoneFontOutlineWidth = ZoneOutlineWidth;
            Properties.Settings.Default.AreaBackgroundColor = AreaBackgroundColor;
            Properties.Settings.Default.AreaLineColor = AreaLineColor;
            Properties.Settings.Default.AreaLineWidth = AreaLineWidth;
            Properties.Settings.Default.AreaAutoAlign = AreaAutoAlign;
            Properties.Settings.Default.SpotHoverColor = SpotHoverColor;
            Properties.Settings.Default.SpotFont = SpotFont;
            Properties.Settings.Default.SpotFontColor = SpotFontColor;
            Properties.Settings.Default.spotHeight = spotHeight;
            Properties.Settings.Default.spotWidth = spotWidth;

            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }
    }
}
