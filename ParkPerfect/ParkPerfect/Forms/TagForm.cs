﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect {
    public partial class TagForm : Form {

        SpotTag formtag;
        string tagNamePrefix;
        int offset;

        public TagForm(SpotTag tag) {
            InitializeComponent();

            List<TagsToSpot> tagsToSpots = DBParkingLot.instance.SearchForTagsToSpot().ToList();

            tagsToSpots.OrderBy(tagtoSpot => tagtoSpot.SpotID);
            List<int[]> breaks = new List<int[]>();

            for (int i = 1; i < tagsToSpots.Count; i++) {
                //if (Math.Abs(tagsToSpots[i].SpotID - tagsToSpots[i - 1].SpotID) > 0)
                
                /*Because the spots aren't numbers and instead strings, I have no way
                 knowing if their ordering is constistent.*/
            }




            formtag = tag;
            tagNamePrefix = labelTagName.Text;

            labelTagName.Text = tagNamePrefix + " " + formtag.TagName;
            labelTagName.BackColor = formtag.TagColor;

            offset = buttonAddRange.Top - textBoxRange2.Bottom;

            for (int i = 0; i < 10; i++) {
                TextBox range1 = new TextBox();
                TextBox range2 = new TextBox();

                range1.Size = textBoxRange1.Size;
                range2.Size = textBoxRange2.Size;

                range1.Location = new Point(textBoxRange1.Location.X, textBoxRange1.Location.Y + (offset + range1.Size.Height) * i);
                range2.Location = new Point(textBoxRange2.Location.X, textBoxRange2.Location.Y + (offset + range2.Size.Height) * i);

                panel1.Controls.Add(range1);
                panel1.Controls.Add(range2);
            }
            buttonAddRange.Location = new Point(buttonAddRange.Location.X, buttonAddRange.Location.Y + (offset + textBoxRange2.Size.Height) * 9);

            textBoxRange1.Hide();
            textBoxRange2.Hide();
        }

        private void buttonChangeName_Click(object sender, EventArgs e) {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Please give a new tag name.", "Prompt", "", 0, 0);
            if (input.Trim() != string.Empty) {
                formtag.TagName = input.Trim();
                labelTagName.Text = tagNamePrefix + " "+ formtag.TagName;
            }
        }

        private void buttonChangeColor_Click(object sender, EventArgs e) {
            if (colorDialog.ShowDialog() == DialogResult.OK) {
                formtag.TagColor = colorDialog.Color;
            }
            labelTagName.BackColor = formtag.TagColor;
        }

        private void buttonAddRange_Click(object sender, EventArgs e) {

        }

        private void buttonSave_Click(object sender, EventArgs e) {
            DBParkingLot.instance.UpdateParkingSpotTag(DBParkingLot.ParkingSpotTagSearchCriteria.TagID, formtag.TagID, formtag);
        }
    }
}

