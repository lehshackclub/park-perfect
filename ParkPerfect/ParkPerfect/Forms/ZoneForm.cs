﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class ZoneForm : Form
    {
        private List<APBox> shapes = new List<APBox>();
        private bool drag = false;
        int x, y;
        Random randomGen = new Random();
        ParkingZone myZone;
        bool isDrawing = false;
        bool isDeleting;
        bool isRenumbering = false;
        Form1 f1;
        const int correctionMargin = 15; //This should prolly change depending on res
        ParkingArea currentArea;
        Timer timer1 = new Timer();
        ParkingSpot selectedSpot;

        //Moving Area Stuff
        bool movingArea = false;
        Point startingPoint;
        ParkingArea grabbedArea;
        int xOffset;
        int yOffset;

        //Renumbering stuff
        int startingSpotNum;
        ParkingSpot startingSpot;
        ParkingSpot endingSpot;

        public ZoneForm(ParkingZone zone, Form1 f)
        {
            InitializeComponent();
            Text = zone.Name;
            myZone = zone;
            f1 = f;
            canvas.BackColor = Style.AreaBackgroundColor;
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void canvas_MouseUp(object sender, MouseEventArgs e)
        {
            //If a grabbedArea exists, update the area to the new location
            if (grabbedArea != null)
            {
                //There will be 2 search critereas for the area points, area name and the point id's for its points
                DBParkingLot.AreaPointSearchCriteria[] crit = { DBParkingLot.AreaPointSearchCriteria.AreaName, DBParkingLot.AreaPointSearchCriteria.PointID };
                object[] keys = { grabbedArea.Name, 1 };
                object[] keys2 = { grabbedArea.Name, 2 };

                //Update the two area points
                DBParkingLot.instance.UpdateAreaPoint(crit, keys, new AreaPoint(grabbedArea.Name, 1.ToString(), grabbedArea.getRectangle().getX1(), grabbedArea.getRectangle().getY1()));
                DBParkingLot.instance.UpdateAreaPoint(crit, keys2, new AreaPoint(grabbedArea.Name, 2.ToString(), grabbedArea.getRectangle().getX2(), grabbedArea.getRectangle().getY2()));

                //Search for all the spots within the area and get their spot numbers
                Spot[] grabbedAreaSpots = DBParkingLot.instance.SearchForSpots(DBParkingLot.ParkingSpotSearchCriteria.AreaName, grabbedArea.Name);
                List<string> grabbedAreaSpotNums = new List<string>();
                foreach (var spot in grabbedAreaSpots)
                {
                    grabbedAreaSpotNums.Add(spot.SpotID);
                }

                //2 search critereas for the spot points, the spot number of them and the ID of the points
                DBParkingLot.SpotPointSearchCriteria[] crit2 = { DBParkingLot.SpotPointSearchCriteria.SpotID, DBParkingLot.SpotPointSearchCriteria.PointID };
                object[] keys3 = { null, 1 };
                object[] keys4 = { null, 2 };

                DBParkingLot.instance.ForceOpenConnection();

                //For each spot in the area, update them to the new area
                foreach (ParkingSpot spot in grabbedArea.spots)
                {
                    keys3[0] = spot.SpotNumber.ToString();
                    keys4[0] = spot.SpotNumber.ToString();

                    DBParkingLot.instance.UpdateParkingSpotPoint(crit2, keys3, new SpotPoint(spot.SpotNumber.ToString(), 1.ToString(), spot.rect.getX1(), spot.rect.getY1()));
                    DBParkingLot.instance.UpdateParkingSpotPoint(crit2, keys4, new SpotPoint(spot.SpotNumber.ToString(), 2.ToString(), spot.rect.getX2(), spot.rect.getY2()));
                }
                DBParkingLot.instance.ForceCloseConnection();
            }
            grabbedArea = null;
            movingArea = false;
            if (drag)
            {
                drag = false;
                APBox temp = shapes.Last();
                if (Style.AreaAutoAlign) //If autoalign is enabled
                {
                    for (int i = 0; i < myZone.parkingAreas.Count; i++) //foreach area
                    {
                        if (myZone.parkingAreas[i] != myZone.parkingAreas.Last()) //obv dont wanna compare to itself
                        {
                            //Does the align check for the top 
                            int lowBounds = temp.getY1() - correctionMargin;
                            int highBounds = temp.getY1() + correctionMargin;
                            int myY = temp.getY1();
                            if (myY < highBounds && myY > lowBounds) //If its within the bounds then it realizes ok you want to align the areas
                            {
                                temp.setY1(temp.getY1());
                            }

                            //Does the align check for the bottom
                            lowBounds = temp.getY2() - correctionMargin;
                            highBounds = temp.getY2() + correctionMargin;
                            myY = temp.getY2();
                            if (myY < highBounds && myY > lowBounds) //If its within the bounds then it realizes ok you want to align the areas
                            {
                                temp.setY2(temp.getY2());
                            }

                            if (temp.getY1() == temp.getY1() &&    //If both have been changed
                                temp.getY2() == temp.getY2())      //Then we will make the width the same
                            {
                                int widthDiff = temp.getWidth() - temp.getWidth();
                                temp.setX2(temp.getX2() + widthDiff);
                            }
                        }
                    }

                }
                //myZone.addArea(new ParkingArea(myZone.Name + (myZone.parkingAreas.Count + 1), temp, myZone, false, 0, 0, f1));
                if (myZone.parkingAreas.Last().Spacing == 3)
                {
                    myZone.parkingAreas.Remove(myZone.parkingAreas.Last());
                    shapes.Remove(shapes.Last());
                    return;
                }

                shapes.Remove(shapes.Last());
                showUserFeedback("Area Created!", 2500);
            }
            isDrawing = false;
            Refresh();
            myZone.Reload();
        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Point endingPoint = e.Location;

            int ex = endingPoint.X;
            int ey = endingPoint.Y;


            if (drag)
            {
                shapes.Last().setX2(ex);
                shapes.Last().setY2(ey);
            }

            if (grabbedArea != null)
            {
                //Modify the local stuff
                xOffset = endingPoint.X - startingPoint.X;
                yOffset = endingPoint.Y - startingPoint.Y;

                grabbedArea.updatePoint1(grabbedArea.getRectangle().getX1() + xOffset, grabbedArea.getRectangle().getY1() + yOffset);
                grabbedArea.updatePoint2(grabbedArea.getRectangle().getX2() + xOffset, grabbedArea.getRectangle().getY2() + yOffset);

                foreach (ParkingSpot spot in grabbedArea.spots)
                {
                    spot.rect.setX1(spot.rect.getX1() + xOffset);
                    spot.rect.setX2(spot.rect.getX2() + xOffset);
                    spot.rect.setY1(spot.rect.getY1() + yOffset);
                    spot.rect.setY2(spot.rect.getY2() + yOffset);
                }
            }

            foreach (var area in myZone.parkingAreas)
            {
                foreach (var spot in area.spots)
                {
                    Rectangle temp = new Rectangle(spot.rect.getX1(), spot.rect.getY1(), spot.rect.getWidth(), spot.rect.getHeight());
                    if (temp.Contains(e.Location))
                    {
                        spot.Showing = true;
                    }
                    else
                    {
                        spot.Showing = false;
                    }
                }
            }
            Refresh();
            startingPoint = endingPoint;
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            List<ParkingArea> areas = myZone.parkingAreas;
            foreach (var item in shapes)
            {
                item.draw(e.Graphics);
            }
            foreach (var item in areas)
            {
                item.draw(e.Graphics); //draws the areas
                foreach (ParkingSpot spot in item.spots) //draws the spots
                {
                    spot.Draw(e.Graphics);
                    if (spot.Showing) { spot.HoverDraw(e.Graphics); } //if your mouse is over a spot, highlight it
                }
            }
        }

        private void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (ParkingArea area in myZone.parkingAreas)
            {
                area.loadArea();
            }
            canvas.Refresh();
        }

        private void canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (isRenumbering)
            {
                selectedSpot = myZone.parkingAreas[withinWhichArea(e)].spots[withinWhichSpot(withinWhichArea(e), e)];
                if (startingSpot == null)
                {
                    startingSpot = selectedSpot;
                    selectedSpot = null;

                    MessageBox.Show("Please click the last spot");
                }
                else if (endingSpot == null)
                {
                    endingSpot = selectedSpot;
                    selectedSpot = null;
                    isRenumbering = false;

                    MessageBox.Show("Starting Spot = " + startingSpot.SpotNumber + "\nEnding Spot = " + endingSpot.SpotNumber);
                }
            }
            else if (WithinArea(e.Location) && !isDeleting && !movingArea) //if you are within an area where you clicked and not deleting, then open that spot in a form
            {
                Person temp = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, myZone.parkingAreas[withinWhichArea(e)].spots[withinWhichSpot(withinWhichArea(e), e)].SpotNumber);
                if (temp != null) //if the spot exists in the database then open it, if not then don't let them open it
                {
                    PersonForm newSpot = new PersonForm(temp);
                    newSpot.FormClosed += new FormClosedEventHandler(ChildFormClosed);
                    currentArea = myZone.parkingAreas[withinWhichArea(e)];
                    newSpot.Show();
                }
            }
            else if (WithinArea(e.Location) && isDeleting && !movingArea) //if you are within an area where you clicked but are deleting then delete it
            {
                ParkingArea curArea = myZone.parkingAreas[withinWhichArea(e)];
                myZone.parkingAreas.Remove(curArea);
                isDeleting = false;
                Refresh();
                isDrawing = false;

                #region Delete Area/AreaPoints/Associated spots from the database.
                DBParkingLot.instance.DeleteArea(curArea.Name);
                DBParkingLot.instance.DeleteAreaPoints(curArea.Name);
                Spot[] temp = DBParkingLot.instance.SearchForSpots(DBParkingLot.ParkingSpotSearchCriteria.AreaName, curArea.Name);
                List<Person> temp2 = new List<Person>();
                foreach (Spot item in temp) //delete all the spots
                {
                    DBParkingLot.instance.DeleteSpot(item.SpotID.ToString());
                    DBParkingLot.instance.DeleteSpotPoints(item.SpotID.ToString());
                }
                for (int i = 0; i < temp.Length; i++) //search for people and vehicles
                {
                    temp2.Add(DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, temp[i].SpotID));
                }
                foreach (Person item in temp2) //delete all people
                {
                    if (item != null)
                    { DBParkingLot.instance.DeletePersonAll(item.PersonID); }
                }
                #endregion

                showUserFeedback("Area Deleted!", 2500);
            }
            else if (movingArea && WithinArea(e.Location)) //if you are within an area and movingarea is enabled then grab it
            {
                startingPoint = e.Location;
                grabbedArea = myZone.parkingAreas[withinWhichArea(e)];
                //Change cursor to closed hand
            }
            else if (isDrawing)//if not in a zone and are drawing a zone, start creating a new area
            {
                drag = true;
                Point startingPoint = e.Location;

                x = startingPoint.X; //Creates the starting point
                y = startingPoint.Y;

                shapes.Add(new APBox(x, y, x, y, Color.Black, 4));   //Figure out a way to track the distance or something so it doesent create a new zone with a single click (maybe in mouseup)
            }
            else //if you arent in a zone or drawing a zone then don't do anything
            {

            }
        }

        private int withinWhichSpot(int a, MouseEventArgs e)
        {
            int spotIn = 0;

            for (int i = 0; i < myZone.parkingAreas[a].spots.Count; i++)
            {
                Rectangle temp = new Rectangle(myZone.parkingAreas[a].spots[i].rect.getX1(), myZone.parkingAreas[a].spots[i].rect.getY1(), myZone.parkingAreas[a].spots[i].rect.getWidth(), myZone.parkingAreas[a].spots[i].rect.getHeight());
                if (temp.Contains(e.Location))
                {
                    spotIn = i;
                }
            }

            return spotIn;
        }

        private int withinWhichArea(MouseEventArgs e)
        {
            int areaIn = 0;

            for (int i = 0; i < myZone.parkingAreas.Count; i++)
            {
                if (myZone.parkingAreas[i].getRectangle().isWithin(e.Location))
                {
                    areaIn = i;
                }
            }

            return areaIn;
        }

        private void createAnAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //isDrawing = true;
            //isDeleting = false;
            //MessageBox.Show("Draw a box, approximately the size of your parking area");
            Point location = new Point(50, 50);
            while (WithinArea(location))
            {
                location.X++;
            }
            myZone.addArea(new ParkingArea(myZone.Name + (myZone.parkingAreas.Count), location , myZone, false, 0, 0, f1));
        }

        private void deleteAndAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isDeleting = true;
            isDrawing = false;
            MessageBox.Show("Click on a parking area to delete it!");
        }

        private bool WithinArea(Point Location)
        {
            bool inZone = false;
            foreach (var item in myZone.parkingAreas)
            {
                if (item.getRectangle().isWithin(Location))
                {
                    inZone = true;
                }
            }
            return inZone;
        }

        public void showUserFeedback(string feedback, int length)
        {
            timer1.Interval = length; // here time in milliseconds
            timer1.Tick += timer1_Tick;
            lblFeedbackMsg.Text = feedback;
            lblFeedbackMsg.Visible = true;
            timer1.Start();
            Refresh();
        }

        private void moveAnAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            movingArea = true;
            isDeleting = false;
            isDrawing = false;
        }

        private void massRenumberingToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        
            DialogResult d = MessageBox.Show("This tool is used to renumber more than spot at a time. Please input the starting spot number, click the first spot you wish to renumber, then click the last spot in the series that you wish to renumber. Start with the lowest number in your range. Using this tool could lead to other spots in the lot being changed if they have a number within the range.", "Mass Renumbering", MessageBoxButtons.OKCancel);
            if (d == DialogResult.OK)
            {
                isRenumbering = true;

                //Gets the first spot num
                InputForm i = new InputForm("What is the starting range of the spots?");
                i.ShowDialog();
                startingSpotNum = int.Parse(i.getData());

                MessageBox.Show("Please click the first spot");
            }
            else
            {
                //Intentionally left blank
            }
        }

        void timer1_Tick(object sender, System.EventArgs e)
        {
            lblFeedbackMsg.Visible = false;
            timer1.Stop();
        }
    }
}
