﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect {
    public partial class SearchForm : Form {

        public SearchForm() {
            InitializeComponent();

            foreach (DBParkingLot.AllTables item in Enum.GetValues(typeof(DBParkingLot.AllTables))) {
                comboBoxTable.Items.Add(item);
            }
            comboBoxTable.SelectedIndex = 0;


            //Creates the Open Form button
            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn {
                Name = "",
                Text = "Open Form",
                UseColumnTextForButtonValue = true
            };
            int columnIndex = 0;
            if (dataGridView1.Columns["button_column"] == null) {
                dataGridView1.Columns.Insert(columnIndex, buttonColumn);
            }

            //Emulates the search button being pressed.
            buttonSearch_Click(this, new EventArgs());
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            var senderGrid = (DataGridView)sender;

            //Opens a form relative to the user's input.
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0) {
                DBParkingLot.AllTables table = (DBParkingLot.AllTables)Enum.Parse(typeof(DBParkingLot.AllTables), comboBoxTable.Text);

                switch (table) {
                    case DBParkingLot.AllTables.People:
                        Person p = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, ((DataTable)dataGridView1.DataSource).Rows[e.RowIndex]["ID"]);
                        PersonForm spotForm = new PersonForm(p);
                        spotForm.ShowDialog();
                        break;
                    case DBParkingLot.AllTables.Vehicles:
                        Vehicle v = DBParkingLot.instance.SearchForVehicle(DBParkingLot.VehiclesSearchCriteria.Plate, ((DataTable)dataGridView1.DataSource).Rows[e.RowIndex]["Plate"]);
                        VehicleForm vehicleForm = new VehicleForm(v);
                        vehicleForm.ShowDialog();
                        break;
                    default:
                        break;
                }


            }
        }

        private void buttonSearch_Click(object sender, EventArgs e) {
            if (comboBoxColumn.Text == string.Empty || comboBoxTable.Text == string.Empty) {
                MessageBox.Show("Please search by some criteria!", "Please search by some criteria!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            try {
                DBParkingLot.AllTables table = (DBParkingLot.AllTables)Enum.Parse(typeof(DBParkingLot.AllTables), comboBoxTable.Text);

                DataTable dt = new DataTable();
                dt.Clear();

                switch (table) {

                    //Searches and constructs a people datatable based on the user input.
                    case DBParkingLot.AllTables.People:
                        Person[] people = DBParkingLot.instance.SearchForPeopleFuzzy((DBParkingLot.PeopleSearchCriteria)Enum.Parse(typeof(DBParkingLot.PeopleSearchCriteria), comboBoxColumn.Text), textBoxSearch.Text);

                        dt.Columns.Add("First Name");
                        dt.Columns.Add("Last Name");
                        dt.Columns.Add("ID");
                        dt.Columns.Add("Spot Number");
                        dt.Columns.Add("Occupation");
                        dt.Columns.Add("Grade");

                        for (int i = 0; i < people.Length; i++) {
                            DataRow person = dt.NewRow();

                            person["First Name"] = people[i].FirstName;
                            person["Last Name"] = people[i].LastName;
                            person["ID"] = people[i].PersonID;
                            person["Spot Number"] = people[i].SpotID;
                            person["Occupation"] = people[i].Occupation;
                            person["Grade"] = people[i].Grade;

                            dt.Rows.Add(person);
                        }

                        break;
                    //Searches and constructs a vehicles datatable based on the user input.
                    case DBParkingLot.AllTables.Vehicles:
                        Vehicle[] vehicles = DBParkingLot.instance.SearchForVehiclesFuzzy((DBParkingLot.VehiclesSearchCriteria)Enum.Parse(typeof(DBParkingLot.VehiclesSearchCriteria), comboBoxColumn.Text), textBoxSearch.Text);

                        dt.Columns.Add("Plate");
                        dt.Columns.Add("Year");
                        dt.Columns.Add("Make");
                        dt.Columns.Add("Model");
                        dt.Columns.Add("Color");
                        dt.Columns.Add("VIN");

                        for (int i = 0; i < vehicles.Length; i++) {
                            DataRow vehicle = dt.NewRow();

                            vehicle["Plate"] = vehicles[i].Plate;
                            vehicle["Year"] = vehicles[i].Year;
                            vehicle["Make"] = vehicles[i].Make;
                            vehicle["Model"] = vehicles[i].Model;
                            vehicle["Color"] = vehicles[i].Color;
                            vehicle["VIN"] = vehicles[i].Vin;

                            dt.Rows.Add(vehicle);
                        }

                        break;
                    default:
                        break;
                }

                dataGridView1.DataSource = dt;
            } catch (Exception) {
                MessageBox.Show("Something went wrong!", "Something went wrong!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void comboBoxTable_SelectedIndexChanged(object sender, EventArgs e) {
            //Parses the text in the table combobox.
            DBParkingLot.AllTables table = (DBParkingLot.AllTables)Enum.Parse(typeof(DBParkingLot.AllTables), comboBoxTable.Text);

            //Clears the column combobox.
            comboBoxColumn.Items.Clear();

            //Fills the column combo box based on the table combobox.
            switch (table) {
                case DBParkingLot.AllTables.People:
                    foreach (DBParkingLot.PeopleSearchCriteria item in Enum.GetValues(typeof(DBParkingLot.PeopleSearchCriteria))) {
                        comboBoxColumn.Items.Add(item);
                    }
                    break;
                case DBParkingLot.AllTables.Vehicles:
                    foreach (DBParkingLot.VehiclesSearchCriteria item in Enum.GetValues(typeof(DBParkingLot.VehiclesSearchCriteria))) {
                        comboBoxColumn.Items.Add(item);
                    }
                    break;
                default:
                    break;
            }

            comboBoxColumn.SelectedIndex = 0;
        }
    }
}
