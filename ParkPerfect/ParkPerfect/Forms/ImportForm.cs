﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace ParkPerfect {
    public partial class ImportForm : Form {

        private const int offSet = 2;
        private const int NUM_OF_VEHICLES = 3;

        private DataSet ds;

        Dictionary<string, ComboBox> comboBoxesCol;
        Dictionary<string, ComboBox> comboBoxesTab;

        public ImportForm() {
            InitializeComponent();

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                if (openFileDialog.FileName != "") {
                    OpenFile(openFileDialog.FileName);
                } else {
                    MessageBox.Show("Please name the file", "Need File Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } else {
                Close();
            }
        }

        public void OpenFile (string path) {
            List<DBParkingLot.PeopleSearchCriteria> peopleCols = Enum.GetValues(typeof(DBParkingLot.PeopleSearchCriteria)).Cast<DBParkingLot.PeopleSearchCriteria>().ToList();
            peopleCols.Remove(DBParkingLot.PeopleSearchCriteria.Occupation);
            Enum[] vehicleCols = Enum.GetValues(typeof(DBParkingLot.VehiclesSearchCriteria)).Cast<Enum>().ToArray();

            string[] peopleColsStr = peopleCols.Select(a => a.ToString()).ToList().ToArray();
            string[][] vehicleColsStr = new string[NUM_OF_VEHICLES][];

            for (int i = 1; i <= NUM_OF_VEHICLES; i++) {
                vehicleColsStr[i - 1] = vehicleCols.Select(a => a.ToString() + " #" + i).ToList().ToArray();
            }



            ds = ExcelUtils.ParseExcelFile(path);

            dataGridView1.DataSource = ds.Tables[0];

            comboBoxTables.Items.Clear();
            for (int i = 0; i < ds.Tables.Count; i++) {
                comboBoxTables.Items.Add(ds.Tables[i].TableName);
            }

            comboBoxTables.SelectedIndex = 0;

            if (comboBoxesCol == null || comboBoxesTab == null) {
                comboBoxesCol = new Dictionary<string, ComboBox>();
                comboBoxesTab = new Dictionary<string, ComboBox>();

                AddBoxes(peopleColsStr, comboBoxColumn.Location.Y, "Person");
                for (int i = 0; i < vehicleColsStr.GetLength(0); i++) {
                    AddBoxes(vehicleColsStr[i], comboBoxesCol.Last().Value.Location.Y + comboBoxesCol.Last().Value.Height * offSet, "Vehicle");
                }

                //Hides the original comboBox and label.
                comboBoxColumn.Hide();
                comboBoxTable.Hide();
                labelColumn.Hide();
                labelHeader.Hide();
            }
        }

        public void AddBoxes(string[] objects, int initialYPos, string headerStr) {

            //Creates the header
            Label header = new Label {
                Size = labelHeader.Size,
                Text = headerStr,
                TextAlign = labelHeader.TextAlign,
                AutoSize = labelHeader.AutoSize,
                Font = labelHeader.Font,
                Location = new Point(labelHeader.Location.X, initialYPos)
                
            };

            panel1.Controls.Add(header);

            for (int i = 1; i <= objects.Length; i++) {
                //Creates the comboBox and puts in the array.
                ComboBox cmboCol = new ComboBox();
                comboBoxesCol.Add(objects[i - 1], cmboCol);

                ComboBox cmboTab = new ComboBox();
                comboBoxesTab.Add(objects[i - 1], cmboTab);

                //Creates the label.
                Label lbl = new Label();

                //Sizes the comboBox and label.
                cmboCol.Size = comboBoxColumn.Size;
                cmboTab.Size = comboBoxTable.Size;
                lbl.Size = lbl.Size;

                //Positions the comboBox and label.
                cmboCol.Location = new Point(comboBoxColumn.Location.X, initialYPos + cmboCol.Height * i * offSet);
                cmboTab.Location = new Point(comboBoxTable.Location.X, initialYPos + cmboTab.Height * i * offSet);
                lbl.Location = new Point(lbl.Location.X, initialYPos + cmboCol.Height * i * offSet);

                //Adds the new comboBox and label to the panel's controls.
                panel1.Controls.Add(cmboCol);
                panel1.Controls.Add(cmboTab);
                panel1.Controls.Add(lbl);

                //Makes sure new box has same DropDownStyle as original.
                cmboCol.DropDownStyle = comboBoxColumn.DropDownStyle;
                cmboTab.DropDownStyle = comboBoxTable.DropDownStyle;

                //Sets the label's text.
                lbl.Text = objects[i - 1];
                lbl.TextAlign = labelColumn.TextAlign;

                //Adds each column in the dataset table to the comboBox just created.
                for (int j = 0; j < ds.Tables[0].Columns.Count; j++) {
                    cmboCol.Items.Add(ds.Tables[0].Columns[j]);
                }

                for (int j = 0; j < ds.Tables.Count; j++) {
                    cmboTab.Items.Add(ds.Tables[j].TableName);
                }
                cmboTab.SelectedIndex = 0;

                cmboTab.SelectedIndexChanged += new System.EventHandler(cmb_SelectedValueChanged);

            }
        }

        private void button1_Click(object sender, EventArgs e) {

            //try {
                DBParkingLot.instance.ForceOpenConnection();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var obj = ds.Tables[0].Columns[0].ColumnName;

                    //Converts to data from the tables into a Person object.
                    string firstName = (comboBoxesCol[DBParkingLot.AllColumns.FirstName.ToString()].Text == string.Empty) 
                        ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.FirstName.ToString()].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.FirstName.ToString()].SelectedIndex].ToString();
                    string lastName = (comboBoxesCol[DBParkingLot.AllColumns.LastName.ToString()].Text == string.Empty) 
                        ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.LastName.ToString()].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.LastName.ToString()].SelectedIndex].ToString();
                    string personID = (comboBoxesCol[DBParkingLot.AllColumns.PersonID.ToString()].Text == string.Empty) 
                        ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.PersonID.ToString()].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.PersonID.ToString()].SelectedIndex].ToString();
                    string grade = (comboBoxesCol[DBParkingLot.AllColumns.Grade.ToString()].Text == string.Empty) 
                        ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.Grade.ToString()].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.Grade.ToString()].SelectedIndex].ToString();
                    if (grade != string.Empty) grade = grade.ToUpper().Trim().Substring(0, 2);

                    Person.Grades gradeEnum;
                    switch (grade) {
                        case "9":
                        case "FR":
                            gradeEnum = Person.Grades.Freshman;
                            break;
                        case "10":
                        case "SO":
                            gradeEnum = Person.Grades.Sophomore;
                            break;
                        case "11":
                        case "JU":
                            gradeEnum = Person.Grades.Junior;
                            break;
                        case "12":
                        case "SE":
                            gradeEnum = Person.Grades.Senior;
                            break;
                        default:
                            gradeEnum = Person.Grades.NoGrade;
                            break;
                    }

                    string occupation = (comboBoxOccupation.Text == string.Empty) 
                        ? "" : ds.Tables[comboBoxOccupation.SelectedIndex].Rows[i].ItemArray[comboBoxOccupation.SelectedIndex].ToString();
                    string spotNumber = (comboBoxesCol[DBParkingLot.AllColumns.SpotID.ToString()].Text == string.Empty) 
                        ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.SpotID.ToString()].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.SpotID.ToString()].SelectedIndex].ToString();


                

                Person p = new Person(firstName, lastName, personID, (spotNumber != null) ? spotNumber.ToString() : string.Empty, (Person.Occupations)Enum.Parse(typeof(Person.Occupations), comboBoxOccupation.Text), gradeEnum);
                    DBParkingLot.instance.InsertPerson(p);

                    //Converts to data from the tables into a Vehicle object.
                    for (int j = 1; j <= NUM_OF_VEHICLES; j++) {
                        string plate = (comboBoxesCol[DBParkingLot.AllColumns.Plate.ToString() + " #" + j].Text == string.Empty)
                            ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.Plate.ToString() + " #" + j].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.Plate.ToString() + " #" + j].SelectedIndex].ToString();

                        //Continues if they don't have a car.
                        if (plate == string.Empty) continue;


                        string year = (comboBoxesCol[DBParkingLot.AllColumns.YearMade.ToString() + " #" + j].Text == string.Empty)
                            ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.YearMade.ToString() + " #" + j].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.YearMade.ToString() + " #" + j].SelectedIndex].ToString();
                        string make = (comboBoxesCol[DBParkingLot.AllColumns.Make.ToString() + " #" + j].Text == string.Empty)
                            ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.Make.ToString() + " #" + j].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.Make.ToString() + " #" + j].SelectedIndex].ToString();
                        string model = (comboBoxesCol[DBParkingLot.AllColumns.Model.ToString() + " #" + j].Text == string.Empty)
                            ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.Model.ToString() + " #" + j].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.Model.ToString() + " #" + j].SelectedIndex].ToString();
                        string vin = (comboBoxesCol[DBParkingLot.AllColumns.VIN.ToString() + " #" + j].Text == string.Empty)
                            ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.VIN.ToString() + " #" + j].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.VIN.ToString() + " #" + j].SelectedIndex].ToString();
                        string color = (comboBoxesCol[DBParkingLot.AllColumns.Color.ToString() + " #" + j].Text == string.Empty)
                            ? "" : ds.Tables[comboBoxesTab[DBParkingLot.AllColumns.Color.ToString() + " #" + j].SelectedIndex].Rows[i].ItemArray[comboBoxesCol[DBParkingLot.AllColumns.Color.ToString() + " #" + j].SelectedIndex].ToString();


                        Vehicle v = new Vehicle(plate, year, make, model, color, vin);
                        DBParkingLot.instance.InsertVehicle(v, p.PersonID);
                    }



                }
                DBParkingLot.instance.ForceCloseConnection();
            //} catch (Exception ex) {
            //    MessageBox.Show("Import Failed", "Import Failed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}

            MessageBox.Show("Import Complete", "Import Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();

        }

        private void comboBoxTables_SelectedIndexChanged(object sender, EventArgs e) {
            dataGridView1.DataSource = ds.Tables[comboBoxTables.SelectedIndex];
        }

        private void cmb_SelectedValueChanged(object sender, EventArgs e) {
            ComboBox cmb = (ComboBox)sender;

            var myKey = comboBoxesTab.FirstOrDefault(x => x.Value == cmb).Key;
            //int indexOf = comboBoxesTab.IndexOf(cmb);
            //int indexOf = Array.IndexOf(comboBoxesTab.ToArray(), cmb);

            comboBoxesCol[myKey].Items.Clear();

            for (int j = 0; j < ds.Tables[cmb.SelectedIndex].Columns.Count; j++) {
                comboBoxesCol[myKey].Items.Add(ds.Tables[cmb.SelectedIndex].Columns[j]);
            }
        }

        private void buttonOpenFile_Click(object sender, EventArgs e) {


            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                if (openFileDialog.FileName != "") {
                    OpenFile(openFileDialog.FileName);
                } else {
                    MessageBox.Show("Please name the file", "Need File Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
