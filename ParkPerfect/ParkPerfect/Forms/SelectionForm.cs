﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class SelectionForm : Form
    {
        public string data;

        public SelectionForm(string query, List<String> choices)
        {
            InitializeComponent();
            lblQuestion.Text = query;
            Text = query;
            checkList.Items.Clear();
            foreach (string choice in choices)
            {
                checkList.Items.Add(choice);
            }
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if(choiceMade() == 1)
            {
                data = 1.ToString();
            }else if(choiceMade() == 2)
            {
                data = 2.ToString();
            }
            Close();
        }

        public string getData()
        {
            return data;
        }

        private int choiceMade()
        {
            int res = 0;
            string checkedItem = checkList.CheckedItems[0].ToString();
            for (int i = 0; i < checkList.Items.Count; i++)
            {
                if(checkedItem == checkList.Items[i].ToString())
                {
                    res = i + 1;
                }
            }
            
            return res;
        }

        private void checkList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //Some code i found on stack overflow that will make it so you can only select one checkbox
            if (e.NewValue == CheckState.Checked)
                for (int ix = 0; ix < checkList.Items.Count; ++ix)
                    if (e.Index != ix) checkList.SetItemChecked(ix, false);
            btnEnter.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            data = "3";
            Close();
        }
    }
}
