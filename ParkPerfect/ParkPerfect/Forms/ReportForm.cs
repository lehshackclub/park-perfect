﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ParkPerfect {
    public partial class ReportForm : Form {

        private const int NUM_OF_VEHICLES = 3;

        public ReportForm() {
            InitializeComponent();

            new ExcelUtils();
        }

        private DataTable currentDataTable;

        private void btnCreate_Click(object sender, EventArgs e) {
            #region
            //try {
            //List<DBParkingLot.PeopleSearchCriteria> peopleCols = new List<DBParkingLot.PeopleSearchCriteria>();
            //List<DBParkingLot.VehiclesSearchCriteria> vehicleCols = new List<DBParkingLot.VehiclesSearchCriteria>();

            //#region Future goal is to optimize and make this more dynamic. For the time being this will do to meet deadlines.
            ////for (int i = 0; i < checkedListBoxPerson.Items.Count; i++) {
            ////    if (checkedListBoxPerson.GetItemChecked(i)) {
            ////        Enum.TryParse(checkedListBoxPerson.Items[i].ToString(), out DBParkingLot.PeopleSearchCriteria col);
            ////        peopleCols.Add(col);
            ////    }
            ////}

            ////for (int i = 0; i < checkedListBoxVehicle.Items.Count; i++) {
            ////    if (checkedListBoxVehicle.GetItemChecked(i)) {
            ////        Enum.TryParse(checkedListBoxVehicle.Items[i].ToString(), out DBParkingLot.VehiclesSearchCriteria col);
            ////        vehicleCols.Add(col);
            ////    }
            ////}

            ////if (peopleCols.Count == 0) {
            ////    MessageBox.Show("Pleas" +
            ////        "e select what data you'd like to report.",
            ////    "Please Select Data to Report.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            ////    return;
            ////}

            //#region To be implemented in some capacity.
            ////List<DBParkingLot.AllColumns> cols = new List<DBParkingLot.AllColumns>();
            ////List<DBParkingLot.SearchKeyOperator> ops = new List<DBParkingLot.SearchKeyOperator>();
            ////List<object> keys = new List<object>();

            ////if (checkBoxCondition.CheckState == CheckState.Checked) {
            ////    if (comboBoxColumn.Text == "" || comboBoxOp.Text == "" || textBoxKey.Text == "") {
            ////        MessageBox.Show("Something went wrong with your conditon.", "Something Wrong with Condition.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            ////        return;
            ////    } else {
            ////        Enum.TryParse(comboBoxColumn.Text, out DBParkingLot.AllColumns col);
            ////        Enum.TryParse(comboBoxOp.Text, out DBParkingLot.SearchKeyOperator op);
            ////        object key = textBoxKey.Text;
            ////        cols.Add(col);
            ////        ops.Add(op);
            ////        keys.Add(key);
            ////    }
            ////}

            ////if (checkBoxCondition2.CheckState == CheckState.Checked) {
            ////    if (comboBoxColumn2.Text == "" || comboBoxOp2.Text == "" || textBoxKey2.Text == "") {
            ////        MessageBox.Show("Something went wrong with your conditon.", "Something Wrong with Condition.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            ////        return;
            ////    } else {
            ////        Enum.TryParse(comboBoxColumn2.Text, out DBParkingLot.AllColumns col);
            ////        Enum.TryParse(comboBoxOp2.Text, out DBParkingLot.SearchKeyOperator op);
            ////        object key = textBoxKey2.Text;
            ////        cols.Add(col);
            ////        ops.Add(op);
            ////        keys.Add(key);
            ////    }
            ////}

            ////if (checkBoxCondition3.CheckState == CheckState.Checked) {
            ////    if (comboBoxColumn3.Text == "" || comboBoxOp3.Text == "" || textBoxKey3.Text == "") {
            ////        MessageBox.Show("Something went wrong with your conditon.", "Something Wrong with Condition.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            ////        return;
            ////    } else {
            ////        Enum.TryParse(comboBoxColumn3.Text, out DBParkingLot.AllColumns col);
            ////        Enum.TryParse(comboBoxOp3.Text, out DBParkingLot.SearchKeyOperator op);
            ////        object key = textBoxKey3.Text;
            ////        cols.Add(col);
            ////        ops.Add(op);
            ////        keys.Add(key);
            ////    }
            ////}

            ////if (checkBoxCondition4.CheckState == CheckState.Checked) {
            ////    if (comboBoxColumn4.Text == "" || comboBoxOp4.Text == "" || textBoxKey4.Text == "") {
            ////        MessageBox.Show("Something went wrong with your conditon.", "Something Wrong with Condition.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            ////        return;
            ////    } else {
            ////        Enum.TryParse(comboBoxColumn4.Text, out DBParkingLot.AllColumns col);
            ////        Enum.TryParse(comboBoxOp4.Text, out DBParkingLot.SearchKeyOperator op);
            ////        object key = textBoxKey4.Text;
            ////        cols.Add(col);
            ////        ops.Add(op);
            ////        keys.Add(key);
            ////    }
            ////}

            ////if (cols.Count > 0) {
            ////    //currentDataTable = DBParkingLot.instance.ReportDataTable(peopleCols.ToArray(), cols.ToArray(), keys.ToArray(), ops.ToArray());
            ////} else {
            ////    //currentDataTable = DBParkingLot.instance.ReportDataTable(peopleCols.ToArray());
            ////}
            //#endregion


            ////Creates DT
            //DataTable dt = new DataTable();
            //dt.Clear();

            //dt.Columns.Add("First Name");
            //dt.Columns.Add("Last Name");
            //dt.Columns.Add("ID");
            //dt.Columns.Add("Spot Number");
            //dt.Columns.Add("Occupation");
            //dt.Columns.Add("Grade");

            //for (int i = 1; i <= NUM_OF_VEHICLES; i++) {
            //    dt.Columns.Add("Plate " + i);
            //    dt.Columns.Add("Year "+ i);
            //    dt.Columns.Add("Make " + i);
            //    dt.Columns.Add("Model " + i);
            //    dt.Columns.Add("Color " + i);
            //    dt.Columns.Add("VIN " + i);
            //}

            ////Where the datatable will be constructed.
            //Person[] people = DBParkingLot.instance.SearchForPeople();

            //for (int i = 0; i < 200; i++) {
            //    Vehicle[] vehicles = people[i].GetVehicles();
            //for (int j = 1; j <= Math.Min(vehicles.Length, NUM_OF_VEHICLES); j++) {
            //        DataRow dr = dt.NewRow();

            //        dr["First Name"] = people[i].FirstName;
            //        dr["Last Name"] = people[i].LastName;
            //        dr["ID"] = people[i].PersonID;
            //        dr["Spot Number"] = people[i].SpotID;
            //        dr["Occupation"] = people[i].Occupation;
            //        dr["Grade"] = people[i].Grade;

            //        dr["Plate " + j] = vehicles[j - 1].Plate;
            //        dr["Year " + j] = vehicles[j - 1].Year;
            //        dr["Make " + j] = vehicles[j - 1].Make;
            //        dr["Model " + j] = vehicles[j - 1].Model;
            //        dr["Color " + j] = vehicles[j - 1].Color;
            //        dr["VIN " + j] = vehicles[j -1].Vin;

            //        dt.Rows.Add(dr);
            //    }
            //    //Can Create New Row Here
            //}

            //dataGridView.DataSource = dt; 
            //#endregion

            //} catch (Exception) {
            //    MessageBox.Show("Something went wrong :(", "An Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            #endregion

            List<DBParkingLot.AllColumns> columns = new List<DBParkingLot.AllColumns>();

            for (int i = 0; i < checkedListBoxPerson.Items.Count; i++) {
                if (checkedListBoxPerson.GetItemChecked(i)) {
                    Enum.TryParse(checkedListBoxPerson.Items[i].ToString(), out DBParkingLot.AllColumns col);
                    columns.Add(col);
                }
            }

            for (int i = 0; i < checkedListBoxVehicle.Items.Count; i++) {
                if (checkedListBoxVehicle.GetItemChecked(i)) {
                    Enum.TryParse(checkedListBoxVehicle.Items[i].ToString(), out DBParkingLot.AllColumns col);
                    columns.Add(col);
                }
            }

            if (columns.Count == 0) {
                MessageBox.Show("Pleas" +
                    "e select what data you'd like to report.",
                "Please Select Data to Report.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            currentDataTable = DBParkingLot.instance.Report(columns.ToArray());
            dataGridView.DataSource = currentDataTable;
        }

        private void btnSave_Click(object sender, EventArgs e) {
            if (currentDataTable == null) {
                MessageBox.Show("Please create a report first.", "Please Create Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SaveFileDialog openFile = new SaveFileDialog();
            openFile.Filter = "excel workbook (*.xlsx)|*.xlsx";

            if (openFile.ShowDialog() == DialogResult.OK) {
                if (openFile.FileName != "") {
                    ExcelUtils.instance.WriteDataTableToExcel(currentDataTable, "Parking Lot Report", openFile.FileName);
                } else {
                    MessageBox.Show("Please name the file", "Need File Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e) {
            if (dataGridView.DataSource == null) {
                MessageBox.Show("Please create a report.", "Please create a report.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //Future goal, make this more dynamic.

            try {
                int maxCol = 6;

                if (dataGridView.Columns.Count > maxCol) split = true;

                Headers = new string[Math.Min(dataGridView.Columns.Count, maxCol) + 1];
                if (split) Headers2 = new string[dataGridView.Columns.Count - maxCol + 1];

                Headers[0] = "#";
                if (split) Headers2[0] = "#";

                for (int i = 1; i <= dataGridView.Columns.Count; i++) {
                    if (i < maxCol + 1)
                        Headers[i] = dataGridView.Columns[i - 1].HeaderText;
                    else
                        Headers2[i - maxCol] = dataGridView.Columns[i - 1].HeaderText;
                }

                Data = new string[dataGridView.Rows.Count, Math.Min(dataGridView.Columns.Count, maxCol) + 1];
                if (split) Data2 = new string[dataGridView.Rows.Count, dataGridView.Columns.Count - maxCol + 1];
                for (int r = 0; r < Data.GetLength(0); r++) {
                    Data[r, 0] = (r + 1).ToString();
                    if (split) Data2[r, 0] = (r + 1).ToString();
                    for (int c = 1; c < Data.GetLength(1) + ((split) ? Data2.GetLength(1) - 1 : 0); c++) {
                        if (dataGridView.Rows[r].Cells[c - 1].Value != null) {
                            if (c <= maxCol) Data[r, c] = dataGridView.Rows[r].Cells[c - 1].Value.ToString();
                            else Data2[r, c - maxCol] = dataGridView.Rows[r].Cells[c - 1].Value.ToString();
                        }
                    }
                }

                if (DialogResult.OK == printDialog1.ShowDialog()) {
                    printDialog1.Document.Print();

                }
            } catch (Exception) {
                MessageBox.Show("Something went wrong :(", "An Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        // The sample data.
        bool split = false;

        private string[] Headers = { };
        private string[] Headers2 = { };

        private string[,] Data = { };
        private string[,] Data2 = { };

        // Figure out how wide each column should be.
        private int[] FindColumnWidths(Graphics gr, Font header_font,
            Font body_font, string[] headers, string[,] values) {
            // Make room for the widths.
            int[] widths = new int[headers.Length];

            // Find the width for each column.
            for (int col = 0; col < widths.Length; col++) {
                // Check the column header.
                widths[col] = (int)gr.MeasureString(
                    headers[col], header_font).Width;

                // Check the items.
                for (int row = 0; row <= values.GetUpperBound(0); row++) {
                    int value_width = (int)gr.MeasureString(
                        values[row, col], body_font).Width;
                    if (widths[col] < value_width)
                        widths[col] = value_width;
                }

                // Add some extra space.
                widths[col] += 10;
            }

            return widths;
        }

        private void pdoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e) {
            if (split) {
                Format(e, Data, Headers);
                Data = Data2;
                Headers = Headers2;
                split = false;
            } else {
                Format(e, Data, Headers);
            }
        }

        private void Format (System.Drawing.Printing.PrintPageEventArgs e, string[,] data, string[] header) {
            
            // Use this font.
            using (Font header_font = new Font("Times New Roman",
                14, FontStyle.Bold)) {
                using (Font body_font = new Font("Times New Roman", 12)) {
                    // We'll skip this much space between rows.
                    int line_spacing = 20;

                    // See how wide the columns must be.
                    int[] column_widths = FindColumnWidths(
                        e.Graphics, header_font, body_font, header, data);

                    // Start at the left margin.
                    int x = e.MarginBounds.Left;

                    // Print by columns.
                    for (int col = 0; col < header.Length; col++) {
                        // Print the header.
                        int y = e.MarginBounds.Top;
                        e.Graphics.DrawString(header[col],
                            header_font, Brushes.Blue, x, y);
                        y += (int)(line_spacing * 1.5);

                        // Print the items in the column.
                        for (int row = 0; row <=
                            data.GetUpperBound(0); row++) {
                            e.Graphics.DrawString(data[row, col],
                                body_font, Brushes.Black, x, y);
                            y += line_spacing;
                        }

                        // Move to the next column.
                        x += column_widths[col];
                    } // Looping over columns
                } // using body_font
            } // using header_font

            //DrawGrid(e, y)
            e.HasMorePages = split;
        }
    }
}