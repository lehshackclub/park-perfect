﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class ParkingLotClearForm : Form
    {
        public ParkingLotClearForm()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (txtSchool.Text.Trim() == DBParkingLot.instance.SearchGeneral().SchoolName)
            {
                DBParkingLot.instance.ForceOpenConnection();
                Person[] p = DBParkingLot.instance.SearchForPeople();
                foreach (Person person in p)
                {
                    DBParkingLot.instance.DeletePersonAll(person.PersonID);
                }
                Vehicle[] v = DBParkingLot.instance.SearchForVehicles();
                foreach (Vehicle vehicle in v)
                {
                    DBParkingLot.instance.DeleteVehicle(vehicle.Plate);
                }

                //Unassigns all parkings spots.
                DBParkingLot.instance.AssignAllSpots(false);

                MessageBox.Show("Parking Lot Cleared!");
                Close();
            }
            else
            {
                MessageBox.Show("Incorrect Input");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aborted. Parking Lot unchanged.");
            Close();
        }
    }
}
