﻿namespace ParkPerfect {
    partial class SplashScreen {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashScreen));
            this.buttonOpenDatabase = new System.Windows.Forms.Button();
            this.buttonRecent = new System.Windows.Forms.Button();
            this.recentPanel = new System.Windows.Forms.Panel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonNew = new System.Windows.Forms.Button();
            this.labelRecent = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.recentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOpenDatabase
            // 
            this.buttonOpenDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenDatabase.Location = new System.Drawing.Point(138, 219);
            this.buttonOpenDatabase.Name = "buttonOpenDatabase";
            this.buttonOpenDatabase.Size = new System.Drawing.Size(190, 49);
            this.buttonOpenDatabase.TabIndex = 1;
            this.buttonOpenDatabase.Text = "Open Database";
            this.buttonOpenDatabase.UseVisualStyleBackColor = true;
            this.buttonOpenDatabase.Click += new System.EventHandler(this.buttonOpenDatabase_Click);
            // 
            // buttonRecent
            // 
            this.buttonRecent.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRecent.Location = new System.Drawing.Point(3, 3);
            this.buttonRecent.Name = "buttonRecent";
            this.buttonRecent.Size = new System.Drawing.Size(307, 46);
            this.buttonRecent.TabIndex = 0;
            this.buttonRecent.Text = "buttonRecentFile";
            this.buttonRecent.UseVisualStyleBackColor = true;
            this.buttonRecent.Click += new System.EventHandler(this.buttonRecent_Click);
            // 
            // recentPanel
            // 
            this.recentPanel.AutoScroll = true;
            this.recentPanel.Controls.Add(this.buttonRecent);
            this.recentPanel.Location = new System.Drawing.Point(334, 196);
            this.recentPanel.Name = "recentPanel";
            this.recentPanel.Size = new System.Drawing.Size(313, 242);
            this.recentPanel.TabIndex = 2;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(138, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(509, 143);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // buttonNew
            // 
            this.buttonNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNew.Location = new System.Drawing.Point(138, 164);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(190, 49);
            this.buttonNew.TabIndex = 0;
            this.buttonNew.Text = "New Database";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // labelRecent
            // 
            this.labelRecent.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecent.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelRecent.Location = new System.Drawing.Point(334, 164);
            this.labelRecent.Name = "labelRecent";
            this.labelRecent.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelRecent.Size = new System.Drawing.Size(313, 29);
            this.labelRecent.TabIndex = 5;
            this.labelRecent.Text = "Recently Opened";
            this.labelRecent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // SplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelRecent);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.recentPanel);
            this.Controls.Add(this.buttonOpenDatabase);
            this.Name = "SplashScreen";
            this.Text = "Park Perfect";
            this.recentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOpenDatabase;
        private System.Windows.Forms.Button buttonRecent;
        private System.Windows.Forms.Panel recentPanel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Label labelRecent;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}