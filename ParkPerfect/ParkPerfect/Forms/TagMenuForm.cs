﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect {
    public partial class TagMenuForm : Form {

        private List<SpotTag> tags;
        private List<Button> buttons;

        private int offset;

        public TagMenuForm() {
            InitializeComponent();

            tags = DBParkingLot.instance.SearchForSpotTags().ToList();
            buttons = new List<Button>();

            offset = buttonAdd.Top - buttonEdit.Bottom;

            for (int i = 0; i < tags.Count; i++) {
                Label lbl = new Label();

                lbl.Size = labelTagName.Size;
                lbl.Location = new Point(labelTagName.Location.X, labelTagName.Location.Y + (lbl.Size.Height  + offset) * i);
                lbl.TextAlign = labelTagName.TextAlign;

                lbl.Text = tags[i].TagName;

                Button btn = new Button();
                buttons.Add(btn);

                btn.Size = buttonEdit.Size;
                btn.Location = new Point(buttonEdit.Location.X, buttonEdit.Location.Y + (btn.Size.Height + offset) * i);
                btn.TextAlign = buttonEdit.TextAlign;
                btn.Text = buttonEdit.Text;

                btn.Click += new EventHandler(buttonEdit_Click);

                panel1.Controls.Add(lbl);
                panel1.Controls.Add(btn);
            }

            textBoxTagName.Location = new Point(textBoxTagName.Location.X, textBoxTagName.Location.Y + (textBoxTagName.Size.Height + offset) * (tags.Count - 1));
            buttonAdd.Location = new Point(buttonAdd.Location.X, buttonAdd.Location.Y + (buttonEdit.Size.Height + offset) * (tags.Count - 1));

            labelTagName.Hide();
            buttonEdit.Hide();

        }

        private void buttonEdit_Click(object sender, EventArgs e) {
            Button senderButton = (Button)sender;

            int i = buttons.IndexOf(senderButton);

            TagForm tf = new TagForm(tags[i]);
            tf.Show();
        }

        private void buttonAdd_Click(object sender, EventArgs e) {
            if (textBoxTagName.Text == string.Empty) return;

            /*Please note. Not using Database Auto number because it saves a small amount
            to retrieve the auto number and the project is not being used simultaneously.*/
            int test = (DBParkingLot.instance.NextTagID() + 1);

            if (colorDialog.ShowDialog() != DialogResult.OK) return;

            tags.Add(new SpotTag(test, textBoxTagName.Text.Trim(), colorDialog.Color));


            Label lbl = new Label();

            lbl.Size = labelTagName.Size;
            lbl.Location = new Point(labelTagName.Location.X, labelTagName.Location.Y + (lbl.Size.Height + offset) * (tags.Count - 1));
            lbl.TextAlign = labelTagName.TextAlign;

            lbl.Text = tags.Last().TagName;

            Button btn = new Button();
            buttons.Add(btn);

            btn.Size = buttonEdit.Size;
            btn.Location = new Point(buttonEdit.Location.X, buttonEdit.Location.Y + (btn.Size.Height + offset) * (tags.Count - 1));
            btn.TextAlign = buttonEdit.TextAlign;
            btn.Text = buttonEdit.Text;

            btn.Click += new EventHandler(buttonEdit_Click);

            panel1.Controls.Add(lbl);
            panel1.Controls.Add(btn);

            textBoxTagName.Location = new Point(textBoxTagName.Location.X, textBoxTagName.Location.Y + (textBoxTagName.Size.Height + offset));
            buttonAdd.Location = new Point(buttonAdd.Location.X, buttonAdd.Location.Y + (buttonEdit.Size.Height + offset));

            DBParkingLot.instance.InsertSpotTag(tags.Last());
        }

        private void TagMenuForm_Load(object sender, EventArgs e) {

        }
    }
}
