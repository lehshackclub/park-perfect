﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class LicenseAndRegistrationForm : Form
    {
        public LicenseAndRegistrationForm()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            string imageFilePath = "";
            OpenFileDialog openImage = new OpenFileDialog();
            while (string.IsNullOrEmpty(imageFilePath))// process of selecting an image of school
            {
                if (openImage.ShowDialog().Equals(DialogResult.Cancel))
                {
                    MessageBox.Show("You must select an image!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    imageFilePath = openImage.FileName;
                }
            }
            pictureBox1.Image = Image.FromFile(imageFilePath);

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }
    }
}
