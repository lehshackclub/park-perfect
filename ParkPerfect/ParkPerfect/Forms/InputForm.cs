﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class InputForm : Form
    {
        public string data;

        public InputForm(string query)
        {
            InitializeComponent();
            lblQuestion.Text = query;    //assign the question to the text variable itself
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            data = txtInput.Text;
            getData();
            Close();
        }

        public string getData()
        {
            return data;
        }
    }
}
