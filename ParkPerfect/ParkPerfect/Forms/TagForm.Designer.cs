﻿namespace ParkPerfect
{
    partial class TagForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTagName = new System.Windows.Forms.Label();
            this.buttonChangeColor = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRange1 = new System.Windows.Forms.TextBox();
            this.textBoxRange2 = new System.Windows.Forms.TextBox();
            this.buttonChangeName = new System.Windows.Forms.Button();
            this.buttonAddRange = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTagName
            // 
            this.labelTagName.Location = new System.Drawing.Point(12, 9);
            this.labelTagName.Name = "labelTagName";
            this.labelTagName.Size = new System.Drawing.Size(134, 25);
            this.labelTagName.TabIndex = 0;
            this.labelTagName.Text = "Tag Name: ";
            this.labelTagName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonChangeColor
            // 
            this.buttonChangeColor.Location = new System.Drawing.Point(15, 66);
            this.buttonChangeColor.Name = "buttonChangeColor";
            this.buttonChangeColor.Size = new System.Drawing.Size(131, 23);
            this.buttonChangeColor.TabIndex = 1;
            this.buttonChangeColor.Text = "Change Color";
            this.buttonChangeColor.UseVisualStyleBackColor = true;
            this.buttonChangeColor.Click += new System.EventHandler(this.buttonChangeColor_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Spot Ranges";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxRange1
            // 
            this.textBoxRange1.Location = new System.Drawing.Point(6, 24);
            this.textBoxRange1.Name = "textBoxRange1";
            this.textBoxRange1.Size = new System.Drawing.Size(100, 20);
            this.textBoxRange1.TabIndex = 3;
            // 
            // textBoxRange2
            // 
            this.textBoxRange2.Location = new System.Drawing.Point(112, 24);
            this.textBoxRange2.Name = "textBoxRange2";
            this.textBoxRange2.Size = new System.Drawing.Size(100, 20);
            this.textBoxRange2.TabIndex = 4;
            // 
            // buttonChangeName
            // 
            this.buttonChangeName.Location = new System.Drawing.Point(15, 37);
            this.buttonChangeName.Name = "buttonChangeName";
            this.buttonChangeName.Size = new System.Drawing.Size(131, 23);
            this.buttonChangeName.TabIndex = 5;
            this.buttonChangeName.Text = "Change Name";
            this.buttonChangeName.UseVisualStyleBackColor = true;
            this.buttonChangeName.Click += new System.EventHandler(this.buttonChangeName_Click);
            // 
            // buttonAddRange
            // 
            this.buttonAddRange.Location = new System.Drawing.Point(112, 50);
            this.buttonAddRange.Name = "buttonAddRange";
            this.buttonAddRange.Size = new System.Drawing.Size(100, 23);
            this.buttonAddRange.TabIndex = 6;
            this.buttonAddRange.Text = "Add Range";
            this.buttonAddRange.UseVisualStyleBackColor = true;
            this.buttonAddRange.Click += new System.EventHandler(this.buttonAddRange_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonAddRange);
            this.panel1.Controls.Add(this.textBoxRange2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxRange1);
            this.panel1.Location = new System.Drawing.Point(152, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(219, 318);
            this.panel1.TabIndex = 7;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(240, 336);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(131, 23);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // TagForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 365);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonChangeName);
            this.Controls.Add(this.buttonChangeColor);
            this.Controls.Add(this.labelTagName);
            this.Name = "TagForm";
            this.Text = "TagForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTagName;
        private System.Windows.Forms.Button buttonChangeColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRange1;
        private System.Windows.Forms.TextBox textBoxRange2;
        private System.Windows.Forms.Button buttonChangeName;
        private System.Windows.Forms.Button buttonAddRange;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonSave;
    }
}