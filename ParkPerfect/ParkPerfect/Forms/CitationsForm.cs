﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class CitationsForm : Form
    {
        string myID;
        List<string> myCitations = new List<string>();

        public CitationsForm(string PersonID)
        {
            InitializeComponent();
            myID = PersonID;
            getCitations();
        }

        private void getCitations()
        {
            myCitations.Clear();
            Citation[] temp = DBParkingLot.instance.SearchForCitations(DBParkingLot.CitationSearchCriteria.PersonID, myID);
            foreach (var item in temp)
            {
                myCitations.Add(item.CitationInfo);
            }

            if (myCitations.Count > 0)
            {
                for (int i = 0; i < myCitations.Count; i++)
                {
                    lboxCitations.Items.Add((i + 1) + ". " + myCitations[i]);
                }
            }
        }

        private void btnAddCitation_Click(object sender, EventArgs e)
        {
            DBParkingLot.instance.InsertCitation(myID, myID + " " + (myCitations.Count + 1), txtNewCitation.Text);
            txtNewCitation.Text = "";
            lboxCitations.Items.Clear();
            getCitations();
            Refresh();
        }

        private void button1_Click(object sender, EventArgs e) {
            //Asks the user if they'd like to remove a citation.
            if (MessageBox.Show("Are you sure you'd like to remove citation" + (lboxCitations.SelectedIndex + 1) + "?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes) {

                //Finds the citation in the database.
                DBParkingLot.CitationSearchCriteria[] searchCriterias = { DBParkingLot.CitationSearchCriteria.PersonID, DBParkingLot.CitationSearchCriteria.CitationName };
                string[] keys = { myID, myID + " " + (lboxCitations.SelectedIndex + 1) };
                DBParkingLot.instance.DeleteCitation(searchCriterias, keys);

                //Refreshes the citation form.
                lboxCitations.Items.Clear();
                getCitations();
                Refresh();
            }
        }
    }
}
