﻿namespace ParkPerfect
{
    partial class ZoneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZoneForm));
            this.canvas = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createAnAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAndAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveAnAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblFeedbackMsg = new System.Windows.Forms.Label();
            this.massRenumberingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.Color.White;
            this.canvas.Location = new System.Drawing.Point(-1, 27);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(1920, 1080);
            this.canvas.TabIndex = 0;
            this.canvas.TabStop = false;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
            this.canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(798, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(65, 21);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(875, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createAnAreaToolStripMenuItem,
            this.deleteAndAreaToolStripMenuItem,
            this.moveAnAreaToolStripMenuItem,
            this.massRenumberingToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // createAnAreaToolStripMenuItem
            // 
            this.createAnAreaToolStripMenuItem.Name = "createAnAreaToolStripMenuItem";
            this.createAnAreaToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.createAnAreaToolStripMenuItem.Text = "Create an area";
            this.createAnAreaToolStripMenuItem.Click += new System.EventHandler(this.createAnAreaToolStripMenuItem_Click);
            // 
            // deleteAndAreaToolStripMenuItem
            // 
            this.deleteAndAreaToolStripMenuItem.Name = "deleteAndAreaToolStripMenuItem";
            this.deleteAndAreaToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.deleteAndAreaToolStripMenuItem.Text = "Delete an area";
            this.deleteAndAreaToolStripMenuItem.Click += new System.EventHandler(this.deleteAndAreaToolStripMenuItem_Click);
            // 
            // moveAnAreaToolStripMenuItem
            // 
            this.moveAnAreaToolStripMenuItem.Name = "moveAnAreaToolStripMenuItem";
            this.moveAnAreaToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.moveAnAreaToolStripMenuItem.Text = "Move an area";
            this.moveAnAreaToolStripMenuItem.Click += new System.EventHandler(this.moveAnAreaToolStripMenuItem_Click);
            // 
            // lblFeedbackMsg
            // 
            this.lblFeedbackMsg.AutoSize = true;
            this.lblFeedbackMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeedbackMsg.Location = new System.Drawing.Point(334, 2);
            this.lblFeedbackMsg.Name = "lblFeedbackMsg";
            this.lblFeedbackMsg.Size = new System.Drawing.Size(229, 20);
            this.lblFeedbackMsg.TabIndex = 5;
            this.lblFeedbackMsg.Text = "Feedback Message Default";
            this.lblFeedbackMsg.Visible = false;
            // 
            // massRenumberingToolStripMenuItem
            // 
            this.massRenumberingToolStripMenuItem.Name = "massRenumberingToolStripMenuItem";
            this.massRenumberingToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.massRenumberingToolStripMenuItem.Text = "Mass Renumbering";
            this.massRenumberingToolStripMenuItem.Click += new System.EventHandler(this.massRenumberingToolStripMenuItem_Click);
            // 
            // ZoneForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(875, 721);
            this.Controls.Add(this.lblFeedbackMsg);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimizeBox = false;
            this.Name = "ZoneForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ZoneForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox canvas;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createAnAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAndAreaToolStripMenuItem;
        private System.Windows.Forms.Label lblFeedbackMsg;
        private System.Windows.Forms.ToolStripMenuItem moveAnAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem massRenumberingToolStripMenuItem;
    }
}