﻿namespace ParkPerfect
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpAesthetics = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnHoverColor = new System.Windows.Forms.Button();
            this.btnSpotFontColor = new System.Windows.Forms.Button();
            this.btnChangeSpotFont = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAutoAlign = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAreaBackgroundColor = new System.Windows.Forms.Button();
            this.btnAreaLineColor = new System.Windows.Forms.Button();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.gbZones = new System.Windows.Forms.GroupBox();
            this.btnZoneFont = new System.Windows.Forms.Button();
            this.btnZoneFontColor = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonChangeImage = new System.Windows.Forms.Button();
            this.buttonChangeLogo = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonChangeName = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tpAesthetics.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.gbZones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpAesthetics);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(650, 563);
            this.tabControl1.TabIndex = 0;
            // 
            // tpAesthetics
            // 
            this.tpAesthetics.Controls.Add(this.groupBox4);
            this.tpAesthetics.Controls.Add(this.groupBox3);
            this.tpAesthetics.Controls.Add(this.groupBox2);
            this.tpAesthetics.Controls.Add(this.groupBox1);
            this.tpAesthetics.Controls.Add(this.gbZones);
            this.tpAesthetics.Location = new System.Drawing.Point(4, 22);
            this.tpAesthetics.Name = "tpAesthetics";
            this.tpAesthetics.Padding = new System.Windows.Forms.Padding(3);
            this.tpAesthetics.Size = new System.Drawing.Size(642, 537);
            this.tpAesthetics.TabIndex = 1;
            this.tpAesthetics.Text = "Aesthetics";
            this.tpAesthetics.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Location = new System.Drawing.Point(183, 223);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(164, 89);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "Dark Theme",
            "Light Theme"});
            this.listBox1.Location = new System.Drawing.Point(8, 40);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(117, 36);
            this.listBox1.TabIndex = 27;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 24);
            this.label12.TabIndex = 25;
            this.label12.Text = "Presets";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.btnHoverColor);
            this.groupBox2.Controls.Add(this.btnSpotFontColor);
            this.groupBox2.Controls.Add(this.btnChangeSpotFont);
            this.groupBox2.Location = new System.Drawing.Point(6, 223);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(171, 132);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 24);
            this.label11.TabIndex = 17;
            this.label11.Text = "Spots";
            // 
            // btnHoverColor
            // 
            this.btnHoverColor.Location = new System.Drawing.Point(8, 43);
            this.btnHoverColor.Name = "btnHoverColor";
            this.btnHoverColor.Size = new System.Drawing.Size(119, 22);
            this.btnHoverColor.TabIndex = 18;
            this.btnHoverColor.Text = "Change Hover Color";
            this.btnHoverColor.UseVisualStyleBackColor = true;
            this.btnHoverColor.Click += new System.EventHandler(this.btnHoverColor_Click);
            // 
            // btnSpotFontColor
            // 
            this.btnSpotFontColor.Location = new System.Drawing.Point(8, 99);
            this.btnSpotFontColor.Name = "btnSpotFontColor";
            this.btnSpotFontColor.Size = new System.Drawing.Size(133, 22);
            this.btnSpotFontColor.TabIndex = 19;
            this.btnSpotFontColor.Text = "Change Spot Font Color";
            this.btnSpotFontColor.UseVisualStyleBackColor = true;
            this.btnSpotFontColor.Click += new System.EventHandler(this.btnSpotFontColor_Click);
            // 
            // btnChangeSpotFont
            // 
            this.btnChangeSpotFont.Location = new System.Drawing.Point(8, 71);
            this.btnChangeSpotFont.Name = "btnChangeSpotFont";
            this.btnChangeSpotFont.Size = new System.Drawing.Size(105, 22);
            this.btnChangeSpotFont.TabIndex = 20;
            this.btnChangeSpotFont.Text = "Change Spot Font";
            this.btnChangeSpotFont.UseVisualStyleBackColor = true;
            this.btnChangeSpotFont.Click += new System.EventHandler(this.btnChangeSpotFont_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbAutoAlign);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnAreaBackgroundColor);
            this.groupBox1.Controls.Add(this.btnAreaLineColor);
            this.groupBox1.Controls.Add(this.trackBar2);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(360, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 207);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            // 
            // cbAutoAlign
            // 
            this.cbAutoAlign.AutoSize = true;
            this.cbAutoAlign.Location = new System.Drawing.Point(11, 178);
            this.cbAutoAlign.Name = "cbAutoAlign";
            this.cbAutoAlign.Size = new System.Drawing.Size(135, 17);
            this.cbAutoAlign.TabIndex = 17;
            this.cbAutoAlign.Text = "Enable Area Auto Align";
            this.cbAutoAlign.UseVisualStyleBackColor = true;
            this.cbAutoAlign.CheckStateChanged += new System.EventHandler(this.cbAutoAlign_CheckStateChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Area Line Width";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 24);
            this.label6.TabIndex = 9;
            this.label6.Text = "Areas";
            // 
            // btnAreaBackgroundColor
            // 
            this.btnAreaBackgroundColor.Location = new System.Drawing.Point(9, 45);
            this.btnAreaBackgroundColor.Name = "btnAreaBackgroundColor";
            this.btnAreaBackgroundColor.Size = new System.Drawing.Size(168, 23);
            this.btnAreaBackgroundColor.TabIndex = 10;
            this.btnAreaBackgroundColor.Text = "Change Area Background Color";
            this.btnAreaBackgroundColor.UseVisualStyleBackColor = true;
            this.btnAreaBackgroundColor.Click += new System.EventHandler(this.btnAreaBackgroundColor_Click);
            // 
            // btnAreaLineColor
            // 
            this.btnAreaLineColor.Location = new System.Drawing.Point(9, 74);
            this.btnAreaLineColor.Name = "btnAreaLineColor";
            this.btnAreaLineColor.Size = new System.Drawing.Size(133, 23);
            this.btnAreaLineColor.TabIndex = 11;
            this.btnAreaLineColor.Text = "Change Area Line Color";
            this.btnAreaLineColor.UseVisualStyleBackColor = true;
            this.btnAreaLineColor.Click += new System.EventHandler(this.btnAreaLineColor_Click);
            // 
            // trackBar2
            // 
            this.trackBar2.BackColor = System.Drawing.Color.White;
            this.trackBar2.Location = new System.Drawing.Point(6, 141);
            this.trackBar2.Maximum = 3;
            this.trackBar2.Minimum = 1;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(144, 45);
            this.trackBar2.TabIndex = 12;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBar2.Value = 1;
            this.trackBar2.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Thin";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(118, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Thick";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(61, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Normal";
            // 
            // gbZones
            // 
            this.gbZones.Controls.Add(this.btnZoneFont);
            this.gbZones.Controls.Add(this.btnZoneFontColor);
            this.gbZones.Controls.Add(this.trackBar1);
            this.gbZones.Controls.Add(this.label2);
            this.gbZones.Controls.Add(this.label3);
            this.gbZones.Controls.Add(this.label4);
            this.gbZones.Controls.Add(this.label5);
            this.gbZones.Controls.Add(this.label1);
            this.gbZones.Location = new System.Drawing.Point(183, 17);
            this.gbZones.Name = "gbZones";
            this.gbZones.Size = new System.Drawing.Size(171, 200);
            this.gbZones.TabIndex = 21;
            this.gbZones.TabStop = false;
            // 
            // btnZoneFont
            // 
            this.btnZoneFont.Location = new System.Drawing.Point(6, 42);
            this.btnZoneFont.Name = "btnZoneFont";
            this.btnZoneFont.Size = new System.Drawing.Size(105, 22);
            this.btnZoneFont.TabIndex = 1;
            this.btnZoneFont.Text = "Change Zone Font";
            this.btnZoneFont.UseVisualStyleBackColor = true;
            this.btnZoneFont.Click += new System.EventHandler(this.btnZoneFont_Click);
            // 
            // btnZoneFontColor
            // 
            this.btnZoneFontColor.Location = new System.Drawing.Point(6, 70);
            this.btnZoneFontColor.Name = "btnZoneFontColor";
            this.btnZoneFontColor.Size = new System.Drawing.Size(133, 23);
            this.btnZoneFontColor.TabIndex = 2;
            this.btnZoneFontColor.Text = "Change Zone Font Color";
            this.btnZoneFontColor.UseVisualStyleBackColor = true;
            this.btnZoneFontColor.Click += new System.EventHandler(this.btnZoneFontColor_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.Color.White;
            this.trackBar1.Location = new System.Drawing.Point(6, 146);
            this.trackBar1.Maximum = 3;
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(144, 45);
            this.trackBar1.TabIndex = 5;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBar1.Value = 1;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Zone Outline Width";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Thin";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Normal";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(116, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Thick";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zones";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(583, 581);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Done";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(503, 581);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonChangeName);
            this.groupBox4.Controls.Add(this.buttonChangeImage);
            this.groupBox4.Controls.Add(this.buttonChangeLogo);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(6, 17);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(171, 200);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            // 
            // buttonChangeImage
            // 
            this.buttonChangeImage.Location = new System.Drawing.Point(6, 71);
            this.buttonChangeImage.Name = "buttonChangeImage";
            this.buttonChangeImage.Size = new System.Drawing.Size(133, 22);
            this.buttonChangeImage.TabIndex = 1;
            this.buttonChangeImage.Text = "Change Lot Image";
            this.buttonChangeImage.UseVisualStyleBackColor = true;
            this.buttonChangeImage.Click += new System.EventHandler(this.buttonChangeImage_Click);
            // 
            // buttonChangeLogo
            // 
            this.buttonChangeLogo.Location = new System.Drawing.Point(6, 99);
            this.buttonChangeLogo.Name = "buttonChangeLogo";
            this.buttonChangeLogo.Size = new System.Drawing.Size(133, 23);
            this.buttonChangeLogo.TabIndex = 2;
            this.buttonChangeLogo.Text = "Change Logo";
            this.buttonChangeLogo.UseVisualStyleBackColor = true;
            this.buttonChangeLogo.Click += new System.EventHandler(this.buttonChangeLogo_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 17);
            this.label13.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 130);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(59, 130);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(116, 130);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 24);
            this.label17.TabIndex = 0;
            this.label17.Text = "General";
            // 
            // buttonChangeName
            // 
            this.buttonChangeName.Location = new System.Drawing.Point(6, 42);
            this.buttonChangeName.Name = "buttonChangeName";
            this.buttonChangeName.Size = new System.Drawing.Size(133, 23);
            this.buttonChangeName.TabIndex = 9;
            this.buttonChangeName.Text = "Change Lot Name";
            this.buttonChangeName.UseVisualStyleBackColor = true;
            this.buttonChangeName.Click += new System.EventHandler(this.buttonChangeName_Click);
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 607);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OptionsForm";
            this.tabControl1.ResumeLayout(false);
            this.tpAesthetics.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.gbZones.ResumeLayout(false);
            this.gbZones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpAesthetics;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.Button btnZoneFont;
        private System.Windows.Forms.Button btnZoneFontColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAreaBackgroundColor;
        private System.Windows.Forms.Button btnAreaLineColor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnHoverColor;
        private System.Windows.Forms.Button btnSpotFontColor;
        private System.Windows.Forms.Button btnChangeSpotFont;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbZones;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.CheckBox cbAutoAlign;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonChangeName;
        private System.Windows.Forms.Button buttonChangeImage;
        private System.Windows.Forms.Button buttonChangeLogo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
    }
}