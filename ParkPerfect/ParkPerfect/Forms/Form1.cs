﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;

namespace ParkPerfect {
    public partial class Form1 : Form {
        public static Form1 instance;

        private List<Point> points = new List<Point>();
        private const int LOCATION_ERROR = 10;
        public List<ParkingZone> myZones = new List<ParkingZone>();
        private bool shapeInProg = false;
        private bool isDrawing = false;
        private const int POINT_SIZE = 10;
        private bool isDeleting;
        public int currSpot;
        private Timer timer1 = new Timer();

        public Form1() {
            //Enforces the singleton design.
            if (instance == null) instance = this;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SplashScreen splash = new SplashScreen();
            splash.ShowDialog();
            if (DBParkingLot.instance == null) Application.Exit();
        }

        private void loadLabels()
        {
            lblTotalVehicles.Text = "Total Vehicles: " + DBParkingLot.instance.SearchForVehicles().Length;
            lblTotalPeople.Text = "Total People: " + (DBParkingLot.instance.SearchForPeople().Length - DBParkingLot.instance.SearchForPeople(DBParkingLot.PeopleSearchCriteria.SpotID, "").Length);
            int emptyspots = 0;
            emptyspots = DBParkingLot.instance.SearchForSpots().Length - (DBParkingLot.instance.SearchForPeople().Length - DBParkingLot.instance.SearchForPeople(DBParkingLot.PeopleSearchCriteria.SpotID, "").Length);
            lblEmptySpots.Text = "Empty Spots: " + emptyspots;
        }

        public void DefaultLoad(string databaseFilePath)
        {
            new DBParkingLot(databaseFilePath);

            currSpot = DBParkingLot.instance.SearchForSpots().Length;
            loadLabels();
            try
            {
                //Connects the default database
                DBParkingLot db = new DBParkingLot(databaseFilePath);

                //Gets the default info (schoolname, image, etc.)
                GeneralInfo g = db.SearchGeneral();

                Image image = null;
                Image logo = null;
                string imageFilePath = "";
                string logoFilePath = "";
                string schoolName = "";

                if (g != null) //If the info is already in the database
                {
                    image = g.Image;
                    logo = g.Logo;
                    schoolName = g.SchoolName;
                }
                else //This stuff will only run on a first time start up, it tells this by if a file to a school photo does not exist, it runs it
                {
                    InputForm input = new InputForm("Enter your school name"); //opens a box to get school name
                    input.ShowDialog();
                    schoolName = input.getData(); //gets school name
                    OpenFileDialog openImage = new OpenFileDialog();
                    while (string.IsNullOrEmpty(imageFilePath))// process of selecting an image of school
                    {
                        if (openImage.ShowDialog().Equals(DialogResult.Cancel))
                        {
                            MessageBox.Show("You must select an image!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            imageFilePath = openImage.FileName;
                        }
                    }
                    OpenFileDialog openLogo = new OpenFileDialog();
                    while (string.IsNullOrEmpty(logoFilePath))// process of selecting an image of school
                    {
                        if (openLogo.ShowDialog().Equals(DialogResult.Cancel))
                        {
                            MessageBox.Show("You must select an image!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            logoFilePath = openLogo.FileName;
                        }
                    }
                    image = Image.FromFile(imageFilePath);
                    logo = Image.FromFile(logoFilePath);

                    db.InsertGeneral(schoolName, image, logo);

                }
                Text = schoolName + " Parking Lot"; //changes window text
                canvas.BackgroundImage = image;
                pbSchoolLogo.BackgroundImage = logo;
                #region Finds pre-existing zones in the Database. Code is not pretty...
                //Finds all zones.
                Zone[] zones = DBParkingLot.instance.SearchForZones();

                //Loops through all zones.
                for (int i = 0; i < zones.Length; i++)
                {
                    //Finds points affiliated with each zone.
                    ZonePoint[] points = DBParkingLot.instance.SearchForZonePoints(DBParkingLot.ZonePointSearchCriteria.ZoneName, zones[i].Name);

                    //Converts Zone points to Points.
                    Point[] p = new Point[points.Length];
                    for (int i2 = 0; i2 < points.Length; i2++)
                    {
                        p[i2] = new Point((int)points[i2].X, (int)points[i2].Y);
                        //p[i2] = new Point(points[i2].X + zoneOffset, points[i2].Y + zoneOffset);
                    }

                    /*Takes the Zone and turns it into a ParkingZone that is added
                    to myZones.*/
                    myZones.Add(new ParkingZone(new List<Point>(p), zones[i].Name, zones[i].Color, this));
                }
                #endregion
                WindowState = FormWindowState.Maximized;
                Refresh();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong trying to open the parking lot.");
                Application.Exit();
            }
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            //Draws out all the dots (if there are any) on the parking lot, the ones displayed while creating a form
            foreach (var item in points)
            {
                e.Graphics.FillEllipse(new SolidBrush(Color.Black), (item.X - POINT_SIZE / 2), (item.Y - POINT_SIZE / 2), POINT_SIZE, POINT_SIZE);
            }
            //Draws out all my zones
            for (int i = 0; i < myZones.Count - 1; i++)
            {
                myZones[i].poly.draw(e.Graphics);
            }
            //If the last zone isn't in progress, then draw it. Without this it trys to draw half completed zones.
            if (!shapeInProg && myZones.Count > 0)
            {
                myZones.Last().poly.draw(e.Graphics);
            }
        }

        private void canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (withinZone(e) && !shapeInProg && !isDeleting) //if within a parking zone and a shape is not in the progress of being made, and not deleting. This means we are clicking on a zone.
            {
                ZoneForm newZone = new ZoneForm(myZones[withinWhichZone(e)], this); //Create new zoneform and then open it for whatever zone you clicked on.
                newZone.FormClosed += new FormClosedEventHandler(ZoneFormClosed);
                newZone.Show();
            }

            if (withinZone(e) && !shapeInProg && isDeleting) //if within a zone, shape not in progress, are deleting. This means we are trying to delete a zone.
            {
                //Finds the zone you clicked on and deletes it
                ParkingZone curZone = myZones[withinWhichZone(e)];
                myZones.Remove(curZone);

                DeletionHelpers.deleteZone(curZone);

                //Set is deleting to false that way it only deletes on thing and the action doesn't linger.
                isDeleting = false;
                Refresh();
                isDrawing = false;
                btnCancel.Visible = false;
                Cursor = Cursors.Default;
                showUserFeedback("Zone Deleted!", 2500);
            }

            if (isDrawing)
            {
                if (!shapeInProg) //if a shape isnt being made, Make a shape
                {
                    myZones.Add(new ParkingZone(points, null, Color.Black, this));
                    shapeInProg = true;
                    points.Add(e.Location);
                    Refresh();
                }
                else if (shapeInProg)//if a shape is being made
                {
                    points.Add(e.Location);
                    if (points.Count >= 3 && Math.Abs(points[points.Count - 1].X - points[0].X) <= LOCATION_ERROR && Math.Abs(points[points.Count - 1].Y - points[0].Y) <= LOCATION_ERROR)
                    {
                        zoneCreated();
                    }
                    Refresh();
                }
            }
        }

        private void zoneCreated()
        {
            shapeInProg = false;
            isDrawing = false;
            points.Remove(points[points.Count - 1]); //Removes the last point from the points array. It was just there temporarily to connect our polygon. It will be remembered for its service.
            InputForm dialog = new InputForm("Enter a name for your new zone"); //Prompt for a zone name
            string entry = "";
            //The following while loop makes it so the user can't give a blank zone name.
            while (entry == "")
            {
                dialog.ShowDialog();
                entry = dialog.getData();
                if (entry != "")
                {
                    break;
                }
                MessageBox.Show("You must enter a name for your zone!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Point[] temp = new Point[points.Count]; //creates a temporary array of all our points

            for (int i = 0; i < points.Count; i++)
            {
                temp[i] = points[i];

                #region Adds the ZonePoints to the database.
                ZonePoint aZonePoint = new ZonePoint(entry, points[i].X, points[i].Y);
                DBParkingLot.instance.InsertZonePoint(aZonePoint);
                #endregion
            }

            ColorDialog colorSelector = new ColorDialog(); //Prompts you to choose a color for your zone outline.
            colorSelector.ShowDialog();
            myZones.Last().poly.setColor(colorSelector.Color);
            myZones.Last().color = colorSelector.Color;
            myZones.Last().Name = entry;
            myZones.Last().Reload(); //ROBBY! PLEASE FIX THIS!
            points = new List<Point>(); //Resets the point list since it is now on to a new shape.
            isDrawing = false;
            btnCancel.Visible = false;
            showUserFeedback("Zone Created!", 2500);

            #region MyRegion Adds the Zone to the database.
            Zone aZone = new Zone(myZones.Last().Name, myZones.Last().color);
            DBParkingLot.instance.InsertZone(aZone);
            #endregion

            Refresh();
        }
        
        private void createANewZoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on the corners of your zone to create");
            points = new List<Point>();
            isDrawing = true;
            isDeleting = false;
            btnCancel.Visible = true;
            btnCancel.Enabled = true;
        }

        private void deleteAZoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on a zone to delete it");
            isDeleting = true;
            isDrawing = false;
            points = new List<Point>();
            btnCancel.Visible = true;
            btnCancel.Enabled = true;
            Cursor = Cursors.Cross;
            Refresh();
        }

        #region Utilities
        private bool withinZone(MouseEventArgs e)
        {
            bool inZone = false;
            foreach (var item in myZones)
            {
                if (!shapeInProg)
                {
                    if (item.poly.isWithin(e.Location))
                    {
                        inZone = true;
                    }
                }
            }
            return inZone;
        }

        public int withinWhichZone(MouseEventArgs e)
        {
            int zoneIn = 0;

            for (int i = 0; i < myZones.Count; i++)
            {
                if (myZones[i].poly.isWithin(e.Location))
                {
                    zoneIn = i;
                }
            }

            return zoneIn;
        }




        #endregion

        public int CurrentSpot
        {
            get { return currSpot; }
            set { currSpot = value; }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (isDeleting) //If you are currently deleting stuff, dont do that anymore
            {
                isDeleting = false;
                Cursor = Cursors.Default;
            }

            if (isDrawing) //If you are drawing stuff, reset that
            {
                isDrawing = false;
                shapeInProg = false;
                points = new List<Point>();
                myZones.Remove(myZones.Last());
                Refresh();
            }

            btnCancel.Enabled = false;//make it disappear after its clicked
            btnCancel.Visible = false;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (ActiveForm != null) canvas.Location = new Point(ActiveForm.Width / 2 - canvas.Width / 2, ActiveForm.Height / 2 - canvas.Height / 2); //Stupid fix.
        }

        private void openDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DBParkingLotManager.OpenDatabase();
        }


        public static Bitmap ResizeImage(Image image, int width, int height)
        //Literally just found this on stack overflow, all you need to know is in the title. It aparently is lossless too.
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportForm r = new ReportForm();
            r.ShowDialog();
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchForm s = new SearchForm();
            s.ShowDialog();
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportForm s = new ImportForm();
            if (!s.IsDisposed) s.ShowDialog();
        }

        private void resizeAZoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Need to have the dots reappearand then be able to drag them around and then click ok
        }

        private void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            canvas.Refresh();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm o = new OptionsForm();
            o.FormClosed += new FormClosedEventHandler(ChildFormClosed);
            o.ShowDialog();
        }

        public void showUserFeedback(string feedback, int length)
        {
            timer1.Interval = length; // here time in milliseconds
            timer1.Tick += timer1_Tick;
            lblFeedbackMsg.Text = feedback;
            lblFeedbackMsg.Visible = true;
            timer1.Start();
            Refresh();
        }

        void timer1_Tick(object sender, System.EventArgs e)
        {
            lblFeedbackMsg.Visible = false;
            timer1.Stop();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void reportIssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming Soon");
        }

        private void clearAllParkingSpotsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Are you sure you want to delete all students from the parking lot? This CANNOT be undone!", "WARNING", MessageBoxButtons.YesNo);
            if (d == DialogResult.Yes)
            {
                ParkingLotClearForm p = new ParkingLotClearForm();
                p.Show();
            }
            else
            {
                //Intentionally left blank
            }
        }

        private void ZoneFormClosed(object sender, FormClosedEventArgs e)
        {
            loadLabels();
        }

        private void btnAddPerson_Click(object sender, EventArgs e)
        {
            PersonForm personForm = new PersonForm();
            personForm.Show();
        }

        private void buttonDeletePerson_Click(object sender, EventArgs e) {
            DBParkingLotManager.DeletePerson();
        }

        private void btnAssignToSpot_Click(object sender, EventArgs e) {
            DBParkingLotManager.AssignPersonToSpot();
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e) {
            DBParkingLotManager.NewDatabase(/*this*/);
        }

        private void buttonUnassignFromSpot_Click(object sender, EventArgs e) {
            DBParkingLotManager.UnassignPersonFromSpot();
        }

        private void buttonUnassignFromPerson_Click(object sender, EventArgs e) {
            DBParkingLotManager.UnassignSpotFromPerson();
        }

        private void editTagsToolStripMenuItem_Click(object sender, EventArgs e) {
            TagMenuForm tagForm = new TagMenuForm();
            tagForm.Show();
        }

    }
}

