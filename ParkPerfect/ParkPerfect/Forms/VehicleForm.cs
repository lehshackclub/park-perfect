﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect
{
    public partial class VehicleForm : Form
    {
        private Vehicle myVehicle;
        private Person[] myOwners;
        private string myPersonID;
        private string mySpotID;

        public VehicleForm(Vehicle v)
        {
            InitializeComponent();
            myVehicle = v;
            myPersonID = v.GetOwner().PersonID;
            mySpotID = v.GetOwner().SpotID;
            
            Text = v.ToString();

            lblPlate.Text = myVehicle.Plate;
            lblYear.Text = myVehicle.Year;
            lblMake.Text = myVehicle.Make;
            lblModel.Text = myVehicle.Model;
            lblColor.Text = myVehicle.Color;

            LoadPeople();
        }

        public VehicleForm(Person p)
        {
            InitializeComponent();
            Text = p.FirstName + "'s Vehicle";
            
            myPersonID = p.PersonID;
            mySpotID = p.SpotID;

            EnableEditMode();
        }

        private void LoadPeople () {
            myOwners = myVehicle.GetOwners();
            dupOwners.Items.Clear();
            for (int i = 0; i < myOwners.Length; i++) {
                dupOwners.Items.Add(myOwners[i].ToString()); //Populate the dup with the persons vehicles
            }
            dupOwners.SelectedIndex = 0;
            lblOwner.Text = "Owners (" + myOwners.Count() + "):";

        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EnableEditMode();
        }

        private void EnableEditMode()
        {
            if (myVehicle == null) {
                txtPlate.Enabled = true;
                txtPlate.Visible = true;
            } else {
                //Put info into text boxes
                txtColor.Text = myVehicle.Color;
                txtMake.Text = myVehicle.Make;
                txtModel.Text = myVehicle.Model;
                txtYear.Text = myVehicle.Year;
            }

            //Enable the text boxes and make them visible
            txtYear.Enabled = true;
            txtModel.Enabled = true;
            txtMake.Enabled = true;
            txtColor.Enabled = true;
            txtYear.Visible = true;
            txtModel.Visible = true;
            txtMake.Visible = true;
            txtColor.Visible = true;

            //make the labels disabled and invisible
            lblYear.Enabled = false;
            lblModel.Enabled = false;
            lblMake.Enabled = false;
            lblColor.Enabled = false;
            lblColor.Visible = false;
            lblMake.Visible = false;
            lblModel.Visible = false;
            lblYear.Visible = false;

            //Make the done button appear
            btnDone.Enabled = true;
            btnDone.Visible = true;

            //Disables owner selection.
            dupOwners.Enabled = false;
            btnOwner.Enabled = false;
        }

        private void btnDone_Click(object sender, EventArgs e) {

            //If all the labels are defaults, then use the text fields to insert a new person, because they dont exist
            if (myVehicle == null) {
                //If plate exists in the db
                Vehicle searchVehicle = DBParkingLot.instance.SearchForVehicle(DBParkingLot.VehiclesSearchCriteria.Plate, txtPlate.Text);
                if (searchVehicle != null) {
                    DialogResult d = MessageBox.Show("This Vehicle already exists in the lot! Are you sure you would like to link it to this person?", "Vehicle Found", MessageBoxButtons.YesNo);
                    //If yes establish a link to the person.
                    if (d == DialogResult.Yes) {
                        myVehicle = searchVehicle;
                        DBParkingLot.instance.InsertVehiclesToPeople(myPersonID, txtPlate.Text.Trim());
                    }
                }
                //if it doesent exist yet
                else {
                    myVehicle = new Vehicle(txtPlate.Text.Trim(), txtYear.Text.Trim(), txtMake.Text.Trim(), txtModel.Text.Trim(), txtColor.Text.Trim(), "");
                    DBParkingLot.instance.InsertVehicle(myVehicle, myPersonID);
                }
            }
            //If my vehicle exists then just update it in the db
            else {
                myVehicle.Year = txtYear.Text;
                myVehicle.Make = txtMake.Text;
                myVehicle.Model = txtModel.Text;
                myVehicle.Color = txtColor.Text;
                //myVehicle.Vin = txtVIN.text;

                DBParkingLot.instance.UpdateVehicle(DBParkingLot.VehiclesSearchCriteria.Plate, myVehicle.Plate, myVehicle);
            }

            //Make labels equal to the text boxes

            lblPlate.Text = myVehicle.Plate;
            lblColor.Text = myVehicle.Color;
            lblMake.Text = myVehicle.Make;
            lblModel.Text = myVehicle.Model;
            lblYear.Text = myVehicle.Year;

            //disable the text boxes and make them invisible
            txtYear.Enabled = false;
            txtModel.Enabled = false;
            txtMake.Enabled = false;
            txtColor.Enabled = false;
            txtYear.Visible = false;
            txtModel.Visible = false;
            txtMake.Visible = false;
            txtColor.Visible = false;

            //Reenable and show labels
            lblYear.Enabled = true;
            lblPlate.Enabled = true;
            lblModel.Enabled = true;
            lblMake.Enabled = true;
            lblColor.Enabled = true;
            lblColor.Visible = true;
            lblMake.Visible = true;
            lblModel.Visible = true;
            lblPlate.Visible = true;
            lblYear.Visible = true;

            //Disable the done button
            btnDone.Enabled = false;
            btnDone.Visible = false;

            txtPlate.Enabled = false;
            txtPlate.Visible = false;

            //Enables owner selection.
            dupOwners.Enabled = true;
            btnOwner.Enabled = true;

            Text = myVehicle.ToString();
            LoadPeople();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void deleteVehicleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete this vehicle?", "Delete Vehicle", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes) {
                DBParkingLot.instance.DeleteVehicle(myVehicle.Plate);
                MessageBox.Show("Vehicle Deleted!");
                Close();
            }
        }

        private void btnOwner_Click(object sender, EventArgs e) {
            Person currPerson = myOwners[dupOwners.SelectedIndex];

            PersonForm pf = new PersonForm(currPerson);
            pf.Show();
        }
    }
}
