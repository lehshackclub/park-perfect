﻿namespace ParkPerfect
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.canvas = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createANewZoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAZoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resizeAZoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTagsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllParkingSpotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCancel = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblFeedbackMsg = new System.Windows.Forms.Label();
            this.lblTotalVehicles = new System.Windows.Forms.Label();
            this.lblTotalPeople = new System.Windows.Forms.Label();
            this.lblEmptySpots = new System.Windows.Forms.Label();
            this.pbSchoolLogo = new System.Windows.Forms.PictureBox();
            this.btnAddPerson = new System.Windows.Forms.Button();
            this.btnAssignToSpot = new System.Windows.Forms.Button();
            this.buttonUnassignFromSpot = new System.Windows.Forms.Button();
            this.buttonUnassignFromPerson = new System.Windows.Forms.Button();
            this.buttonDeletePerson = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSchoolLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.canvas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.canvas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvas.Location = new System.Drawing.Point(12, 39);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(1063, 924);
            this.canvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.canvas.TabIndex = 1;
            this.canvas.TabStop = false;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIleToolStripMenuItem,
            this.editToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1370, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fIleToolStripMenuItem
            // 
            this.fIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.openDatabaseToolStripMenuItem,
            this.importToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.reportIssueToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fIleToolStripMenuItem.Name = "fIleToolStripMenuItem";
            this.fIleToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fIleToolStripMenuItem.Text = "File";
            // 
            // newFileToolStripMenuItem
            // 
            this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
            this.newFileToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.newFileToolStripMenuItem.Text = "New File";
            this.newFileToolStripMenuItem.Click += new System.EventHandler(this.newFileToolStripMenuItem_Click);
            // 
            // openDatabaseToolStripMenuItem
            // 
            this.openDatabaseToolStripMenuItem.Name = "openDatabaseToolStripMenuItem";
            this.openDatabaseToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.openDatabaseToolStripMenuItem.Text = "Open File";
            this.openDatabaseToolStripMenuItem.Click += new System.EventHandler(this.openDatabaseToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.optionsToolStripMenuItem.Text = "Preferences";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // reportIssueToolStripMenuItem
            // 
            this.reportIssueToolStripMenuItem.Name = "reportIssueToolStripMenuItem";
            this.reportIssueToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.reportIssueToolStripMenuItem.Text = "Report Issue";
            this.reportIssueToolStripMenuItem.Click += new System.EventHandler(this.reportIssueToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createANewZoneToolStripMenuItem,
            this.deleteAZoneToolStripMenuItem,
            this.resizeAZoneToolStripMenuItem,
            this.editTagsToolStripMenuItem,
            this.clearAllParkingSpotsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // createANewZoneToolStripMenuItem
            // 
            this.createANewZoneToolStripMenuItem.Name = "createANewZoneToolStripMenuItem";
            this.createANewZoneToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.createANewZoneToolStripMenuItem.Text = "Create a new zone";
            this.createANewZoneToolStripMenuItem.Click += new System.EventHandler(this.createANewZoneToolStripMenuItem_Click);
            // 
            // deleteAZoneToolStripMenuItem
            // 
            this.deleteAZoneToolStripMenuItem.Name = "deleteAZoneToolStripMenuItem";
            this.deleteAZoneToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.deleteAZoneToolStripMenuItem.Text = "Delete a zone";
            this.deleteAZoneToolStripMenuItem.Click += new System.EventHandler(this.deleteAZoneToolStripMenuItem_Click);
            // 
            // resizeAZoneToolStripMenuItem
            // 
            this.resizeAZoneToolStripMenuItem.Name = "resizeAZoneToolStripMenuItem";
            this.resizeAZoneToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.resizeAZoneToolStripMenuItem.Text = "Resize a zone";
            this.resizeAZoneToolStripMenuItem.Click += new System.EventHandler(this.resizeAZoneToolStripMenuItem_Click);
            // 
            // editTagsToolStripMenuItem
            // 
            this.editTagsToolStripMenuItem.Name = "editTagsToolStripMenuItem";
            this.editTagsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.editTagsToolStripMenuItem.Text = "Edit Spot Tags";
            this.editTagsToolStripMenuItem.Click += new System.EventHandler(this.editTagsToolStripMenuItem_Click);
            // 
            // clearAllParkingSpotsToolStripMenuItem
            // 
            this.clearAllParkingSpotsToolStripMenuItem.Name = "clearAllParkingSpotsToolStripMenuItem";
            this.clearAllParkingSpotsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.clearAllParkingSpotsToolStripMenuItem.Text = "Clear All Parking Spots";
            this.clearAllParkingSpotsToolStripMenuItem.Click += new System.EventHandler(this.clearAllParkingSpotsToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            this.reportsToolStripMenuItem.Click += new System.EventHandler(this.reportsToolStripMenuItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(1295, 1);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "*.accdb";
            // 
            // lblFeedbackMsg
            // 
            this.lblFeedbackMsg.AutoSize = true;
            this.lblFeedbackMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeedbackMsg.Location = new System.Drawing.Point(346, 1);
            this.lblFeedbackMsg.Name = "lblFeedbackMsg";
            this.lblFeedbackMsg.Size = new System.Drawing.Size(229, 20);
            this.lblFeedbackMsg.TabIndex = 4;
            this.lblFeedbackMsg.Text = "Feedback Message Default";
            this.lblFeedbackMsg.Visible = false;
            // 
            // lblTotalVehicles
            // 
            this.lblTotalVehicles.AutoSize = true;
            this.lblTotalVehicles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVehicles.Location = new System.Drawing.Point(1088, 293);
            this.lblTotalVehicles.Name = "lblTotalVehicles";
            this.lblTotalVehicles.Size = new System.Drawing.Size(112, 20);
            this.lblTotalVehicles.TabIndex = 5;
            this.lblTotalVehicles.Text = "Total Vehicles:";
            // 
            // lblTotalPeople
            // 
            this.lblTotalPeople.AutoSize = true;
            this.lblTotalPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPeople.Location = new System.Drawing.Point(1088, 313);
            this.lblTotalPeople.Name = "lblTotalPeople";
            this.lblTotalPeople.Size = new System.Drawing.Size(101, 20);
            this.lblTotalPeople.TabIndex = 6;
            this.lblTotalPeople.Text = "Total People:";
            // 
            // lblEmptySpots
            // 
            this.lblEmptySpots.AutoSize = true;
            this.lblEmptySpots.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmptySpots.Location = new System.Drawing.Point(1088, 333);
            this.lblEmptySpots.Name = "lblEmptySpots";
            this.lblEmptySpots.Size = new System.Drawing.Size(104, 20);
            this.lblEmptySpots.TabIndex = 7;
            this.lblEmptySpots.Text = "Empty Spots:";
            // 
            // pbSchoolLogo
            // 
            this.pbSchoolLogo.BackColor = System.Drawing.Color.White;
            this.pbSchoolLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbSchoolLogo.Location = new System.Drawing.Point(1092, 39);
            this.pbSchoolLogo.Name = "pbSchoolLogo";
            this.pbSchoolLogo.Size = new System.Drawing.Size(356, 248);
            this.pbSchoolLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbSchoolLogo.TabIndex = 8;
            this.pbSchoolLogo.TabStop = false;
            // 
            // btnAddPerson
            // 
            this.btnAddPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPerson.Location = new System.Drawing.Point(1092, 376);
            this.btnAddPerson.Name = "btnAddPerson";
            this.btnAddPerson.Size = new System.Drawing.Size(212, 31);
            this.btnAddPerson.TabIndex = 9;
            this.btnAddPerson.Text = "Add New Person";
            this.btnAddPerson.UseVisualStyleBackColor = true;
            this.btnAddPerson.Click += new System.EventHandler(this.btnAddPerson_Click);
            // 
            // btnAssignToSpot
            // 
            this.btnAssignToSpot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssignToSpot.Location = new System.Drawing.Point(1092, 450);
            this.btnAssignToSpot.Name = "btnAssignToSpot";
            this.btnAssignToSpot.Size = new System.Drawing.Size(212, 31);
            this.btnAssignToSpot.TabIndex = 10;
            this.btnAssignToSpot.Text = "Assign person to spot";
            this.btnAssignToSpot.UseVisualStyleBackColor = true;
            this.btnAssignToSpot.Click += new System.EventHandler(this.btnAssignToSpot_Click);
            // 
            // buttonUnassignFromSpot
            // 
            this.buttonUnassignFromSpot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUnassignFromSpot.Location = new System.Drawing.Point(1092, 487);
            this.buttonUnassignFromSpot.Name = "buttonUnassignFromSpot";
            this.buttonUnassignFromSpot.Size = new System.Drawing.Size(212, 31);
            this.buttonUnassignFromSpot.TabIndex = 11;
            this.buttonUnassignFromSpot.Text = "Unassign person from spot";
            this.buttonUnassignFromSpot.UseVisualStyleBackColor = true;
            this.buttonUnassignFromSpot.Click += new System.EventHandler(this.buttonUnassignFromSpot_Click);
            // 
            // buttonUnassignFromPerson
            // 
            this.buttonUnassignFromPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUnassignFromPerson.Location = new System.Drawing.Point(1092, 524);
            this.buttonUnassignFromPerson.Name = "buttonUnassignFromPerson";
            this.buttonUnassignFromPerson.Size = new System.Drawing.Size(212, 31);
            this.buttonUnassignFromPerson.TabIndex = 12;
            this.buttonUnassignFromPerson.Text = "Unassign spot from person";
            this.buttonUnassignFromPerson.UseVisualStyleBackColor = true;
            this.buttonUnassignFromPerson.Click += new System.EventHandler(this.buttonUnassignFromPerson_Click);
            // 
            // buttonDeletePerson
            // 
            this.buttonDeletePerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeletePerson.Location = new System.Drawing.Point(1092, 413);
            this.buttonDeletePerson.Name = "buttonDeletePerson";
            this.buttonDeletePerson.Size = new System.Drawing.Size(212, 31);
            this.buttonDeletePerson.TabIndex = 13;
            this.buttonDeletePerson.Text = "Delete Person";
            this.buttonDeletePerson.UseVisualStyleBackColor = true;
            this.buttonDeletePerson.Click += new System.EventHandler(this.buttonDeletePerson_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.buttonDeletePerson);
            this.Controls.Add(this.buttonUnassignFromPerson);
            this.Controls.Add(this.buttonUnassignFromSpot);
            this.Controls.Add(this.btnAssignToSpot);
            this.Controls.Add(this.btnAddPerson);
            this.Controls.Add(this.pbSchoolLogo);
            this.Controls.Add(this.lblEmptySpots);
            this.Controls.Add(this.lblTotalPeople);
            this.Controls.Add(this.lblTotalVehicles);
            this.Controls.Add(this.lblFeedbackMsg);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parking Lot";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSchoolLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox canvas;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createANewZoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAZoneToolStripMenuItem;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDatabaseToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resizeAZoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.Label lblFeedbackMsg;
        private System.Windows.Forms.ToolStripMenuItem reportIssueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAllParkingSpotsToolStripMenuItem;
        private System.Windows.Forms.Label lblTotalVehicles;
        private System.Windows.Forms.Label lblTotalPeople;
        private System.Windows.Forms.Label lblEmptySpots;
        private System.Windows.Forms.PictureBox pbSchoolLogo;
        private System.Windows.Forms.Button btnAddPerson;
        private System.Windows.Forms.ToolStripMenuItem editTagsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
        private System.Windows.Forms.Button btnAssignToSpot;
        private System.Windows.Forms.Button buttonUnassignFromSpot;
        private System.Windows.Forms.Button buttonUnassignFromPerson;
        private System.Windows.Forms.Button buttonDeletePerson;
    }
}

