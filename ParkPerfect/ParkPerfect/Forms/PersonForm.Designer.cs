﻿namespace ParkPerfect
{
    partial class PersonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPVehicle = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblGrade = new System.Windows.Forms.Label();
            this.dupGrade = new System.Windows.Forms.DomainUpDown();
            this.btnVehicle = new System.Windows.Forms.Button();
            this.btnCitations = new System.Windows.Forms.Button();
            this.btnLicenseNReg = new System.Windows.Forms.Button();
            this.dupVehicles = new System.Windows.Forms.DomainUpDown();
            this.btnAddVehicle = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(288, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveStudentToolStripMenuItem,
            this.deleteStudentToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // moveStudentToolStripMenuItem
            // 
            this.moveStudentToolStripMenuItem.Name = "moveStudentToolStripMenuItem";
            this.moveStudentToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.moveStudentToolStripMenuItem.Text = "Move Student";
            this.moveStudentToolStripMenuItem.Click += new System.EventHandler(this.moveStudentToolStripMenuItem_Click);
            // 
            // deleteStudentToolStripMenuItem
            // 
            this.deleteStudentToolStripMenuItem.Name = "deleteStudentToolStripMenuItem";
            this.deleteStudentToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.deleteStudentToolStripMenuItem.Text = "Delete Student";
            this.deleteStudentToolStripMenuItem.Click += new System.EventHandler(this.deleteStudentToolStripMenuItem_Click_1);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(71, 36);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(87, 13);
            this.lblFirstName.TabIndex = 1;
            this.lblFirstName.Text = "Name: Jubal Foo";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(71, 89);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(86, 13);
            this.lblID.TabIndex = 3;
            this.lblID.Text = "PLACEHOLDER";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Location = new System.Drawing.Point(70, 33);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(202, 20);
            this.txtFirstName.TabIndex = 0;
            this.txtFirstName.Visible = false;
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(70, 86);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(202, 20);
            this.txtID.TabIndex = 2;
            this.txtID.Visible = false;
            // 
            // btnDone
            // 
            this.btnDone.Enabled = false;
            this.btnDone.Location = new System.Drawing.Point(203, 172);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(69, 23);
            this.btnDone.TabIndex = 5;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Visible = false;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "First Name:";
            // 
            // lblPVehicle
            // 
            this.lblPVehicle.AutoSize = true;
            this.lblPVehicle.Location = new System.Drawing.Point(5, 144);
            this.lblPVehicle.Name = "lblPVehicle";
            this.lblPVehicle.Size = new System.Drawing.Size(50, 13);
            this.lblPVehicle.TabIndex = 9;
            this.lblPVehicle.Text = "Vehicles:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "ID:";
            // 
            // txtLastName
            // 
            this.txtLastName.Enabled = false;
            this.txtLastName.Location = new System.Drawing.Point(70, 59);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(202, 20);
            this.txtLastName.TabIndex = 1;
            this.txtLastName.Visible = false;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(71, 62);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(87, 13);
            this.lblLastName.TabIndex = 12;
            this.lblLastName.Text = "Name: Jubal Foo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Last Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Grade:";
            // 
            // lblGrade
            // 
            this.lblGrade.AutoSize = true;
            this.lblGrade.Location = new System.Drawing.Point(71, 115);
            this.lblGrade.Name = "lblGrade";
            this.lblGrade.Size = new System.Drawing.Size(37, 13);
            this.lblGrade.TabIndex = 15;
            this.lblGrade.Text = "Senior";
            // 
            // dupGrade
            // 
            this.dupGrade.Enabled = false;
            this.dupGrade.Items.Add("Senior");
            this.dupGrade.Items.Add("Junior");
            this.dupGrade.Items.Add("Sophomore");
            this.dupGrade.Items.Add("Freshman");
            this.dupGrade.Items.Add("No Grade");
            this.dupGrade.Items.Add("");
            this.dupGrade.Items.Add("");
            this.dupGrade.Items.Add("");
            this.dupGrade.Location = new System.Drawing.Point(70, 113);
            this.dupGrade.Name = "dupGrade";
            this.dupGrade.Size = new System.Drawing.Size(202, 20);
            this.dupGrade.TabIndex = 3;
            this.dupGrade.Visible = false;
            // 
            // btnVehicle
            // 
            this.btnVehicle.Location = new System.Drawing.Point(227, 141);
            this.btnVehicle.Name = "btnVehicle";
            this.btnVehicle.Size = new System.Drawing.Size(45, 22);
            this.btnVehicle.TabIndex = 4;
            this.btnVehicle.Text = "View Vehicle";
            this.btnVehicle.UseVisualStyleBackColor = true;
            this.btnVehicle.Click += new System.EventHandler(this.btnVehicle_Click);
            // 
            // btnCitations
            // 
            this.btnCitations.Location = new System.Drawing.Point(7, 172);
            this.btnCitations.Name = "btnCitations";
            this.btnCitations.Size = new System.Drawing.Size(59, 23);
            this.btnCitations.TabIndex = 16;
            this.btnCitations.Text = "Citations";
            this.btnCitations.UseVisualStyleBackColor = true;
            this.btnCitations.Click += new System.EventHandler(this.btnCitations_Click);
            // 
            // btnLicenseNReg
            // 
            this.btnLicenseNReg.Location = new System.Drawing.Point(74, 172);
            this.btnLicenseNReg.Name = "btnLicenseNReg";
            this.btnLicenseNReg.Size = new System.Drawing.Size(123, 23);
            this.btnLicenseNReg.TabIndex = 17;
            this.btnLicenseNReg.Text = "License && Registration";
            this.btnLicenseNReg.UseVisualStyleBackColor = true;
            this.btnLicenseNReg.Click += new System.EventHandler(this.btnLicenseNReg_Click);
            // 
            // dupVehicles
            // 
            this.dupVehicles.Location = new System.Drawing.Point(70, 142);
            this.dupVehicles.Name = "dupVehicles";
            this.dupVehicles.Size = new System.Drawing.Size(160, 20);
            this.dupVehicles.TabIndex = 18;
            this.dupVehicles.Text = "No Vehicle";
            this.dupVehicles.Wrap = true;
            // 
            // btnAddVehicle
            // 
            this.btnAddVehicle.Enabled = false;
            this.btnAddVehicle.Location = new System.Drawing.Point(70, 141);
            this.btnAddVehicle.Name = "btnAddVehicle";
            this.btnAddVehicle.Size = new System.Drawing.Size(202, 23);
            this.btnAddVehicle.TabIndex = 19;
            this.btnAddVehicle.Text = "Add Vehicle";
            this.btnAddVehicle.UseVisualStyleBackColor = true;
            this.btnAddVehicle.Visible = false;
            this.btnAddVehicle.Click += new System.EventHandler(this.btnAddVehicle_Click);
            // 
            // PersonForm
            // 
            this.AcceptButton = this.btnDone;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 227);
            this.Controls.Add(this.btnAddVehicle);
            this.Controls.Add(this.dupVehicles);
            this.Controls.Add(this.btnLicenseNReg);
            this.Controls.Add(this.btnCitations);
            this.Controls.Add(this.btnVehicle);
            this.Controls.Add(this.dupGrade);
            this.Controls.Add(this.lblGrade);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPVehicle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.lblLastName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PersonForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Person Form";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPVehicle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblGrade;
        private System.Windows.Forms.DomainUpDown dupGrade;
        private System.Windows.Forms.Button btnVehicle;
        private System.Windows.Forms.ToolStripMenuItem moveStudentToolStripMenuItem;
        private System.Windows.Forms.Button btnCitations;
        private System.Windows.Forms.Button btnLicenseNReg;
        private System.Windows.Forms.ToolStripMenuItem deleteStudentToolStripMenuItem;
        private System.Windows.Forms.DomainUpDown dupVehicles;
        private System.Windows.Forms.Button btnAddVehicle;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
    }
}