﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ParkPerfect {
    public partial class SplashScreen : Form {

        List<Button> buttons;
        List<string> recentFiles;

        public SplashScreen() {
            InitializeComponent();

            //Creates the button list.
            buttons = new List<Button>();

            //Pulls the recent files from the appplication settings.
            recentFiles = Properties.Settings.Default.RecentFiles.Cast<string>().ToList<string>();

            //Creates the buttons.
            for (int i = 0; i < recentFiles.Count; i++) {
                /*Creates the display string and tooltip string.
                 *The display string is just the filename alone.
                 The tooltip is the filename and directory location.*/
                string displayString = recentFiles[i].Contains("\\") ? recentFiles[i].Substring(recentFiles[i].LastIndexOf('\\') + 1) : recentFiles[i];
                string toolTipString = recentFiles[i];

                //Creates the button.
                Button newButton = new Button();

                //Matches the design button size, location, and font.
                newButton.Size = buttonRecent.Size;
                newButton.Location = new Point(buttonRecent.Location.X, buttonRecent.Location.Y + buttonRecent.Size.Height * i);
                newButton.Font = buttonRecent.Font;

                //Sets the button's text to match the display string.
                newButton.Text = displayString;

                //Creates a tooltip for the button.
                ToolTip ToolTip1 = new ToolTip();
                ToolTip1.SetToolTip(newButton, toolTipString);

                //Creates an event handler for the button click.
                newButton.Click += new System.EventHandler(buttonRecent_Click);

                //Adds the button to the panel.
                recentPanel.Controls.Add(newButton);

                //Adds the button to the button list.
                buttons.Add(newButton);
            }

            //Hides the design button.
            buttonRecent.Hide();
            
        }

        private void buttonRecent_Click(object sender, EventArgs e) {
            //Casts the sender object to a button.
            Button sentButton = (Button)sender;

            //Gets the index of the button in button list.
            int index = buttons.IndexOf(sentButton);

            //Checks if the file still exists.
            if (!File.Exists(Properties.Settings.Default.RecentFiles[index])) {
                /*If the file doesn't exist, display an error message and remove
                 the file from the recently opened list.*/
                MessageBox.Show("Please move the database somewhere readable and writable.", "Move database", MessageBoxButtons.OK);

                Properties.Settings.Default.RecentFiles.RemoveAt(index);
                Properties.Settings.Default.Save();

                //Quit the application after attempting to open the file.
                Application.Exit();
                this.Close();
                return;
            }

            //Moves the file being opened to the top of the list.
            string str = Properties.Settings.Default.RecentFiles[index];
            Properties.Settings.Default.RecentFiles.RemoveAt(index);
            Properties.Settings.Default.RecentFiles.Insert(0, str);

            //Save user settings.
            Properties.Settings.Default.Save();

            //Opens the file.
            Close();
            Form1.instance.DefaultLoad(str);
        }

        private void buttonOpenDatabase_Click(object sender, EventArgs e) {
            DBParkingLotManager.OpenDatabase();
            Close();
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            DBParkingLotManager.NewDatabase();
            Close();
        }
    }
}
