﻿namespace ParkPerfect
{
    partial class CitationsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNewCitation = new System.Windows.Forms.TextBox();
            this.lboxCitations = new System.Windows.Forms.ListBox();
            this.btnAddCitation = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNewCitation
            // 
            this.txtNewCitation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewCitation.Location = new System.Drawing.Point(13, 286);
            this.txtNewCitation.Name = "txtNewCitation";
            this.txtNewCitation.Size = new System.Drawing.Size(514, 26);
            this.txtNewCitation.TabIndex = 0;
            // 
            // lboxCitations
            // 
            this.lboxCitations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lboxCitations.FormattingEnabled = true;
            this.lboxCitations.ItemHeight = 20;
            this.lboxCitations.Location = new System.Drawing.Point(12, 33);
            this.lboxCitations.Name = "lboxCitations";
            this.lboxCitations.Size = new System.Drawing.Size(514, 184);
            this.lboxCitations.TabIndex = 2;
            // 
            // btnAddCitation
            // 
            this.btnAddCitation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCitation.Location = new System.Drawing.Point(413, 318);
            this.btnAddCitation.Name = "btnAddCitation";
            this.btnAddCitation.Size = new System.Drawing.Size(114, 36);
            this.btnAddCitation.TabIndex = 3;
            this.btnAddCitation.Text = "Add Citation";
            this.btnAddCitation.UseVisualStyleBackColor = true;
            this.btnAddCitation.Click += new System.EventHandler(this.btnAddCitation_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Current Citations";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(219, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Add A Citation";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(293, 318);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 36);
            this.button1.TabIndex = 6;
            this.button1.Text = "Remove Citation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CitationsForm
            // 
            this.AcceptButton = this.btnAddCitation;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 356);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddCitation);
            this.Controls.Add(this.lboxCitations);
            this.Controls.Add(this.txtNewCitation);
            this.Name = "CitationsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CitationsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNewCitation;
        private System.Windows.Forms.ListBox lboxCitations;
        private System.Windows.Forms.Button btnAddCitation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}