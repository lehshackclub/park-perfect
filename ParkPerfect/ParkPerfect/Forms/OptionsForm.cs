﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace ParkPerfect
{
    public partial class OptionsForm : Form
    {
        GeneralInfo general;

        public OptionsForm()
        {
            general = DBParkingLot.instance.SearchGeneral();

            InitializeComponent();
            switch (Style.ZoneOutlineWidth)  //This makes the trackbar look right when it opens
            {
                case 3: trackBar1.Value = 1; break;
                case 5: trackBar1.Value = 2; break;
                case 8: trackBar1.Value = 3; break;
            }

            switch (Style.AreaLineWidth)
            {
                case .3f: trackBar2.Value = 1; break;
                case 1: trackBar2.Value = 2; break;
                case 2: trackBar2.Value = 3; break;
            }





            if (Style.AreaAutoAlign == true) { cbAutoAlign.CheckState = CheckState.Checked; } else { cbAutoAlign.CheckState = CheckState.Unchecked; }
        }

        private void btnZoneFont_Click(object sender, EventArgs e) {
            fontDialog.Font = Style.ZoneFont;
            fontDialog.ShowDialog();
            Style.ZoneFont = fontDialog.Font;
        }

        private void btnZoneFontColor_Click(object sender, EventArgs e) {
            colorDialog.Color = Style.ZoneFontColor;
            colorDialog.ShowDialog();
            Style.ZoneFontColor = colorDialog.Color;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            switch (trackBar1.Value)
            {
                case 1: Style.ZoneOutlineWidth = 3; break;
                case 2: Style.ZoneOutlineWidth = 5; break;
                case 3: Style.ZoneOutlineWidth = 8; break;
            }
        }

        private void btnAreaBackgroundColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = Style.AreaBackgroundColor;
            colorDialog.ShowDialog();
            Style.AreaBackgroundColor = colorDialog.Color;
        }

        private void btnAreaLineColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = Style.AreaLineColor;
            colorDialog.ShowDialog();
            Style.AreaLineColor = colorDialog.Color;
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            switch (trackBar2.Value)
            {
                case 1: Style.AreaLineWidth = .3f; break;
                case 2: Style.AreaLineWidth = 1; break;
                case 3: Style.AreaLineWidth = 2; break;
            }
        }

        private void btnHoverColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = Style.SpotHoverColor;
            colorDialog.ShowDialog();
            Style.SpotHoverColor = colorDialog.Color;
        }

        private void btnSpotFontColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = Style.SpotHoverColor;
            colorDialog.ShowDialog();
            Style.SpotFontColor = colorDialog.Color;
        }

        private void btnChangeSpotFont_Click(object sender, EventArgs e)
        {
            fontDialog.Font = Style.SpotFont;
            fontDialog.ShowDialog();
            Style.SpotFont = fontDialog.Font;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (listBox1.SelectedIndex)
            {
                case 0: //Dark Theme
                    Style.AreaLineColor = Color.FromArgb(255, 200, 200, 0);
                    Style.AreaBackgroundColor = Color.Black;
                    Style.SpotHoverColor = Color.DarkGray;
                    Style.SpotFontColor = Color.White;
                    break;
                case 1: //Light Theme
                    Style.AreaLineColor = Color.Black;
                    Style.AreaBackgroundColor = Color.LightGray;
                    Style.SpotHoverColor = Color.Gray;
                    Style.SpotFontColor = Color.Black;
                    break;
            }
        }

        private void cbAutoAlign_CheckStateChanged(object sender, EventArgs e)
        {
            if(cbAutoAlign.CheckState == CheckState.Checked)
            {
                Style.AreaAutoAlign = true;
            }
            else if (cbAutoAlign.CheckState == CheckState.Unchecked)
            {
                Style.AreaAutoAlign = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Style.Load();
            Close();
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            Style.Save();

            DBParkingLot.instance.UpdateGeneral(general.SchoolName, general.Image, general.Logo);

            MessageBox.Show("Some of these changes will not be applied without restarting the program.", "Restart may be required.", MessageBoxButtons.OK);

            Close();
        }

        private void buttonChangeName_Click(object sender, EventArgs e) {
            string schoolName = Microsoft.VisualBasic.Interaction.InputBox("Please enter the name you'd like to change the school name to.", "Enter a school name.", general.SchoolName).Trim();

            if (schoolName == string.Empty) return;

            general.SchoolName = schoolName;
        }

        private void buttonChangeImage_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            /*Sets the openFileDialog with some default info so the user
            can easily find the file and can't open a wrong file type.*/
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Bitmap Files (*.bmp;*.dib)|*.bmp;*.dib" +
                "|JPEG (*.jpg;*.jpeg;*.jpe;*.jfif)|*.jpg;*.jpeg;*.jpe;*.jfif" +
                "|GIF (*.gif)|*.gif|TIFF (*.tif;*.tiff)|*.tif;*.tiff" +
                "|PNG (*.png)|*.png" +
                "|ICO (*.ico)|*.ico" +
                "|All Picture Files|*.bmp;*.dib;*.jpg;*.jpeg;*.jpe;*.jfif;*.tif;*.tiff;*.png;*.ico" +
                "|All Files|*.*";
            openFileDialog.FilterIndex = 7;

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                Image lotImage = Image.FromFile(openFileDialog.FileName);
                general.Image = lotImage;
            }

        }

        private void buttonChangeLogo_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            /*Sets the openFileDialog with some default info so the user
            can easily find the file and can't open a wrong file type.*/
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Bitmap Files (*.bmp;*.dib)|*.bmp;*.dib" +
                "|JPEG (*.jpg;*.jpeg;*.jpe;*.jfif)|*.jpg;*.jpeg;*.jpe;*.jfif" +
                "|GIF (*.gif)|*.gif|TIFF (*.tif;*.tiff)|*.tif;*.tiff" +
                "|PNG (*.png)|*.png" +
                "|ICO (*.ico)|*.ico" +
                "|All Picture Files|*.bmp;*.dib;*.jpg;*.jpeg;*.jpe;*.jfif;*.tif;*.tiff;*.png;*.ico" +
                "|All Files|*.*";
            openFileDialog.FilterIndex = 7;

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                Image logo = Image.FromFile(openFileDialog.FileName);
                general.Logo = logo;
            }
        }
    }
}
