﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParkPerfect {
    public partial class PersonForm : Form {
        Person myPerson;
        Vehicle[] myVehicles;
        bool editing = false;

        public PersonForm(Person person) {
            InitializeComponent();

            LoadPerson(person);
            LoadVehicles();
        }

        public PersonForm() {
            InitializeComponent();
            lblFirstName.Text = "Unassigned";
            lblLastName.Text = "Unassigned";
            lblID.Text = "Null";
            lblGrade.Text = "Null";
            EnableEditMode();
        }

        private void LoadVehicles() {
            myVehicles = myPerson.GetVehicles();
            dupVehicles.Items.Clear();
            for (int i = 0; i < myVehicles.Length; i++) {
                dupVehicles.Items.Add(myVehicles[i].ToString()); //Populate the dup with the persons vehicles
                dupVehicles.DownButton(); //Displays the first item in the collection instead of the default text
            }
            lblPVehicle.Text = "Vehicles (" + myVehicles.Count() + "):";
        }

        private void LoadPerson(Person person) {
            myPerson = person;

            Text = myPerson.ToString();

            lblFirstName.Text = myPerson.FirstName;
            lblLastName.Text = myPerson.LastName;

            lblID.Text = myPerson.PersonID;
            lblGrade.Text = myPerson.Grade.ToString();
        }

        private void EnableEditMode() {
            editing = true;

            if (lblID.Text == "Null") {
                txtID.Enabled = true;
                txtID.Visible = true;

                txtFirstName.Text = "";
                txtLastName.Text = "";
            } else {
                //Put the persons information into the textboxes
                txtFirstName.Text = lblFirstName.Text;
                txtLastName.Text = lblLastName.Text;
                txtID.Text = lblID.Text;
                dupGrade.Text = lblGrade.Text;
            }

            //Enable the text boxes and make them visible
            txtFirstName.Enabled = true;
            txtLastName.Enabled = true;
            btnVehicle.Enabled = true;
            dupGrade.Enabled = true;
            txtFirstName.Visible = true;
            txtLastName.Visible = true;
            btnVehicle.Visible = true;
            dupGrade.Visible = true;
            btnAddVehicle.Enabled = true;
            btnAddVehicle.Visible = true;

            //Wipe the text out of the text boxes if they equal the default info
            if (lblFirstName.Text == "Unassigned" ||
            lblLastName.Text == "Unassigned" ||
            lblID.Text == "Null" ||
            lblGrade.Text == "Null") {
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtID.Text = "";
                dupGrade.Text = "";
            }

            //make the labels disabled and invisible
            lblFirstName.Enabled = false;
            lblLastName.Enabled = false;
            lblGrade.Enabled = false;
            lblFirstName.Visible = false;
            lblLastName.Visible = false;
            lblGrade.Visible = false;
            dupVehicles.Visible = false;
            dupVehicles.Enabled = false;
            btnVehicle.Enabled = false;
            btnVehicle.Visible = false;

            //Make the done button appear
            btnDone.Enabled = true;
            btnDone.Visible = true;
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e) {
            EnableEditMode();
        }

        private void btnDone_Click(object sender, EventArgs e) {
            bool existingId = false;
            Person matchingID = null;

            try {
                matchingID = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, txtID.Text);
                if (matchingID.SpotID != myPerson.SpotID) //Need a way to see if it exists
                {
                    existingId = true;
                }
            } catch (Exception) //If it can't find anything thats good!
              {

            }

            //If they are blank then dont save it
            if (txtFirstName.Text == "" ||
            txtLastName.Text == "" ||
            txtID.Text == "" ||
            dupGrade.Text == "") {

            } else if (existingId) //If there is an existing ID
              {
                MessageBox.Show("ID Already Exists!");
            } else {
                //If the spot was formerly empty, then create a new person for that spot
                if (myPerson == null) { DBParkingLot.instance.InsertPerson(new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, myPerson.SpotID, Person.Occupations.Student, getGrade())); myPerson = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.SpotID, myPerson.SpotID); } else if (myPerson == null) { DBParkingLot.instance.InsertPerson(new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, string.Empty, Person.Occupations.Student, getGrade())); } else //If the spot was previously occupied, then just update whoever was in there
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       {
                    DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, myPerson.PersonID, new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, myPerson.SpotID, myPerson.Occupation, getGrade()));
                }
                if (myPerson != null) {
                    myVehicles = myPerson.GetVehicles();

                    //Set the spot to 'Assigned'
                    Spot temp = DBParkingLot.instance.SearchForSpots(DBParkingLot.ParkingSpotSearchCriteria.SpotID, myPerson.SpotID)[0];
                    DBParkingLot.instance.UpdateParkingSpot(DBParkingLot.ParkingSpotSearchCriteria.SpotID, myPerson.SpotID, new Spot(temp.SpotID, temp.SpotType, temp.AreaName, temp.ZoneName));
                }

                //Make labels equal to the text boxes
                lblFirstName.Text = txtFirstName.Text;
                lblLastName.Text = txtLastName.Text;
                lblID.Text = txtID.Text;
                lblGrade.Text = dupGrade.Text;
            }

            //disable the text boxes and make them invisible
            txtFirstName.Enabled = false;
            txtLastName.Enabled = false;
            txtID.Enabled = false;
            dupGrade.Enabled = false;
            txtFirstName.Visible = false;
            txtLastName.Visible = false;
            txtID.Visible = false;
            dupGrade.Visible = false;
            btnAddVehicle.Enabled = false;
            btnAddVehicle.Visible = false;

            //Reenable and show labels
            lblFirstName.Enabled = true;
            lblLastName.Enabled = true;
            lblID.Enabled = true;
            lblGrade.Enabled = true;
            lblFirstName.Visible = true;
            lblLastName.Visible = true;
            lblID.Visible = true;
            lblGrade.Visible = true;
            dupVehicles.Visible = true;
            dupVehicles.Enabled = true;
            btnVehicle.Enabled = true;
            btnVehicle.Visible = true;

            //Disable the done button
            btnDone.Enabled = false;
            btnDone.Visible = false;

            editing = false;
        }

        private Person.Grades getGrade() {
            string entry = "";
            if (editing == true) {
                entry = dupGrade.Text;
            } else {
                entry = lblGrade.Text;
            }
            switch (entry) {
                case "No Grade": return Person.Grades.NoGrade;
                case "NoGrade": return Person.Grades.NoGrade;
                case "Freshman": return Person.Grades.Freshman;
                case "Sophomore": return Person.Grades.Sophomore;
                case "Junior": return Person.Grades.Junior;
                case "Senior": return Person.Grades.Senior;
                default: MessageBox.Show("ERROR 091"); return Person.Grades.NoGrade;
            }
        }

        private void changeSpotNumberToolStripMenuItem_Click(object sender, EventArgs e) {
            DBParkingLotManager.AssignPersonToSpot(myPerson);
            Refresh();
        }

        private void btnVehicle_Click(object sender, EventArgs e) {
            //If other fields are not filled out
            if (txtFirstName.Text == "Unassigned" ||
            txtLastName.Text == "Unassigned" ||
            txtID.Text == "Null" ||
            dupGrade.Text == "Null") {
                MessageBox.Show("Please fill out all other fields before the vehicle");
            } else //If everything is filled out
              {
                //Have to insert person into database first
                if (myPerson == null) { DBParkingLot.instance.InsertPerson(new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, myPerson.SpotID, Person.Occupations.Student, getGrade())); } else //If the spot was previously occupied, then just update whoever was in there
                                                                                                                                                                                                    {
                    if (editing) {
                        DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, myPerson.PersonID, new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, myPerson.SpotID, myPerson.Occupation, getGrade()));
                    } else {
                        DBParkingLot.instance.UpdatePeople(DBParkingLot.PeopleSearchCriteria.PersonID, myPerson.PersonID, new Person(lblFirstName.Text, lblLastName.Text, lblID.Text, myPerson.SpotID, myPerson.Occupation, getGrade()));
                    }
                }

                //Refresh myPerson with the new one we put in the database
                if (editing) {
                    myPerson = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, txtID.Text);
                } else {
                    myPerson = DBParkingLot.instance.SearchForPerson(DBParkingLot.PeopleSearchCriteria.PersonID, lblID.Text);

                }

                Vehicle currVehicle = myVehicles[dupVehicles.SelectedIndex];

                VehicleForm vf = new VehicleForm(currVehicle);
                vf.FormClosed += new FormClosedEventHandler(ChildFormClosed);
                vf.Show();

            }
        }

        private void btnCitations_Click(object sender, EventArgs e) {
            CitationsForm c = new CitationsForm(myPerson.PersonID);
            c.ShowDialog();
        }

        private void btnLicenseNReg_Click(object sender, EventArgs e) {
            LicenseAndRegistrationForm lnr = new LicenseAndRegistrationForm();
            lnr.ShowDialog();
        }

        private void ChildFormClosed(object sender, FormClosedEventArgs e) {
            LoadVehicles();
        }

        private void btnAddVehicle_Click(object sender, EventArgs e) {
            if (myPerson == null) {
                DBParkingLot.instance.InsertPerson(new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, myPerson.SpotID, Person.Occupations.Student, getGrade()));
            }
            myPerson = new Person(txtFirstName.Text, txtLastName.Text, txtID.Text, myPerson.SpotID, Person.Occupations.Student, getGrade());
            VehicleForm v = new VehicleForm(myPerson);
            v.FormClosed += new FormClosedEventHandler(ChildFormClosed);
            v.Show();
        }

        private void moveStudentToolStripMenuItem_Click(object sender, EventArgs e) {
            DBParkingLotManager.AssignPersonToSpot(myPerson);
        }

        private void deleteStudentToolStripMenuItem_Click_1(object sender, EventArgs e) {
            DBParkingLotManager.DeletePerson(myPerson);
            Close();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
