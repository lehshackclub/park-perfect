namespace ParkPerfect {
    partial class ReportForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
            this.checkedListBoxPerson = new System.Windows.Forms.CheckedListBox();
            this.lblPersonInformation = new System.Windows.Forms.Label();
            this.checkedListBoxVehicle = new System.Windows.Forms.CheckedListBox();
            this.lblVehicleInformation = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxColumn = new System.Windows.Forms.ComboBox();
            this.comboBoxOp = new System.Windows.Forms.ComboBox();
            this.textBoxKey = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxCondition = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxColumn2 = new System.Windows.Forms.ComboBox();
            this.comboBoxOp2 = new System.Windows.Forms.ComboBox();
            this.textBoxKey2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxCondition2 = new System.Windows.Forms.CheckBox();
            this.checkBoxCondition4 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxKey4 = new System.Windows.Forms.TextBox();
            this.comboBoxOp4 = new System.Windows.Forms.ComboBox();
            this.comboBoxColumn4 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBoxCondition3 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxKey3 = new System.Windows.Forms.TextBox();
            this.comboBoxOp3 = new System.Windows.Forms.ComboBox();
            this.comboBoxColumn3 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pdoc = new System.Drawing.Printing.PrintDocument();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // checkedListBoxPerson
            // 
            this.checkedListBoxPerson.CheckOnClick = true;
            this.checkedListBoxPerson.FormattingEnabled = true;
            this.checkedListBoxPerson.Items.AddRange(new object[] {
            "FirstName",
            "LastName",
            "PersonID",
            "SpotNumber",
            "Grade",
            "Occupation"});
            this.checkedListBoxPerson.Location = new System.Drawing.Point(8, 44);
            this.checkedListBoxPerson.Name = "checkedListBoxPerson";
            this.checkedListBoxPerson.Size = new System.Drawing.Size(120, 94);
            this.checkedListBoxPerson.TabIndex = 2;
            // 
            // lblPersonInformation
            // 
            this.lblPersonInformation.Location = new System.Drawing.Point(9, 18);
            this.lblPersonInformation.Name = "lblPersonInformation";
            this.lblPersonInformation.Size = new System.Drawing.Size(119, 23);
            this.lblPersonInformation.TabIndex = 3;
            this.lblPersonInformation.Text = "Person Information";
            // 
            // checkedListBoxVehicle
            // 
            this.checkedListBoxVehicle.CheckOnClick = true;
            this.checkedListBoxVehicle.FormattingEnabled = true;
            this.checkedListBoxVehicle.Items.AddRange(new object[] {
            "Plate",
            "Make",
            "Model",
            "Color",
            "YearMade",
            "VIN"});
            this.checkedListBoxVehicle.Location = new System.Drawing.Point(8, 186);
            this.checkedListBoxVehicle.Name = "checkedListBoxVehicle";
            this.checkedListBoxVehicle.Size = new System.Drawing.Size(120, 94);
            this.checkedListBoxVehicle.TabIndex = 4;
            // 
            // lblVehicleInformation
            // 
            this.lblVehicleInformation.Location = new System.Drawing.Point(9, 160);
            this.lblVehicleInformation.Name = "lblVehicleInformation";
            this.lblVehicleInformation.Size = new System.Drawing.Size(119, 23);
            this.lblVehicleInformation.TabIndex = 5;
            this.lblVehicleInformation.Text = "Vehicle Information";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(368, 348);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 12;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(166, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Check if";
            // 
            // comboBoxColumn
            // 
            this.comboBoxColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxColumn.Enabled = false;
            this.comboBoxColumn.FormattingEnabled = true;
            this.comboBoxColumn.Items.AddRange(new object[] {
            "",
            "FirstName",
            "LastName",
            "PersonID",
            "SpotNumber",
            "Grade",
            "Occupation",
            "Plate",
            "Make",
            "Model",
            "Color",
            "YearMade",
            "VIN"});
            this.comboBoxColumn.Location = new System.Drawing.Point(164, 71);
            this.comboBoxColumn.Name = "comboBoxColumn";
            this.comboBoxColumn.Size = new System.Drawing.Size(121, 21);
            this.comboBoxColumn.TabIndex = 7;
            // 
            // comboBoxOp
            // 
            this.comboBoxOp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOp.Enabled = false;
            this.comboBoxOp.FormattingEnabled = true;
            this.comboBoxOp.Items.AddRange(new object[] {
            "EqualTo",
            "GreaterThanOrEqualTo",
            "LessThanOrEqualTo",
            "GreaterThan",
            "LessThan"});
            this.comboBoxOp.Location = new System.Drawing.Point(296, 71);
            this.comboBoxOp.Name = "comboBoxOp";
            this.comboBoxOp.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOp.TabIndex = 8;
            // 
            // textBoxKey
            // 
            this.textBoxKey.Enabled = false;
            this.textBoxKey.Location = new System.Drawing.Point(424, 71);
            this.textBoxKey.Name = "textBoxKey";
            this.textBoxKey.Size = new System.Drawing.Size(100, 20);
            this.textBoxKey.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(298, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Is";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(423, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "to";
            // 
            // checkBoxCondition
            // 
            this.checkBoxCondition.AutoSize = true;
            this.checkBoxCondition.Enabled = false;
            this.checkBoxCondition.Location = new System.Drawing.Point(164, 24);
            this.checkBoxCondition.Name = "checkBoxCondition";
            this.checkBoxCondition.Size = new System.Drawing.Size(106, 17);
            this.checkBoxCondition.TabIndex = 13;
            this.checkBoxCondition.Text = "Enable Condition";
            this.checkBoxCondition.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(166, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Check if";
            // 
            // comboBoxColumn2
            // 
            this.comboBoxColumn2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxColumn2.Enabled = false;
            this.comboBoxColumn2.FormattingEnabled = true;
            this.comboBoxColumn2.Items.AddRange(new object[] {
            "",
            "FirstName",
            "LastName",
            "PersonID",
            "SpotNumber",
            "Grade",
            "Occupation",
            "Plate",
            "Make",
            "Model",
            "Color",
            "YearMade",
            "VIN"});
            this.comboBoxColumn2.Location = new System.Drawing.Point(164, 144);
            this.comboBoxColumn2.Name = "comboBoxColumn2";
            this.comboBoxColumn2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxColumn2.TabIndex = 15;
            // 
            // comboBoxOp2
            // 
            this.comboBoxOp2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOp2.Enabled = false;
            this.comboBoxOp2.FormattingEnabled = true;
            this.comboBoxOp2.Items.AddRange(new object[] {
            "EqualTo",
            "GreaterThanOrEqualTo",
            "LessThanOrEqualTo",
            "GreaterThan",
            "LessThan"});
            this.comboBoxOp2.Location = new System.Drawing.Point(296, 144);
            this.comboBoxOp2.Name = "comboBoxOp2";
            this.comboBoxOp2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOp2.TabIndex = 16;
            // 
            // textBoxKey2
            // 
            this.textBoxKey2.Enabled = false;
            this.textBoxKey2.Location = new System.Drawing.Point(424, 144);
            this.textBoxKey2.Name = "textBoxKey2";
            this.textBoxKey2.Size = new System.Drawing.Size(100, 20);
            this.textBoxKey2.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(298, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Is";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(423, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "to";
            // 
            // checkBoxCondition2
            // 
            this.checkBoxCondition2.AutoSize = true;
            this.checkBoxCondition2.Enabled = false;
            this.checkBoxCondition2.Location = new System.Drawing.Point(164, 97);
            this.checkBoxCondition2.Name = "checkBoxCondition2";
            this.checkBoxCondition2.Size = new System.Drawing.Size(106, 17);
            this.checkBoxCondition2.TabIndex = 20;
            this.checkBoxCondition2.Text = "Enable Condition";
            this.checkBoxCondition2.UseVisualStyleBackColor = true;
            // 
            // checkBoxCondition4
            // 
            this.checkBoxCondition4.AutoSize = true;
            this.checkBoxCondition4.Enabled = false;
            this.checkBoxCondition4.Location = new System.Drawing.Point(164, 246);
            this.checkBoxCondition4.Name = "checkBoxCondition4";
            this.checkBoxCondition4.Size = new System.Drawing.Size(106, 17);
            this.checkBoxCondition4.TabIndex = 34;
            this.checkBoxCondition4.Text = "Enable Condition";
            this.checkBoxCondition4.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(423, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "to";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(298, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Is";
            // 
            // textBoxKey4
            // 
            this.textBoxKey4.Enabled = false;
            this.textBoxKey4.Location = new System.Drawing.Point(424, 293);
            this.textBoxKey4.Name = "textBoxKey4";
            this.textBoxKey4.Size = new System.Drawing.Size(100, 20);
            this.textBoxKey4.TabIndex = 31;
            // 
            // comboBoxOp4
            // 
            this.comboBoxOp4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOp4.Enabled = false;
            this.comboBoxOp4.FormattingEnabled = true;
            this.comboBoxOp4.Items.AddRange(new object[] {
            "EqualTo",
            "GreaterThanOrEqualTo",
            "LessThanOrEqualTo",
            "GreaterThan",
            "LessThan"});
            this.comboBoxOp4.Location = new System.Drawing.Point(296, 293);
            this.comboBoxOp4.Name = "comboBoxOp4";
            this.comboBoxOp4.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOp4.TabIndex = 30;
            // 
            // comboBoxColumn4
            // 
            this.comboBoxColumn4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxColumn4.Enabled = false;
            this.comboBoxColumn4.FormattingEnabled = true;
            this.comboBoxColumn4.Items.AddRange(new object[] {
            "",
            "FirstName",
            "LastName",
            "PersonID",
            "SpotNumber",
            "Grade",
            "Occupation",
            "Plate",
            "Make",
            "Model",
            "Color",
            "YearMade",
            "VIN"});
            this.comboBoxColumn4.Location = new System.Drawing.Point(164, 293);
            this.comboBoxColumn4.Name = "comboBoxColumn4";
            this.comboBoxColumn4.Size = new System.Drawing.Size(121, 21);
            this.comboBoxColumn4.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(166, 277);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Check if";
            // 
            // checkBoxCondition3
            // 
            this.checkBoxCondition3.AutoSize = true;
            this.checkBoxCondition3.Enabled = false;
            this.checkBoxCondition3.Location = new System.Drawing.Point(164, 170);
            this.checkBoxCondition3.Name = "checkBoxCondition3";
            this.checkBoxCondition3.Size = new System.Drawing.Size(106, 17);
            this.checkBoxCondition3.TabIndex = 27;
            this.checkBoxCondition3.Text = "Enable Condition";
            this.checkBoxCondition3.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(423, 201);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "to";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(298, 201);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Is";
            // 
            // textBoxKey3
            // 
            this.textBoxKey3.Enabled = false;
            this.textBoxKey3.Location = new System.Drawing.Point(424, 217);
            this.textBoxKey3.Name = "textBoxKey3";
            this.textBoxKey3.Size = new System.Drawing.Size(100, 20);
            this.textBoxKey3.TabIndex = 24;
            // 
            // comboBoxOp3
            // 
            this.comboBoxOp3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOp3.Enabled = false;
            this.comboBoxOp3.FormattingEnabled = true;
            this.comboBoxOp3.Items.AddRange(new object[] {
            "EqualTo",
            "GreaterThanOrEqualTo",
            "LessThanOrEqualTo",
            "GreaterThan",
            "LessThan"});
            this.comboBoxOp3.Location = new System.Drawing.Point(296, 217);
            this.comboBoxOp3.Name = "comboBoxOp3";
            this.comboBoxOp3.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOp3.TabIndex = 23;
            // 
            // comboBoxColumn3
            // 
            this.comboBoxColumn3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxColumn3.Enabled = false;
            this.comboBoxColumn3.FormattingEnabled = true;
            this.comboBoxColumn3.Items.AddRange(new object[] {
            "",
            "FirstName",
            "LastName",
            "PersonID",
            "SpotNumber",
            "Grade",
            "Occupation",
            "Plate",
            "Make",
            "Model",
            "Color",
            "YearMade",
            "VIN"});
            this.comboBoxColumn3.Location = new System.Drawing.Point(164, 217);
            this.comboBoxColumn3.Name = "comboBoxColumn3";
            this.comboBoxColumn3.Size = new System.Drawing.Size(121, 21);
            this.comboBoxColumn3.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(166, 201);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Check if";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView.Location = new System.Drawing.Point(562, 71);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(359, 300);
            this.dataGridView.TabIndex = 35;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(449, 348);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 36;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.Document = this.pdoc;
            this.printDialog1.UseEXDialog = true;
            // 
            // pdoc
            // 
            this.pdoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.pdoc_PrintPage);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Enabled = false;
            this.buttonPrint.Location = new System.Drawing.Point(287, 348);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(75, 23);
            this.buttonPrint.TabIndex = 37;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.pdoc;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 392);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.checkBoxCondition4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxKey4);
            this.Controls.Add(this.comboBoxOp4);
            this.Controls.Add(this.comboBoxColumn4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.checkBoxCondition3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxKey3);
            this.Controls.Add(this.comboBoxOp3);
            this.Controls.Add(this.comboBoxColumn3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.checkBoxCondition2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxKey2);
            this.Controls.Add(this.comboBoxOp2);
            this.Controls.Add(this.comboBoxColumn2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.checkBoxCondition);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxKey);
            this.Controls.Add(this.comboBoxOp);
            this.Controls.Add(this.comboBoxColumn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblVehicleInformation);
            this.Controls.Add(this.checkedListBoxVehicle);
            this.Controls.Add(this.lblPersonInformation);
            this.Controls.Add(this.checkedListBoxPerson);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(949, 430);
            this.Name = "ReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckedListBox checkedListBoxPerson;
        private System.Windows.Forms.Label lblPersonInformation;
        private System.Windows.Forms.CheckedListBox checkedListBoxVehicle;
        private System.Windows.Forms.Label lblVehicleInformation;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxColumn;
        private System.Windows.Forms.ComboBox comboBoxOp;
        private System.Windows.Forms.TextBox textBoxKey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxCondition;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxColumn2;
        private System.Windows.Forms.ComboBox comboBoxOp2;
        private System.Windows.Forms.TextBox textBoxKey2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxCondition2;
        private System.Windows.Forms.CheckBox checkBoxCondition4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxKey4;
        private System.Windows.Forms.ComboBox comboBoxOp4;
        private System.Windows.Forms.ComboBox comboBoxColumn4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBoxCondition3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxKey3;
        private System.Windows.Forms.ComboBox comboBoxOp3;
        private System.Windows.Forms.ComboBox comboBoxColumn3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Button buttonPrint;
        private System.Drawing.Printing.PrintDocument pdoc;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}