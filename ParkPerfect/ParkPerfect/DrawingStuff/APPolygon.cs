﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace ParkPerfect
{
    public class APPolygon : APShape
    {
        private List<Point> points = new List<Point>();
        StringFormat sf = new StringFormat();

        public APPolygon()
        {
        }
        public override void draw(Graphics g)
        {
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            g.FillPolygon(new SolidBrush(Color.FromArgb(40, getColor())), points.ToArray());   //gives the zone that lil tint on the inside
            g.DrawPolygon(new Pen(getColor(), Style.ZoneOutlineWidth), points.ToArray());       //this draws the outline of the zone
            g.DrawString(getName(), Style.ZoneFont, new SolidBrush(Style.ZoneFontColor), GetCentroid(points), sf);    //draws the zones name
        }

        public static PointF GetCentroid(List<Point> poly)
        {
            float accumulatedArea = 0.0f;
            float centerX = 0.0f;
            float centerY = 0.0f;

            for (int i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                float temp = poly[i].X * poly[j].Y - poly[j].X * poly[i].Y;
                accumulatedArea += temp;
                centerX += (poly[i].X + poly[j].X) * temp;
                centerY += (poly[i].Y + poly[j].Y) * temp;
            }

            if (Math.Abs(accumulatedArea) < 1E-7f)
                return PointF.Empty;  // Avoid division by zero

            accumulatedArea *= 3f;
            return new PointF(centerX / accumulatedArea, centerY / accumulatedArea);
        }

        public APPolygon(Color c, List<Point> ps)
            : base(0, 0, 0, 0, Color.Purple)
        {
            setColor(c);
            points = ps;
        }

        private int averageX()
        {
            int total = 0;
            foreach (Point point in points)
            {
                total += point.X;
            }
            return total / points.Count;
        }

        private int averageY()
        {
            int total = 0;
            foreach (Point point in points)
            {
                total += point.Y;
            }
            return total / points.Count;

        }

        public override List<Point> getPoints()
        {
            return points;
        }

        public int ActualHeight()
        {
            int topMost = 0;
            int bottomMost = int.MaxValue;

            foreach (var item in points)
            {
                if (item.Y > topMost) { topMost = item.Y; }
                if (item.Y < bottomMost) { bottomMost = item.Y; }
            }

            return topMost - bottomMost;
        }

        public int ActualWidth()
        {
            int leftMost = int.MaxValue;
            int rightMost = 0;

            foreach (var item in points)
            {
                if (item.X < leftMost) { leftMost = item.X; }
                if (item.X > rightMost) { rightMost = item.X; }
            }

            return rightMost - leftMost;
        }

        #region isWithin and helpers
        public override bool isWithin(Point p)
        {
            // Get the angle between the point and the
            // first and last vertices.
            int max_point = points.Count - 1;
            float total_angle = GetAngle(
                points[max_point].X, points[max_point].Y,
                p.X, p.Y,
                points[0].X, points[0].Y);

            // Add the angles from the point
            // to each other pair of vertices.
            for (int i = 0; i < max_point; i++)
            {
                total_angle += GetAngle(
                    points[i].X, points[i].Y,
                    p.X, p.Y,
                    points[i + 1].X, points[i + 1].Y);
            }

            // The total angle should be 2 * PI or -2 * PI if
            // the point is in the polygon and close to zero
            // if the point is outside the polygon.
            return (Math.Abs(total_angle) > 0.000001);
        }

        // Return the angle ABC.
        // Return a value between PI and -PI.
        // Note that the value is the opposite of what you might
        // expect because Y coordinates increase downward.
        public static float GetAngle(float Ax, float Ay,
            float Bx, float By, float Cx, float Cy)
        {
            // Get the dot product.
            float dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy);

            // Get the cross product.
            float cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, Cy);

            // Calculate the angle.
            return (float)Math.Atan2(cross_product, dot_product);
        }

        // Return the dot product AB · BC.
        // Note that AB · BC = |AB| * |BC| * Cos(theta).
        private static float DotProduct(float Ax, float Ay,
            float Bx, float By, float Cx, float Cy)
        {
            // Get the vectors' coordinates.
            float BAx = Ax - Bx;
            float BAy = Ay - By;
            float BCx = Cx - Bx;
            float BCy = Cy - By;

            // Calculate the dot product.
            return (BAx * BCx + BAy * BCy);
        }

        // Return the cross product AB x BC.
        // The cross product is a vector perpendicular to AB
        // and BC having length |AB| * |BC| * Sin(theta) and
        // with direction given by the right-hand rule.
        // For two vectors in the X-Y plane, the result is a
        // vector with X and Y components 0 so the Z component
        // gives the vector's length and direction.
        public static float CrossProductLength(float Ax, float Ay,
            float Bx, float By, float Cx, float Cy)
        {
            // Get the vectors' coordinates.
            float BAx = Ax - Bx;
            float BAy = Ay - By;
            float BCx = Cx - Bx;
            float BCy = Cy - By;

            // Calculate the Z coordinate of the cross product.
            return (BAx * BCy - BAy * BCx);
        }

        #endregion
    }
}

