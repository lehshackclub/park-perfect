using System;
using System.Collections.Generic;
using System.Drawing;

namespace ParkPerfect
{
    public class APBox : APShape
    {
        int thiccness;

        public APBox()
        {
        }

        public APBox(int x1, int y1, int x2, int y2, Color c, int size)
         : base(x1, y1, x2, y2, Color.Purple)
        {
            setColor(c);
            thiccness = size;
        }

        public APBox(int x1, int y1, int x2, int y2)
         : base(x1, y1, x2, y2, Color.Purple)
        {

        }

        /// <summary>
        /// Draws a box
        /// </summary>
        /// <param name="g">Graphics object</param>
        public override void draw(Graphics g)
        {
            g.DrawRectangle(new Pen(getColor(), thiccness),
                Math.Min(getX1(), getX2()), //find Top left corner X value
                Math.Min(getY1(), getY2()), //find Top left corner Y
                Math.Abs(getX2() - getX1()), //Get Width
                Math.Abs(getY2() - getY1())); //Get Height
            g.DrawString(getName(), new Font(FontFamily.GenericSerif, 24, FontStyle.Bold), new SolidBrush(getColor()), new Point(Math.Min(getX1(), getX2()), Math.Min(getY1(), getY2())));
        }


        public Rectangle getRectangle()
        {
            return new Rectangle(Math.Min(getX1(), getX2()), Math.Min(getY1(), getY2()), Math.Abs(getX2() - getX1()), Math.Abs(getY2() - getY1()));
        }

        public int getHeight()
        {
            return Math.Abs(getY2() - getY1()); //Get Height
        }

        public int getWidth()
        {
            return Math.Abs(getX2() - getX1()); //Get Width
        }

        public override bool isWithin(Point p)
        {
            if (getRectangle().Contains(p))
            {
                return true;
            }
            return false;
        }

        public override List<Point> getPoints()
        {
            List<Point> p = new List<Point>();
            p.Add(new Point(getX1(), getY1()));
            p.Add(new Point(getX2(), getY2()));
            return p;
        }
    }
}