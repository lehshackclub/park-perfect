![Park Perfect](Logo.png)
======================

The repository contains the combined user interface and database code for Park Perfect, a project by Lakota East Hack Club.

## Table of content

- [Getting Started](#getting-started)
- [Repository Team Members](#repository-team-members)

## Getting Started

The Trello board will be constantly updated with goals that need to be accomplished. If the goal is not assigned to a specific team member, you are free to work on it. 
The Trello board can be found at by navigating down to "Boards" on the left hand side of the projec repo.


## Repository Team Members

* "Traegan Daniels" <trae.daniels@gmail.com>
* "Robby Hoover" <robbyphoover@gmail.com>
* "Kyle Lierer" <ktlierer@gmail.com>
* "Wesley Reed" <reed.wesley.s@gmail.com>
